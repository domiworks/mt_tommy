<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/bootstrap.offcanvas.css">
        <?php include('_css.php'); ?>
        
    </head>
    <body class="body-offcanvas">
        <!-- Header -->
        <!--==================================================-->
        <header>
            <!-- Begin Navbar -->
            <div class="row purpleHeader">
                
            </div>
            <div class="row backgroundHeader">
                
            </div>

        </header>
        <!--Content-->
        <!--==================================================-->
        <section id="content">
            <div class="row backgroundContent">
                <div class="container">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                                <div class="news-module-ver">
                                    <div class="news-module-ver-img">
                                        <img src="../img/produk/dummy.jpg">
                                    </div>

                                    <div class="news-module-ver-content font-sourceSansPro">
                                        <div class="news-module-ver-title">
                                            Jejak Sejarah yang Hilang di Makassar
                                        </div>

                                        <div class="news-module-ver-etc">
                                            <div class="news-module-ver-tag pull-left">
                                                SPONSOR
                                                <div class="orange-tooltip">
                                                    <img src="../img/icon/orgTooltip.png">
                                                </div>
                                            </div>


                                            <div class="news-module-ver-icon pull-left">
                                                <img src="../img/icon/clock.png">
                                            </div>

                                            <div class="news-module-ver-time pull-left">
                                                24 Februari 2016 13:15
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="news-module-ver">
                                    <div class="news-module-ver-img">
                                        <img src="../img/produk/dummy.jpg">
                                    </div>

                                    <div class="news-module-ver-content font-sourceSansPro">
                                        <div class="news-module-ver-title">
                                            Jejak Sejarah yang Hilang di Makassar
                                        </div>

                                        <div class="news-module-ver-etc">
                                            <div class="news-module-ver-tag pull-left">
                                                SPONSOR
                                                <div class="orange-tooltip">
                                                    <img src="../img/icon/orgTooltip.png">
                                                </div>
                                            </div>


                                            <div class="news-module-ver-icon pull-left">
                                                <img src="../img/icon/clock.png">
                                            </div>

                                            <div class="news-module-ver-time pull-left">
                                                24 Februari 2016 13:15
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="news-module-ver">
                                    <div class="news-module-ver-img">
                                        <img src="../img/produk/dummy.jpg">
                                    </div>

                                    <div class="news-module-ver-content font-sourceSansPro">
                                        <div class="news-module-ver-title">
                                            Jejak Sejarah yang Hilang di Makassar
                                        </div>

                                        <div class="news-module-ver-etc">
                                            <div class="news-module-ver-tag pull-left">
                                                SPONSOR
                                                <div class="orange-tooltip">
                                                    <img src="../img/icon/orgTooltip.png">
                                                </div>
                                            </div>


                                            <div class="news-module-ver-icon pull-left">
                                                <img src="../img/icon/clock.png">
                                            </div>

                                            <div class="news-module-ver-time pull-left">
                                                24 Februari 2016 13:15
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </body>    
</html>