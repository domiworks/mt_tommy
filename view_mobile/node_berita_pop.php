<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/bootstrap.offcanvas.css">

        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Header -->
        <!--==================================================-->
        <header>
            <!-- Begin Navbar -->

        </header>
        <!--Content-->
        <!--==================================================-->
        <section id="content">
            <div class="row backgroundContent">
                <div class="container">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">

                                <div class="news-wrapper-pop">
                                    <div class="news-wrapper-pop-header">
                                        <div class="news-wrapper-pop-left">
                                            <img src="../img/icon/berita-pop.png">
                                        </div>

                                        <div class="news-wrapper-pop-right">
                                              LIHAT SEMUA <a href="#"><img src="../img/icon/all-window.png"></a>
                                        </div>
                                    </div>

                                    <div class="news-module-hor-pop">
                                        <div class="news-module-hor-img-pop">
                                            <img src="../img/produk/dummy.jpg">
                                        </div>

                                        <div class="news-module-hor-content-pop font-sourceSansPro">
                                            <div class="news-module-hor-title-pop">
                                                Calon Kepala Sekolah Berkumpul di Kediaman Danny, Mau Apa Yah?
                                            </div>

                                            <div class="news-module-hor-desc-pop">
                                                Dilihat 2500 kali
                                            </div>
                                        </div>
                                    </div>

                                    <div class="news-module-hor-pop">
                                        <div class="news-module-hor-img-pop">
                                            <img src="../img/produk/dummy.jpg">
                                        </div>

                                        <div class="news-module-hor-content-pop font-sourceSansPro">
                                            <div class="news-module-hor-title-pop">
                                                Calon Kepala Sekolah Berkumpul di Kediaman Danny, Mau Apa Yah?
                                            </div>

                                            <div class="news-module-hor-desc-pop">
                                                Dilihat 2500 kali
                                            </div>
                                        </div>
                                    </div>

                                    <div class="news-module-hor-pop">
                                        <div class="news-module-hor-img-pop">
                                            <img src="../img/produk/dummy.jpg">
                                        </div>

                                        <div class="news-module-hor-content-pop font-sourceSansPro">
                                            <div class="news-module-hor-title-pop">
                                                Calon Kepala Sekolah Berkumpul di Kediaman Danny, Mau Apa Yah?
                                            </div>

                                            <div class="news-module-hor-desc-pop">
                                                Dilihat 2500 kali
                                            </div>
                                        </div>
                                    </div>

                                    <div class="news-module-hor-pop">
                                        <div class="news-module-hor-img-pop">
                                            <img src="../img/produk/dummy.jpg">
                                        </div>

                                        <div class="news-module-hor-content-pop font-sourceSansPro">
                                            <div class="news-module-hor-title-pop">
                                                Calon Kepala Sekolah Berkumpul di Kediaman Danny, Mau Apa Yah?
                                            </div>

                                            <div class="news-module-hor-desc-pop">
                                                Dilihat 2500 kali
                                            </div>
                                        </div>
                                    </div>

                                    <div class="news-module-hor-pop">
                                        <div class="news-module-hor-img-pop">
                                            <img src="../img/produk/dummy.jpg">
                                        </div>

                                        <div class="news-module-hor-content-pop font-sourceSansPro">
                                            <div class="news-module-hor-title-pop">
                                                Calon Kepala Sekolah Berkumpul di Kediaman Danny, Mau Apa Yah?
                                            </div>

                                            <div class="news-module-hor-desc-pop">
                                                Dilihat 2500 kali
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </body>
</html>
