
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/bootstrap.offcanvas.css">
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" href="../css/custom/home.css">
        <link rel="stylesheet" href="../css/custom/header.css">

        <link rel="stylesheet" href="../css/custom/news-module-hor.css">
        <link rel="stylesheet" href="../css/custom/news-module-ver.css">
        <link rel="stylesheet" href="../css/custom/news-module-pop.css">
        <link rel="stylesheet" href="../css/custom/event-module-terkini.css">
        <link rel="stylesheet" href="../css/custom/node-privacy-pol.css">
        <link rel="stylesheet" href="../css/custom/node-login-register.css">

        <link rel="stylesheet" href="../css/custom/advertisement.css">
        <link rel="stylesheet" href="../css/custom/main-content.css">

        <script src="../js/jquery-2.1.4.js"></script>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

