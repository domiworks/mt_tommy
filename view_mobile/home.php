<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Advertisement Top -->
        <div class="adv adv-top">
            <div class="adv-closer">
                <span>close/tutup</span>
                <span class="the-x">X</span>
            </div>
            ADVERTISE 1100 x 90
        </div>
        <!--==================================================-->
        <!-- Header -->
        <!--==================================================-->
        <header class="s-header">
            <!-- Begin Navbar -->
            <div class="s-header-c0"><!-- 35px -->
                <div class="s-container">
                    <a href="#">f</a>
                    <a href="#">t</a>
                    <a href="#">g</a>
                    <span>|</span>
                    <a href="#">p</a>
                    <a href="#">Login/Daftar</a>
                </div>
            </div>
            <div class="s-header-sprt--white"><!-- 3px -->
                <div class="s-container">
                <div class="s-beauty-line" style="">
                    </div>
                </div>
            </div>
            <div class="s-header-c1"><!-- 81px -->
                <div class="s-container">
                    <a href="#">logo</a>
                    <a href="#">home</a>
                    <a href="#">news</a>
                    <a href="#">bisnis</a>
                    <a href="#">tekno</a>
                    <a href="#">life style</a>
                    <a href="#">sport</a>
                    <a href="#">gallery</a>
                    <a href="#">indeks</a>
                    <a href="#">citizen report</a>
                    <a href="#">search</a>
                </div>
            </div>
            <div class="s-header-sprt--red"><!-- 3px -->
                &nbsp;
            </div>
        </header>





        <!--Content-->
        <!--==================================================-->
        <section class="s-main-content">

            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

            <!-- Adv Left -->
            <!--==================================================-->
            <div class="adv adv-160-600 adv-top-left">

            </div>

            <!-- Adv Right -->
            <!--==================================================-->
            <div class="adv adv-160-600 adv-top-right">

            </div>

            <!-- Big Carousel -->
            <!--==================================================-->
            <div class="s-big-carousel">
                <div id="main-carousel" class="carousel slide" data-ride="carousel" style="height: inherit; position: relative;">

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox" style="height: inherit;">
                        <div class="item active" style="">
                            <?php for($i=0; $i < 3; $i++){ ?>
                            <a href="#" class="s-news-node" style="background: url('https://image.apkpure.com/82/67b970657730f4/com.eweiyin.fengjing-screen-4.jpeg') no-repeat center center; background-size: cover; ">

                                <div class="s-news-info-cnt" style="">
                                <h3>
                                    Technopark Makassar Kolaborasikan Universitas, Pemerintah, dan Swasta
                                </h3>
                                <span class="s-time">
                                    <span class="s-clock-white-filled pull-left"></span>
                                    <span class="pull-left">13 Februari 2016 18:10</span>
                                </span>
                                </div>
                            </a>
                            <?php } ?>
                        </div>
                        <div class="item" style="color: #fff; height: inherit;">
                            <?php for($i=0; $i < 3; $i++){ ?>
                            <a href="#" class="s-news-node" style="background: url('https://s-media-cache-ak0.pinimg.com/736x/16/0b/c7/160bc7a33fff836a3a85950c79a614e9.jpg') no-repeat center center; background-size: cover; ">
                                <!-- <img src=""> -->
                                <div class="s-news-info-cnt" style="">
                                <h3>
                                    Technopark Makassar Kolaborasikan Universitas, Pemerintah, dan Swasta
                                </h3>
                                <span class="s-time">
                                    <span class="s-clock-white-filled pull-left"></span>
                                    <span class="pull-left">13 Februari 2016 18:10</span>
                                </span>
                                </div>
                            </a>
                            <?php } ?>
                        </div>
                        <div class="item" style="color: #fff; height: inherit;">
                            <?php for($i=0; $i < 3; $i++){ ?>
                            <a href="#" class="s-news-node" style="background: url('https://lh4.ggpht.com/t0VAk0yMLwPg2jXCZ5HPMJAKH9Z-1fDcFGciAbQSqbTn3Xd_0OIeXbE7pUV02khWDw=h900') no-repeat center center; background-size: cover; ">
                                <!-- <img src=""> -->
                                <div class="s-news-info-cnt" style="">
                                <h3>
                                    Technopark Makassar Kolaborasikan Universitas, Pemerintah, dan Swasta
                                </h3>
                                <span class="s-time">
                                    <span class="s-clock-white-filled pull-left"></span>
                                    <span class="pull-left">13 Februari 2016 18:10</span>
                                </span>
                                </div>
                            </a>
                            <?php } ?>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="s-left-btn" href="#main-carousel" role="button" data-slide="prev" style="">
                        <img src="../img/icon/big-car-left.png">
                    </a>
                    <a class="s-right-btn" href="#main-carousel" role="button" data-slide="next">
                        <img src="../img/icon/big-car-right.png">
                    </a>
                </div>
            </div>

            <!-- News Ticker -->
            <!--==================================================-->
            <div class="s-news-ticker">
                <span class="s-label">
                    Berita Terkini:
                </span>
                <div class="s-ticker">
                    <div id="ticker-carousel" class="carousel slide" data-ride="carousel" style="height: inherit; position: relative;">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <a href="#" class="item s-ellipsis active ">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </a href="#">
                            <a href="#" class="item s-ellipsis" style="color: #fff; height: inherit; line-height: 38px; text-indent: 16px;">
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
                            </a href="#">
                            <a href="#" class="item s-ellipsis" style="color: #fff; height: inherit; line-height: 38px; text-indent: 16px;">
                                But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.
                            </a href="#">
                        </div>

                        <!-- Controls -->
                        <a class="s-tick" href="#ticker-carousel" role="button" data-slide="prev" style="top: 7px; right: 36px;">
                            <img src="../img/icon/news-tick-left.png">
                        </a>
                        <a class="s-tick" href="#ticker-carousel" role="button" data-slide="next" style="top: 7px; right: 16px;">
                            <img src="../img/icon/news-tick-right.png">
                        </a>
                    </div>
                </div>
            </div>

            <!-- Middel Content -->
            <!--==================================================-->
            <div class="s-col-2-per-3 pull-left" style="/*background: #123;*/">
                <div class="adv adv-790-100">
                    ADVERTISE 790 x 100
                </div>

                he standard Lorem Ipsum passage, used since the 1500s

                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

                Section 1.10.32 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC

                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?"

                1914 translation by H. Rackham

                "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?"
            </div>

            <!-- Middel Content -->
            <!--==================================================-->
            <div class="s-col-1-per-3 pull-right" style="">
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php include('node_berita_pop.php'); ?>
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php include('node_event_terkini.php'); ?>
                <?php for($i=0; $i < 10; $i++){ ?>
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php } ?>
             </div>



        </section>




        <!--Footer-->
        <!--==================================================-->
        <footer>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 backgroundFooter">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="container">
                            <div class="col-md-12 col-sm-12 col-xs-12 font-openSans">
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div class="col-md-3 col-sm-6 col-xs-12 footer-jarakText">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">News</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Peristiwa</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Politik</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Nasional</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Liputan Khusus</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Today in History</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12 footer-jarakText">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Bisnis</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Ekonomi</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Perbankan</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Property</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Otomotif</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Hotel</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12 footer-jarakText">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Lifestyle</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Shopping</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Kuliner</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Travel</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Budaya</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Event</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12 footer-jarakText">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">News</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Peristiwa</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Politik</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Nasional</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Liputan Khusus</a>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                                    <a href="">Today in History</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12 footer-terhubung">
                                            <div class="row">
                                                <div class="col-md-12 footer-logo">
                                                    <img src="../img/icon/logo.png">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 footer-titleBerita">
                                                    Dapatkan Berita Terbaru kami secara Gratis!
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 footer-button">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control field-newsletter font-openSans" placeholder="masukan email anda ...">
                                                        <div class="input-group-btn">
                                                            <button class="btn btn-default btn-subscribed"  type="submit">KIRIM</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 footer-subBerita">
                                                    example@mail.com
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row redHeader">
                <div class="container">
                    <div class="col-md-12 col-sm-12 footer-copyright font-sourceSansPro">
                        <div class="footer-left-section">
                            © 2016 - MAKASSAR TERKINI. All Right Reserved
                            </br>
                            Developed & Maintenance By : PT. iLugroup Multimedia Indonesia
                        </div>

                        <div class="footer-right-section">
                            <ul>
                                <li><a>Contact</a></li>
                                <li><a>Redaksi</a></li>
                                <li><a>Career</a></li>
                                <li><a>Sitemap</a></li>
                                <li><a>Disclaimer</a></li>
                                <li><a>Mobile Version</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>





    </body>
    <script src="../js/jquery-2.1.4.js"></script>
</html>
