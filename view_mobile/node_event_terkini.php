<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/bootstrap.offcanvas.css">

        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Header -->
        <!--==================================================-->
        <header>
            <!-- Begin Navbar -->

        </header>
        <!--Content-->
        <!--==================================================-->
        <section id="content">
            <div class="row backgroundContent">
                <div class="container">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">

                                <div class="news-wrapper-terkini">
                                    <div class="news-wrapper-terkini-header">
                                        <div class="news-wrapper-terkini-left">
                                            EVENT TERKINI
                                        </div>

                                        <div class="news-wrapper-terkini-right">
                                              LIHAT SEMUA <a href="#"><img src="../img/icon/all-window.png"></a>
                                        </div>
                                    </div>

                                    <div class="news-module-ver-terkini">
                                        <div class="news-module-ver-img-terkini">
                                            <img src="../img/produk/kupluk.png">
                                        </div>

                                        <div class="news-module-ver-content-terkini font-sourceSansPro">
                                            <div class="news-module-ver-title-terkini">
                                                Anji Siap Bernostalgia di Makassar
                                            </div>

                                            <div class="news-module-ver-etc-terkini">
                                                <div class="news-module-ver-icon-terkini pull-left">
                                                    <img src="../img/icon/clock-white.png">
                                                </div>

                                                <div class="news-module-ver-time-terkini pull-left">
                                                    13 Februari 2016 13:15
                                                </div>
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="news-module-ver-terkini">
                                        <div class="news-module-ver-img-terkini">
                                            <img src="../img/produk/kupluk.png">
                                        </div>

                                        <div class="news-module-ver-content-terkini font-sourceSansPro">
                                            <div class="news-module-ver-title-terkini">
                                                Anji Siap Bernostalgia di Makassar
                                            </div>

                                            <div class="news-module-ver-etc-terkini">
                                                <div class="news-module-ver-icon-terkini pull-left">
                                                    <img src="../img/icon/clock-white.png">
                                                </div>

                                                <div class="news-module-ver-time-terkini pull-left">
                                                    13 Februari 2016 13:15
                                                </div>
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="news-module-ver-terkini">
                                        <div class="news-module-ver-img-terkini">
                                            <img src="../img/produk/kupluk.png">
                                        </div>

                                        <div class="news-module-ver-content-terkini font-sourceSansPro">
                                            <div class="news-module-ver-title-terkini">
                                                Anji Siap Bernostalgia di Makassar
                                            </div>

                                            <div class="news-module-ver-etc-terkini">
                                                <div class="news-module-ver-icon-terkini pull-left">
                                                    <img src="../img/icon/clock-white.png">
                                                </div>

                                                <div class="news-module-ver-time-terkini pull-left">
                                                    13 Februari 2016 13:15
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </body>    
</html>