<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/bootstrap.offcanvas.css">
        <link rel="stylesheet" href="../css/custom/news-module-hor.css">
    </head>
    <body class="body-offcanvas">
        <!-- Header -->
        <!--==================================================-->
        <header>
            <!-- Begin Navbar -->

        </header>
        <!--Content-->
        <!--==================================================-->
        <section id="content">
            <div class="row backgroundContent">
                <div class="container">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">

                                <div class="news-module-hor">
                                    <div class="news-module-hor-img">
                                        <img src="../img/produk/dummy.jpg">
                                    </div>

                                    <div class="news-module-hor-content font-sourceSansPro">
                                        <div class="news-module-hor-title">
                                            Jejak Sejarah yang Hilang di Makassar
                                        </div>

                                        <div class="news-module-hor-desc">
                                            MAKASSAR – Pasca penangkapan 15 ton ikan berformalin oleh polisi perairan Polda Sulawesi Selatan, Kepala …    
                                        </div>

                                        <div class="news-module-hor-etc">
                                            <div class="news-module-hor-tag pull-left">
                                                SPONSOR
                                                <div class="orange-tooltip">
                                                    <img src="../img/icon/orgTooltip.png">
                                                </div>
                                            </div>

                                            <div class="news-module-hor-info pull-left">
                                                Info kota
                                            </div>

                                            <div class="news-module-hor-icon pull-left">
                                                <img src="../img/icon/clock.png">
                                            </div>

                                            <div class="news-module-hor-time pull-left">
                                                9 menit yang lalu
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="news-module-hor">
                                    <div class="news-module-hor-img">
                                        <img src="../img/produk/dummy.jpg">
                                    </div>

                                    <div class="news-module-hor-content font-sourceSansPro">
                                        <div class="news-module-hor-title">
                                            Jejak Sejarah yang Hilang di Makassar
                                        </div>

                                        <div class="news-module-hor-desc">
                                            MAKASSAR – Pasca penangkapan 15 ton ikan berformalin oleh polisi perairan Polda Sulawesi Selatan, Kepala …    
                                        </div>

                                        <div class="news-module-hor-etc">
                                            <div class="news-module-hor-tag pull-left">
                                                SPONSOR
                                                <div class="orange-tooltip">
                                                    <img src="../img/icon/orgTooltip.png">
                                                </div>
                                            </div>

                                            <div class="news-module-hor-info pull-left">
                                                Info kota
                                            </div>

                                            <div class="news-module-hor-icon pull-left">
                                                <img src="../img/icon/clock.png">
                                            </div>

                                            <div class="news-module-hor-time pull-left">
                                                9 menit yang lalu
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <div class="news-module-hor">
                                    <div class="news-module-hor-img">
                                        <img src="../img/produk/dummy.jpg">
                                    </div>

                                    <div class="news-module-hor-content font-sourceSansPro">
                                        <div class="news-module-hor-title">
                                            Jejak Sejarah yang Hilang di Makassar
                                        </div>

                                        <div class="news-module-hor-desc">
                                            MAKASSAR – Pasca penangkapan 15 ton ikan berformalin oleh polisi perairan Polda Sulawesi Selatan, Kepala …    
                                        </div>

                                        <div class="news-module-hor-etc">
                                            <div class="news-module-hor-tag pull-left">
                                                SPONSOR
                                                <div class="orange-tooltip">
                                                    <img src="../img/icon/orgTooltip.png">
                                                </div>
                                            </div>

                                            <div class="news-module-hor-info pull-left">
                                                Info kota
                                            </div>

                                            <div class="news-module-hor-icon pull-left">
                                                <img src="../img/icon/clock.png">
                                            </div>

                                            <div class="news-module-hor-time pull-left">
                                                9 menit yang lalu
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </body>    
</html>