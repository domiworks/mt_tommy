<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/bootstrap.offcanvas.css">
        <?php include('_css.php'); ?>
        
    </head>
    <body class="body-offcanvas">
        <!-- Header -->
        <!--==================================================-->
        <header>
            <!-- Begin Navbar -->
            <div class="row purpleHeader">
                
            </div>
            <div class="row backgroundHeader">
                
            </div>

        </header>
        <!--Content-->
        <!--==================================================-->
        <section id="content">
            <div class="row backgroundContent">
                <div class="container">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                                <div class="priv-pol-module">
                                    <div class="priv-pol-module-header">

                                        <div class="priv-pol-module-header-stripes">
                                        </div>
                                        <div class="priv-pol-module-header-title">
                                            <p>Disclaimer</p>

                                            <div class="priv-pol-module-header-title-tooltip">
                                                <img src="../img/icon/red-tri.png">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="priv-pol-module-content">
                                        <p>
                                            Seluruh layanan yang diberikan mengikuti aturan main yang berlaku dan ditetapkan oleh makassarterkini.com.

</br></br>Pasal Sanggahan (Disclaimer):

</br></br>makassarterkini.com tidak bertanggung-jawab atas tidak tersampaikannya data/informasi yang disampaikan oleh pembaca melalui berbagai jenis saluran komunikasi (e-mail, sms, online form) karena faktor kesalahan teknis yang tidak diduga-duga sebelumnya

</br></br>makassarterkini.com berhak untuk memuat , tidak memuat, mengedit, dan/atau menghapus data/informasi yang disampaikan oleh pembaca.

</br></br>Data dan/atau informasi yang tersedia di makassarterkini.com hanya sebagai rujukan/referensi belaka, dan tidak diharapkan untuk tujuan perdagangan saham, transaksi keuangan/bisnis maupun transaksi lainnya. Walau berbagai upaya telah dilakukan untuk menampilkan data dan/atau informasi seakurat mungkin, makassarterkini.com dan semua mitra yang menyediakan data dan informasi, termasuk para pengelola halaman konsultasi, tidak bertanggung jawab atas segala kesalahan dan keterlambatan memperbarui data atau informasi, atau segala kerugian yang timbul karena tindakan yang berkaitan dengan penggunaan data/informasi yang disajikan makassarterkini.com.
</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </body>    
</html>