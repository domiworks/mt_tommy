

                <div class="container-fluid">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                                <div class="news-head-kategori">
                                    <img src="../img/icon/berita-kategori.png">

                                    <div class="redspan">
                                        <div class="greyspan">
                                        </div>

                                        <a href="#" class="mini-ls">
                                            LIHAT SEMUA
                                            <img src="../img/icon/all-window-red.png">
                                        </a>
                                    </div>
                                </div>
                                <div class="s-berita-rekomendasi" style="">
                                    <ul class="nav nav-tabs s-pp-nav-right" id="myTab00">
                                        <li class="active" style="width: inherit; display: block;"><a data-target="#m-pa" data-toggle="tab">News</a></li>
                                        <li  style="width: inherit; display: block;"><a data-target="#m-ep" data-toggle="tab">Bisnis</a></li>
                                        <li  style="width: inherit; display: block;"><a data-target="#m-kb" data-toggle="tab">Tekno</a></li>
                                        <li  style="width: inherit; display: block;"><a data-target="#m-kbf" data-toggle="tab">Lifestyle</a></li>
                                        <li  style="width: inherit; display: block;"><a data-target="#m-kbv" data-toggle="tab">Sport</a></li>
                                    </ul>

                                    <div class="tab-content pull-left" style="width: calc(100% - 208px); display: block;">
                                        <div class="tab-pane active" id="m-pa" style="padding-top: 10px; padding-right: 10px;">
                                            <div id="news-kategori-carousel" class="carousel slide" data-ride="carousel" style="height: inherit; position: relative;">
                                                <div class="carousel-inner" role="listbox">
                                                    <div class="item active ">
                                                       <div class="news-module-ver s-news-lg pull-left">
                                                        <div class="news-module-ver-img">
                                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-118900.jpg" style="width: 271px; height: 174px;">
                                                        </div>

                                                        <div class="news-module-ver-content font-sourceSansPro">
                                                            <div class="news-module-ver-title">
                                                                Jejak Sejarah yang Hilang di Makassar
                                                            </div>
                                                            <p>
                                                                MAKASSAR – Petugas Dinas Pekerjaan Umum Kota Makassar mengganti lampu jalan yang berada di jalan Toddopuli 5, Minggu 14 Februari 2016.
                                                            </p>

                                                            <div class="news-module-ver-etc">
                                                                <div class="news-module-ver-icon pull-left">
                                                                    <img src="../img/icon/clock.png">
                                                                </div>

                                                                <div class="news-module-ver-time pull-left">
                                                                    24 Februari 2016 13:15
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                        <?php for($i=0; $i < 3; $i++){ ?>
                                                        <div class="s-news-node s-news-xs pull-right" style="width: 288px; margin-bottom: 10px;">
                                                            <div class="" style="width: 99px; height: 75px; overflow: hidden; float: left;">
                                                                <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-126075.jpg" style="width: 100%; width: 99px; height: 75px;">
                                                            </div>

                                                            <div class="news-module-ver-content" style="width: 188px; float: left; padding-left: 15px;">
                                                                <div class="news-module-ver-title">
                                                                    Jejak Sejarah yang Hilang di Makassar
                                                                </div>

                                                                <div class="news-module-ver-etc">
                                                                    <div class="news-module-ver-icon pull-left">
                                                                        <img src="../img/icon/clock.png" style="margin-top: 0px; margin-left: 0px;">
                                                                    </div>

                                                                    <div class="news-module-ver-time pull-left" style="margin-top: 0px;">
                                                                        24 Februari 2016 13:15
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="item ">
                                                        <div class="news-module-ver s-news-lg pull-left">
                                                            <div class="news-module-ver-img">
                                                                <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-33573.jpg" style="width: 271px; height: 174px;">
                                                            </div>

                                                            <div class="news-module-ver-content font-sourceSansPro">
                                                                <div class="news-module-ver-title">
                                                                    Jejak Sejarah yang Hilang di Makassar
                                                                </div>
                                                                <p>
                                                                    MAKASSAR – Petugas Dinas Pekerjaan Umum Kota Makassar mengganti lampu jalan yang berada di jalan Toddopuli 5, Minggu 14 Februari 2016.
                                                                </p>

                                                                <div class="news-module-ver-etc">
                                                                    <div class="news-module-ver-icon pull-left">
                                                                        <img src="../img/icon/clock.png">
                                                                    </div>

                                                                    <div class="news-module-ver-time pull-left">
                                                                        24 Februari 2016 13:15
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php for($i=0; $i < 3; $i++){ ?>
                                                        <div class="s-news-node s-news-xs pull-right" style="width: 288px; margin-bottom: 10px;">
                                                            <div class="" style="width: 99px; height: 75px; overflow: hidden; float: left;">
                                                                <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-118900.jpg" style="width: 100%; width: 99px; height: 75px;">
                                                            </div>

                                                            <div class="news-module-ver-content" style="width: 188px; float: left; padding-left: 15px;">
                                                                <div class="news-module-ver-title">
                                                                    Jejak Sejarah yang Hilang di Makassar
                                                                </div>

                                                                <div class="news-module-ver-etc">
                                                                    <div class="news-module-ver-icon pull-left">
                                                                        <img src="../img/icon/clock.png" style="margin-top: 0px; margin-left: 0px;">
                                                                    </div>

                                                                    <div class="news-module-ver-time pull-left" style="margin-top: 0px;">
                                                                        24 Februari 2016 13:15
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>

                                            </div>
                                            <div style="position: relative; float: right; text-align: center; margin-top: -4px;">
                                                <a href="#" class="pull-left">
                                                    <span style="font-size: 12px; color: #ec2227;">LIHAT SEMUA</span>
                                                    <img src="../img/icon/all-window-red.png" height="10px">
                                                </a>
                                                <span class="pull-left" style="margin-left: 5px; margin-right: 5px;">atau</span>
                                                <a class="s-tick" href="#news-kategori-carousel" role="button" data-slide="prev" style="top: 7px; right: 36px;">
                                                    <img src="../img/icon/news-tick-left.png" width="12px">
                                                </a>
                                                <a class="s-tick" href="#news-kategori-carousel" role="button" data-slide="next" style="top: 7px; right: 16px;">
                                                    <img src="../img/icon/news-tick-right.png" width="12px">
                                                </a>
                                            </div>


                                        </div>
                                        <div class="tab-pane" id="m-ep" style="padding-top: 10px; padding-right: 10px;">
                                            <div id="bisnis-kategori-carousel" class="carousel slide" data-ride="carousel" style="height: inherit; position: relative;">
                                                <div class="carousel-inner" role="listbox">
                                                    <div class="item active ">
                                                       <div class="news-module-ver s-news-lg pull-left">
                                                        <div class="news-module-ver-img">
                                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-242882.jpg" style="width: 271px; height: 174px;">
                                                        </div>

                                                        <div class="news-module-ver-content font-sourceSansPro">
                                                            <div class="news-module-ver-title">
                                                                Jejak Sejarah yang Hilang di Makassar
                                                            </div>
                                                            <p>
                                                                MAKASSAR – Petugas Dinas Pekerjaan Umum Kota Makassar mengganti lampu jalan yang berada di jalan Toddopuli 5, Minggu 14 Februari 2016.
                                                            </p>

                                                            <div class="news-module-ver-etc">
                                                                <div class="news-module-ver-icon pull-left">
                                                                    <img src="../img/icon/clock.png">
                                                                </div>

                                                                <div class="news-module-ver-time pull-left">
                                                                    24 Februari 2016 13:15
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                        <?php for($i=0; $i < 3; $i++){ ?>
                                                        <div class="s-news-node s-news-xs pull-right" style="width: 288px; margin-bottom: 10px;">
                                                            <div class="" style="width: 99px; height: 75px; overflow: hidden; float: left;">
                                                                <img src="../img/produk/dummy.jpg" style="width: 100%; width: 99px; height: 75px;">
                                                            </div>

                                                            <div class="news-module-ver-content" style="width: 188px; float: left; padding-left: 15px;">
                                                                <div class="news-module-ver-title">
                                                                    Jejak Sejarah yang Hilang di Makassar
                                                                </div>

                                                                <div class="news-module-ver-etc">
                                                                    <div class="news-module-ver-icon pull-left">
                                                                        <img src="../img/icon/clock.png" style="margin-top: 0px; margin-left: 0px;">
                                                                    </div>

                                                                    <div class="news-module-ver-time pull-left" style="margin-top: 0px;">
                                                                        24 Februari 2016 13:15
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="item ">
                                                        <div class="news-module-ver s-news-lg pull-left">
                                                            <div class="news-module-ver-img">
                                                                <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-126075.jpg" style="width: 271px; height: 174px;">
                                                            </div>

                                                            <div class="news-module-ver-content font-sourceSansPro">
                                                                <div class="news-module-ver-title">
                                                                    Jejak Sejarah yang Hilang di Makassar
                                                                </div>
                                                                <p>
                                                                    MAKASSAR – Petugas Dinas Pekerjaan Umum Kota Makassar mengganti lampu jalan yang berada di jalan Toddopuli 5, Minggu 14 Februari 2016.
                                                                </p>

                                                                <div class="news-module-ver-etc">
                                                                    <div class="news-module-ver-icon pull-left">
                                                                        <img src="../img/icon/clock.png">
                                                                    </div>

                                                                    <div class="news-module-ver-time pull-left">
                                                                        24 Februari 2016 13:15
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php for($i=0; $i < 3; $i++){ ?>
                                                        <div class="s-news-node s-news-xs pull-right" style="width: 288px; margin-bottom: 10px;">
                                                            <div class="" style="width: 99px; height: 75px; overflow: hidden; float: left;">
                                                                <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-118900.jpg" style="width: 100%; width: 99px; height: 75px;">
                                                            </div>

                                                            <div class="news-module-ver-content" style="width: 188px; float: left; padding-left: 15px;">
                                                                <div class="news-module-ver-title">
                                                                    Jejak Sejarah yang Hilang di Makassar
                                                                </div>

                                                                <div class="news-module-ver-etc">
                                                                    <div class="news-module-ver-icon pull-left">
                                                                        <img src="../img/icon/clock.png" style="margin-top: 0px; margin-left: 0px;">
                                                                    </div>

                                                                    <div class="news-module-ver-time pull-left" style="margin-top: 0px;">
                                                                        24 Februari 2016 13:15
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>

                                            </div>
                                            <div style="position: relative; float: right; text-align: center; margin-top: -4px;">
                                                <a href="#" class="pull-left">
                                                    <span style="font-size: 12px; color: #ec2227;">LIHAT SEMUA</span>
                                                    <img src="../img/icon/all-window-red.png" height="10px">
                                                </a>
                                                <span class="pull-left" style="margin-left: 5px; margin-right: 5px;">atau</span>
                                                <a class="s-tick" href="#bisnis-kategori-carousel" role="button" data-slide="prev" style="top: 7px; right: 36px;">
                                                    <img src="../img/icon/news-tick-left.png" width="12px">
                                                </a>
                                                <a class="s-tick" href="#bisnis-kategori-carousel" role="button" data-slide="next" style="top: 7px; right: 16px;">
                                                    <img src="../img/icon/news-tick-right.png" width="12px">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="m-kb" style="padding-top: 10px; padding-right: 10px;">
                                            <div id="tekno-kategori-carousel" class="carousel slide" data-ride="carousel" style="height: inherit; position: relative;">
                                                <div class="carousel-inner" role="listbox">
                                                    <div class="item active ">
                                                     <div class="news-module-ver s-news-lg pull-left">
                                                        <div class="news-module-ver-img">
                                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-52206.jpg" style="width: 271px; height: 174px;">
                                                        </div>

                                                        <div class="news-module-ver-content font-sourceSansPro">
                                                            <div class="news-module-ver-title">
                                                                Jejak Sejarah yang Hilang di Makassar
                                                            </div>
                                                            <p>
                                                                MAKASSAR – Petugas Dinas Pekerjaan Umum Kota Makassar mengganti lampu jalan yang berada di jalan Toddopuli 5, Minggu 14 Februari 2016.
                                                            </p>

                                                            <div class="news-module-ver-etc">
                                                                <div class="news-module-ver-icon pull-left">
                                                                    <img src="../img/icon/clock.png">
                                                                </div>

                                                                <div class="news-module-ver-time pull-left">
                                                                    24 Februari 2016 13:15
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php for($i=0; $i < 3; $i++){ ?>
                                                    <div class="s-news-node s-news-xs pull-right" style="width: 288px; margin-bottom: 10px;">
                                                        <div class="" style="width: 99px; height: 75px; overflow: hidden; float: left;">
                                                            <img src="../img/produk/dummy.jpg" style="width: 100%; width: 99px; height: 75px;">
                                                        </div>

                                                        <div class="news-module-ver-content" style="width: 188px; float: left; padding-left: 15px;">
                                                            <div class="news-module-ver-title">
                                                                Jejak Sejarah yang Hilang di Makassar
                                                            </div>

                                                            <div class="news-module-ver-etc">
                                                                <div class="news-module-ver-icon pull-left">
                                                                    <img src="../img/icon/clock.png" style="margin-top: 0px; margin-left: 0px;">
                                                                </div>

                                                                <div class="news-module-ver-time pull-left" style="margin-top: 0px;">
                                                                    24 Februari 2016 13:15
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="item ">
                                                    <div class="news-module-ver s-news-lg pull-left">
                                                        <div class="news-module-ver-img">
                                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-33573.jpg" style="width: 271px; height: 174px;">
                                                        </div>

                                                        <div class="news-module-ver-content font-sourceSansPro">
                                                            <div class="news-module-ver-title">
                                                                Jejak Sejarah yang Hilang di Makassar
                                                            </div>
                                                            <p>
                                                                MAKASSAR – Petugas Dinas Pekerjaan Umum Kota Makassar mengganti lampu jalan yang berada di jalan Toddopuli 5, Minggu 14 Februari 2016.
                                                            </p>

                                                            <div class="news-module-ver-etc">
                                                                <div class="news-module-ver-icon pull-left">
                                                                    <img src="../img/icon/clock.png">
                                                                </div>

                                                                <div class="news-module-ver-time pull-left">
                                                                    24 Februari 2016 13:15
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php for($i=0; $i < 3; $i++){ ?>
                                                    <div class="s-news-node s-news-xs pull-right" style="width: 288px; margin-bottom: 10px;">
                                                        <div class="" style="width: 99px; height: 75px; overflow: hidden; float: left;">
                                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-242882.jpg" style="width: 100%; width: 99px; height: 75px;">
                                                        </div>

                                                        <div class="news-module-ver-content" style="width: 188px; float: left; padding-left: 15px;">
                                                            <div class="news-module-ver-title">
                                                                Jejak Sejarah yang Hilang di Makassar
                                                            </div>

                                                            <div class="news-module-ver-etc">
                                                                <div class="news-module-ver-icon pull-left">
                                                                    <img src="../img/icon/clock.png" style="margin-top: 0px; margin-left: 0px;">
                                                                </div>

                                                                <div class="news-module-ver-time pull-left" style="margin-top: 0px;">
                                                                    24 Februari 2016 13:15
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>

                                            </div>
                                            <div style="position: relative; float: right; text-align: center; margin-top: -4px;">
                                                <a href="#" class="pull-left">
                                                    <span style="font-size: 12px; color: #ec2227;">LIHAT SEMUA</span>
                                                    <img src="../img/icon/all-window-red.png" height="10px">
                                                </a>
                                                <span class="pull-left" style="margin-left: 5px; margin-right: 5px;">atau</span>
                                                <a class="s-tick" href="#tekno-kategori-carousel" role="button" data-slide="prev" style="top: 7px; right: 36px;">
                                                    <img src="../img/icon/news-tick-left.png" width="12px">
                                                </a>
                                                <a class="s-tick" href="#tekno-kategori-carousel" role="button" data-slide="next" style="top: 7px; right: 16px;">
                                                    <img src="../img/icon/news-tick-right.png" width="12px">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="tab-pane" id="m-kbf" style="padding-top: 10px; padding-right: 10px;">
                                            <div id="lifestyle-kategori-carousel" class="carousel slide" data-ride="carousel" style="height: inherit; position: relative;">
                                                <div class="carousel-inner" role="listbox">
                                                    <div class="item active ">
                                                       <div class="news-module-ver s-news-lg pull-left">
                                                        <div class="news-module-ver-img">
                                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-242882.jpg" style="width: 271px; height: 174px;">
                                                        </div>

                                                        <div class="news-module-ver-content font-sourceSansPro">
                                                            <div class="news-module-ver-title">
                                                                Jejak Sejarah yang Hilang di Makassar
                                                            </div>
                                                            <p>
                                                                MAKASSAR – Petugas Dinas Pekerjaan Umum Kota Makassar mengganti lampu jalan yang berada di jalan Toddopuli 5, Minggu 14 Februari 2016.
                                                            </p>

                                                            <div class="news-module-ver-etc">
                                                                <div class="news-module-ver-icon pull-left">
                                                                    <img src="../img/icon/clock.png">
                                                                </div>

                                                                <div class="news-module-ver-time pull-left">
                                                                    24 Februari 2016 13:15
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                        <?php for($i=0; $i < 3; $i++){ ?>
                                                        <div class="s-news-node s-news-xs pull-right" style="width: 288px; margin-bottom: 10px;">
                                                            <div class="" style="width: 99px; height: 75px; overflow: hidden; float: left;">
                                                                <img src="../img/produk/dummy.jpg" style="width: 100%; width: 99px; height: 75px;">
                                                            </div>

                                                            <div class="news-module-ver-content" style="width: 188px; float: left; padding-left: 15px;">
                                                                <div class="news-module-ver-title">
                                                                    Jejak Sejarah yang Hilang di Makassar
                                                                </div>

                                                                <div class="news-module-ver-etc">
                                                                    <div class="news-module-ver-icon pull-left">
                                                                        <img src="../img/icon/clock.png" style="margin-top: 0px; margin-left: 0px;">
                                                                    </div>

                                                                    <div class="news-module-ver-time pull-left" style="margin-top: 0px;">
                                                                        24 Februari 2016 13:15
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="item ">
                                                        <div class="news-module-ver s-news-lg pull-left">
                                                            <div class="news-module-ver-img">
                                                                <img src="../img/produk/dummy.jpg" style="width: 271px; height: 174px;">
                                                            </div>

                                                            <div class="news-module-ver-content font-sourceSansPro">
                                                                <div class="news-module-ver-title">
                                                                    Jejak Sejarah yang Hilang di Makassar
                                                                </div>
                                                                <p>
                                                                    MAKASSAR – Petugas Dinas Pekerjaan Umum Kota Makassar mengganti lampu jalan yang berada di jalan Toddopuli 5, Minggu 14 Februari 2016.
                                                                </p>

                                                                <div class="news-module-ver-etc">
                                                                    <div class="news-module-ver-icon pull-left">
                                                                        <img src="../img/icon/clock.png">
                                                                    </div>

                                                                    <div class="news-module-ver-time pull-left">
                                                                        24 Februari 2016 13:15
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php for($i=0; $i < 3; $i++){ ?>
                                                        <div class="s-news-node s-news-xs pull-right" style="width: 288px; margin-bottom: 10px;">
                                                            <div class="" style="width: 99px; height: 75px; overflow: hidden; float: left;">
                                                                <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-118900.jpg" style="width: 100%; width: 99px; height: 75px;">
                                                            </div>

                                                            <div class="news-module-ver-content" style="width: 188px; float: left; padding-left: 15px;">
                                                                <div class="news-module-ver-title">
                                                                    Jejak Sejarah yang Hilang di Makassar
                                                                </div>

                                                                <div class="news-module-ver-etc">
                                                                    <div class="news-module-ver-icon pull-left">
                                                                        <img src="../img/icon/clock.png" style="margin-top: 0px; margin-left: 0px;">
                                                                    </div>

                                                                    <div class="news-module-ver-time pull-left" style="margin-top: 0px;">
                                                                        24 Februari 2016 13:15
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>

                                                </div>
                                                <div style="position: relative; float: right; text-align: center; margin-top: -4px;">
                                                    <a href="#" class="pull-left">
                                                        <span style="font-size: 12px; color: #ec2227;">LIHAT SEMUA</span>
                                                        <img src="../img/icon/all-window-red.png" height="10px">
                                                    </a>
                                                    <span class="pull-left" style="margin-left: 5px; margin-right: 5px;">atau</span>
                                                    <a class="s-tick" href="#lifestyle-kategori-carousel" role="button" data-slide="prev" style="top: 7px; right: 36px;">
                                                        <img src="../img/icon/news-tick-left.png" width="12px">
                                                    </a>
                                                    <a class="s-tick" href="#lifestyle-kategori-carousel" role="button" data-slide="next" style="top: 7px; right: 16px;">
                                                        <img src="../img/icon/news-tick-right.png" width="12px">
                                                    </a>
                                                </div>
                                        </div>
                                        <div class="tab-pane" id="m-kbv" style="padding-top: 10px; padding-right: 10px;">

                                            <div id="sport-kategori-carousel" class="carousel slide" data-ride="carousel" style="height: inherit; position: relative;">
                                                <div class="carousel-inner" role="listbox">
                                                    <div class="item active ">
                                                       <div class="news-module-ver s-news-lg pull-left">
                                                        <div class="news-module-ver-img">
                                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-118900.jpg" style="width: 271px; height: 174px;">
                                                        </div>

                                                        <div class="news-module-ver-content font-sourceSansPro">
                                                            <div class="news-module-ver-title">
                                                                Jejak Sejarah yang Hilang di Makassar
                                                            </div>
                                                            <p>
                                                                MAKASSAR – Petugas Dinas Pekerjaan Umum Kota Makassar mengganti lampu jalan yang berada di jalan Toddopuli 5, Minggu 14 Februari 2016.
                                                            </p>

                                                            <div class="news-module-ver-etc">
                                                                <div class="news-module-ver-icon pull-left">
                                                                    <img src="../img/icon/clock.png">
                                                                </div>

                                                                <div class="news-module-ver-time pull-left">
                                                                    24 Februari 2016 13:15
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                        <?php for($i=0; $i < 3; $i++){ ?>
                                                        <div class="s-news-node s-news-xs pull-right" style="width: 288px; margin-bottom: 10px;">
                                                            <div class="" style="width: 99px; height: 75px; overflow: hidden; float: left;">
                                                                <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-38480.jpg" style="width: 100%; width: 99px; height: 75px;">
                                                            </div>

                                                            <div class="news-module-ver-content" style="width: 188px; float: left; padding-left: 15px;">
                                                                <div class="news-module-ver-title">
                                                                    Jejak Sejarah yang Hilang di Makassar
                                                                </div>

                                                                <div class="news-module-ver-etc">
                                                                    <div class="news-module-ver-icon pull-left">
                                                                        <img src="../img/icon/clock.png" style="margin-top: 0px; margin-left: 0px;">
                                                                    </div>

                                                                    <div class="news-module-ver-time pull-left" style="margin-top: 0px;">
                                                                        24 Februari 2016 13:15
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="item ">
                                                        <div class="news-module-ver s-news-lg pull-left">
                                                            <div class="news-module-ver-img">
                                                                <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-33573.jpg" style="width: 271px; height: 174px;">
                                                            </div>

                                                            <div class="news-module-ver-content font-sourceSansPro">
                                                                <div class="news-module-ver-title">
                                                                    Jejak Sejarah yang Hilang di Makassar
                                                                </div>
                                                                <p>
                                                                    MAKASSAR – Petugas Dinas Pekerjaan Umum Kota Makassar mengganti lampu jalan yang berada di jalan Toddopuli 5, Minggu 14 Februari 2016.
                                                                </p>

                                                                <div class="news-module-ver-etc">
                                                                    <div class="news-module-ver-icon pull-left">
                                                                        <img src="../img/icon/clock.png">
                                                                    </div>

                                                                    <div class="news-module-ver-time pull-left">
                                                                        24 Februari 2016 13:15
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php for($i=0; $i < 3; $i++){ ?>
                                                        <div class="s-news-node s-news-xs pull-right" style="width: 288px; margin-bottom: 10px;">
                                                            <div class="" style="width: 99px; height: 75px; overflow: hidden; float: left;">
                                                                <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-33574.jpg" style="width: 100%; width: 99px; height: 75px;">
                                                            </div>

                                                            <div class="news-module-ver-content" style="width: 188px; float: left; padding-left: 15px;">
                                                                <div class="news-module-ver-title">
                                                                    Jejak Sejarah yang Hilang di Makassar
                                                                </div>

                                                                <div class="news-module-ver-etc">
                                                                    <div class="news-module-ver-icon pull-left">
                                                                        <img src="../img/icon/clock.png" style="margin-top: 0px; margin-left: 0px;">
                                                                    </div>

                                                                    <div class="news-module-ver-time pull-left" style="margin-top: 0px;">
                                                                        24 Februari 2016 13:15
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>

                                            </div>
                                            <div style="position: relative; float: right; text-align: center; margin-top: -4px;">
                                                <a href="#" class="pull-left">
                                                    <span style="font-size: 12px; color: #ec2227;">LIHAT SEMUA</span>
                                                    <img src="../img/icon/all-window-red.png" height="10px">
                                                </a>
                                                <span class="pull-left" style="margin-left: 5px; margin-right: 5px;">atau</span>
                                                <a class="s-tick" href="#sport-kategori-carousel" role="button" data-slide="prev" style="top: 7px; right: 36px;">
                                                    <img src="../img/icon/news-tick-left.png" width="12px">
                                                </a>
                                                <a class="s-tick" href="#sport-kategori-carousel" role="button" data-slide="next" style="top: 7px; right: 16px;">
                                                    <img src="../img/icon/news-tick-right.png" width="12px">
                                                </a>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                        </div>
                    </div>
                </div>
