<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/bootstrap.offcanvas.css">
        <?php include('_css.php'); ?>
        <script src="../js/gmaps/gmaps.js"></script>
        
    </head>
    <body class="body-offcanvas">
        <!-- Header -->
        <!--==================================================-->
        <header>
            <!-- Begin Navbar -->
            <div class="row purpleHeader">
                
            </div>
            <div class="row backgroundHeader">
                
            </div>

        </header>
        <!--Content-->
        <!--==================================================-->
        <section id="content">
            <div class="row backgroundContent">
                <div class="container">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                                <div class="kontak-module">
                                    <div class="kontak-module-header">

                                        <div class="kontak-module-header-stripes">
                                        </div>
                                        <div class="kontak-module-header-title">
                                            <p>Kontak</p>

                                            <div class="kontak-module-header-title-tooltip">
                                                <img src="../img/icon/red-tri.png">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="kontak-map">

                                    </div>

                                    <div class="kontak-module-content">
                                        <div class="kontak-module-info">
                                            <div class="kontak-info-title">
                                                Makassar Terkini
                                            </div>

                                            <div class="kontak-info-content">
                                                <div class="kontak-info-content">
                                                    <img src="../img/icon/loc.png">

                                                    <div class="kontak-info-detail">
                                                        Jl. A. Mappaodang No 8 Makassar 90133
                                                    </div>    
                                                </div>

                                                <div class="kontak-info-content">
                                                    <img src="../img/icon/call.png">

                                                    <div class="kontak-info-detail">
                                                        Telp. 0411-8112525
                                                        </br>Fax: 0411 871983
                                                    </div>    
                                                </div>

                                                <div class="kontak-info-content">
                                                    <img src="../img/icon/mail.png">

                                                    <div class="kontak-info-detail">
                                                        info@makassarterkini.com
                                                        </br>iklan@makassarterkini.com
                                                        </br>makassar.terkini@yahoo.co.id
                                                    </div>    
                                                </div>

                                                <div class="kontak-info-content">
                                                    <img src="../img/icon/fb2.png">

                                                    <div class="kontak-info-detail">
                                                        Makassar Terkini
                                                    </div>    
                                                </div>

                                                <div class="kontak-info-content">
                                                    <img src="../img/icon/twitter.png">

                                                    <div class="kontak-info-detail">
                                                        @makassarterkini
                                                    </div>    
                                                </div>
                                            </div>
                                        </div> 

                                        <div class="kontak-module-form-wrapper">
                                            <div class="kontak-module-form">
                                                <div class="kontak-form-nama">
                                                    <span>Nama Anda*</span>
                                                    <input type="text">
                                               </div>

                                               <div class="kontak-form-nama">
                                                    <span>Email Anda*</span>
                                                    <input type="text">
                                               </div>
                                            </div> 

                                            <div class="kontak-form-msg">
                                                <span>Judul Pesan</span>
                                                <input type="text">
                                           </div>

                                           <div class="kontak-form-msg">
                                                <span>Isi Pesan</span>
                                                <textarea></textarea>
                                           </div>
                                        </div>

                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </body>    
</html>