<!DOCTYPE html>
<html>
    <head>
        <title>Kategori Bisnis</title>
        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Advertisement Top -->
        <!--==================================================-->
        <!-- Header -->
        <!--==================================================-->
        <?php include('_header.php'); ?>


        <!--Content-->
        <!--==================================================-->
        <section class="s-main-content">

            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

            <!-- Adv Left -->
            <!--==================================================-->
            <div class="adv adv-160-600 adv-top-left">

            </div>

            <!-- Adv Right -->
            <!--==================================================-->
            <div class="adv adv-160-600 adv-top-right">

            </div>

            <!-- Middel Content -->
            <!--==================================================-->
            <div class="s-col-2-per-3 pull-left f-content-area" style="/*background: #123;*/">
                <?php include('node_pencarian_berita.php'); ?>
                <ul class="ui-pagination">
                    <li class="prev">
                        <a href="">
                            <span class="fa fa-chevron-left" style=""></span>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            3
                        </a>
                    </li>
                    <li>
                        <a href="">
                            4
                        </a>
                    </li>
                    <li class="active">
                        <a href="">
                            5
                        </a>
                    </li>
                    <li>
                        <a href="">
                            6
                        </a>
                    </li>
                    <li>
                        <a href="">
                            7
                        </a>
                    </li>
                    <li class="hidden-xs">
                        <a href="">
                            ...
                        </a>
                    </li>
                    <li class="hidden-xs">
                        <a href="">
                            n-1
                        </a>
                    </li>
                    <li class="hidden-xs">
                        <a href="">
                            n
                        </a>
                    </li>
                    <li class="next">
                        <a href="">
                            <span class="fa fa-chevron-right" style=""></span>
                        </a>
                    </li>
                </ul>
            </div>

            <!-- Middel Content -->
            <!--==================================================-->
            <div class="s-col-1-per-3 pull-right f-extra-area" style="">
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php include('node_berita_pop.php'); ?>
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php include('node_event_terkini.php'); ?>
                <?php for($i=0; $i < 1; $i++){ ?>
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php } ?>
            </div>
            <span class="clearfix"></span>
            <!-- <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div> -->



        </section>




        <!--Footer-->
        <!--==================================================-->
        <?php include('_footer.php'); ?>





    </body>
</html>
