<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Advertisement Top -->
        <!--==================================================-->
        <!-- Header -->
        <!--==================================================-->
        <?php include('_header.php'); ?>
        <script src="http://maps.googleapis.com/maps/api/js"></script>
        <script>
            function initialize() {
                var mapProp = {
                    center:new google.maps.LatLng(51.508742,-0.120850),
                    zoom:5,
                    mapTypeId:google.maps.MapTypeId.ROADMAP
                };
                var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
            }
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>


        <!--Content-->
        <!--==================================================-->
        <section class="s-main-content">

            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

            <div class="container-fluid">
                <div class="col-sm-12">
                    <div class="pull-left" style="width: 762px; margin-bottom: 20px;">
                        <div style="width: 100%; background: #f2f4f6; height: 34px; display: block; overflow: hidden; margin-bottom: 15px;">
                            <div class="pull-left">
                                <img src="../img/icon/detail-event-red-date.png" class="pull-left" >
                                <span style="margin-left: 10px; line-height: 34px;">Sayyang Pattuddu</span>
                            </div>
                            <div class="pull-right">
                                <img src="../img/icon/red-pin.png" class="pull-left" style="margin-top: 9px;">
                                <span style="margin-left: 10px; line-height: 34px; margin-right: 15px;">Jalan Poros Makassar - Palu Km. 123 Mamuju</span>
                            </div>
                        </div>

                        <div style="margin-bottom: 10px; overflow: hidden;">
                            <img class="the-choosen-one" src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-377352.jpg" width="100%" height="479px">
                        </div>
                        <div id="foto-populer-carousel" class="carousel slide" data-ride="carousel" style="height: inherit; position: relative;">
                        <a class="s-tick" href="#foto-populer-carousel" role="button" data-slide="prev" style="top: 29px; position: absolute; left: 0px; z-index: 1;">
                                <img src="../img/icon/big-car-left.png" width="33px">
                            </a>
                            <a class="s-tick" href="#foto-populer-carousel" role="button" data-slide="next" style="top: 29px; position: absolute; right: 0px; z-index: 1;">
                                <img src="../img/icon/big-car-right.png" width="33px">
                            </a>
                            <div class="carousel-inner" role="listbox">
                                <div class="item active " style="text-align: justify;"><!-- open state -->
                                    <img class="image-chooser" src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-377352.jpg" style="width: 146px; height: 92px; margin-left: 5px; float: left; border-radius: 0px;">
                                    <img class="image-chooser" src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-377353.jpg" style="width: 146px; height: 92px; margin-left: 5px; float: left; border-radius: 0px;">
                                    <img class="image-chooser" src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-377358.jpg" style="width: 146px; height: 92px; margin-left: 5px; float: left; border-radius: 0px;">
                                    <img class="image-chooser" src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-377346.jpg" style="width: 146px; height: 92px; margin-left: 5px; float: left; border-radius: 0px;">
                                    <img class="image-chooser" src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-377338.jpg" style="width: 146px; height: 92px; margin-left: 5px; float: left; border-radius: 0px;">
                                </div><!-- setiap looping 5x img echo baris ini -->
                                <div class="item " style="text-align: justify;"><!-- setiap looping 5x img echo baris ini -->
                                    <img class="image-chooser" src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-377352.jpg" style="width: 146px; height: 92px; margin-left: 5px; float: left; border-radius: 0px;">
                                    <img class="image-chooser" src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-377353.jpg" style="width: 146px; height: 92px; margin-left: 5px; float: left; border-radius: 0px;">
                                    <img class="image-chooser" src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-377358.jpg" style="width: 146px; height: 92px; margin-left: 5px; float: left; border-radius: 0px;">
                                    <img class="image-chooser" src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-377346.jpg" style="width: 146px; height: 92px; margin-left: 5px; float: left; border-radius: 0px;">
                                    <img class="image-chooser" src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-377338.jpg" style="width: 146px; height: 92px; margin-left: 5px; float: left; border-radius: 0px;">
                                </div><!-- close state -->
                            </div>

                        </div>
                        <script type="text/javascript">

                        $(document).on('click', '.image-chooser', function(){
                            $('.the-choosen-one').attr('src', $(this).attr('src'));
                        })
                        </script>




                    </div>
                    <div class="pull-right" style="width: 320px;">
                        <div style="width: 100%; background: #2e3092; height: 34px; display: block; overflow: hidden; margin-bottom: 15px;">
                            <span style="margin-left: 10px; line-height: 34px; color: #fff;">Lokasi Kegiatan</span>
                        </div>
                        <div id="googleMap" style="width:320px; height:338px; margin-bottom: 15px;"></div>
                        <div style="background: #f4f5f6; margin-bottom: 15px; border-top: 1px solid #1cc4be; padding: 13px;">
                            <strong>Bagikan Ke:</strong>
                            <div class="pull-right">
                                <a href="#">
                                    <img src="../img/icon/event-fb.png" class="pull-left" style="margin-left: 5px;">
                                </a>
                                <a href="#">
                                    <img src="../img/icon/event-twitter.png" class="pull-left" style="margin-left: 5px;">
                                </a>
                                <a href="#">
                                    <img src="../img/icon/event-gplus.png" class="pull-left" style="margin-left: 5px;">
                                </a>
                                <a href="#">
                                    <img src="../img/icon/event-path.png" class="pull-left" style="margin-left: 5px;">
                                </a>
                            </div>
                        </div>
                        <div style="background: #f4f5f6; margin-bottom: 15px; border-top: 1px solid #1cc4be; overflow: hidden;">
                            <strong class="pull-left" style=" padding: 13px;">Penilaian<br/>Pengunjung</strong>
                            <div class="pull-right" style="margin-right: 13px;">
                                <span style="color: #303986; font-weight: 700; font-size: 32px;">6.0</span><span style="color: #303986; font-weight: 700; font-size: 18px;">/10</span>
                                <span class="clearfix"></span>
                                <span style="color: #303986;">Lumayan</span>
                            </div>
                        </div>
                    </div>
                    <span class="clearfix"></span>


                    <div class="s-fancy-header-red">
                        <span class="s-title">Deskripsi Kegiatan</span>
                    </div>
                    <span class="sub-title-event">SAYYANG PATTUDDUK</span>
                    <span class="clearfix"></span>
                    Sayyang pattudu (kuda menari) atau kadang orang menyebutnya sebgai to messawe (orang yang mengendarai) merupakan acara  yang diadakan dalam rangka untuk mensyukuri anak-anak yang khatam (tamat) Alquran. Bagi suku Mandar di Sulawesi Barat tamat Alquran adalah sesuatu yang sangat istimewa, dan perlu disyukuri secara khusus dengan mengadakan pesta adat sayyang pattudu. Pesta ini diadakan sekali dalam setahun, biasanya bertepatan dengan bulan Maulid/Rabiul Awwal (kalender hijriyah). Dalam pesta tersebut menampilkan atraksi kuda berhias yang menari sembari ditunggangi anak-anak yang sedang mengikuti acara tersebut.
                    <br/>
                    <br/>
                    Bagi masyarakat Mandar, khatam Alquran dan upacara adat sayyang pattudu memiliki pertalian yang sangat erat antara yang satu dengan yang lainnya. Acara ini mereka tetap lestarikan dengan baik. Bahkan masyarakat suku mandar yang berdiam di luar Sulawesi Barat akan kembali kekampung halamannya demi mengikuti acara tersebut. Penyelenggaraan acara ini sudah berlangsung lama, tapi tidak ada yang tahu pasti kapan acara ini diadakan pertama kali. Jejak sejarah yang menunjukkan awal pelaksanaan dari kegiatan ini belum terdeteksi oleh para tokoh masyarakat dan para sejarawan.
                </div>
            </div>


            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

        </section>




        <!--Footer-->
        <!--==================================================-->
        <?php include('_footer.php'); ?>
        <style type="text/css">
            #chartdiv {
                width       : 100%;
                height      : 500px;
                font-size   : 11px;
            }
        </style>
        <script type="text/javascript">
            var chart = AmCharts.makeChart( "chartdiv", {
              "type": "pie",
              "theme": "light",
              "dataProvider": [ {
                "country": "Lithuania",
                "litres": 501.9
            }, {
                "country": "Czech Republic",
                "litres": 301.9
            }, {
                "country": "Ireland",
                "litres": 201.1
            }, {
                "country": "Germany",
                "litres": 165.8
            }, {
                "country": "Australia",
                "litres": 139.9
            }, {
                "country": "Austria",
                "litres": 128.3
            }, {
                "country": "UK",
                "litres": 99
            }, {
                "country": "Belgium",
                "litres": 60
            }, {
                "country": "The Netherlands",
                "litres": 50
            } ],
            "valueField": "litres",
            "titleField": "country",
            "balloon":{
             "fixedPosition":true
         },
         "export": {
            "enabled": true
        }
    } );
</script>

    </body>
</html>
