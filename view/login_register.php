<!DOCTYPE html>
<html>
    <head>
        <title>Login Register</title>
        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Advertisement Top -->
        <!--==================================================-->
        <!-- Header -->
        <!--==================================================-->
        <?php include('_header.php'); ?>



        <!--Content-->
        <!--==================================================-->


        <section class="s-main-content" style="overflow: hidden;">
                <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

            <div class="row backgroundContent" style="margin-bottom: 10px;">
                <div class="s-container">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                                <div class="log-reg-module">
                                    <div class="log-reg-pretext">
                                        <div class="log-reg-pretext-title">
                                            Terima Kasih telah mendaftar
                                        </div>

                                        <div class="log-reg-pretext-content">
                                            Silahkan cek email Anda. Email validasi telah Kami kirimkan pada alamat email yang Anda Masukkan,lakukan konfirmasi dengan cara klik link yang tersedia.
                                            </br><a href="#">Kirim ulang</a>, jika tidak menerima email dari Kami
                                        </div>
                                    </div>

                                    <div class="log-reg-midwrap">
                                        <div class="log-reg-left">
                                            <div class="log-reg-title">
                                                LOGIN PAGE
                                            </div>

                                            <div class="log-reg-subtitle">
                                                Silahkan Login Untuk Mendapatkan Beragam Fasilitas
                                            </div>

                                            <div class="log-reg-logbar">
                                                <input type="text" placeholder="Email / Username">
                                                <img src="../img/icon/gembok.png">
                                            </div>

                                            <div class="log-reg-logbar">
                                                <input type="password" placeholder="Password">
                                                <img src="../img/icon/profile.png">
                                            </div>

                                            <!-- MEJIK -->

                                             <div class="log-reg-misc">
                                                 <div class="warning">
                                                    <div class="log-reg-misc-left">
                                                        <a href="#"> Lupa password?</a>
                                                    </div>

                                                    <div class="log-reg-misc-right">
                                                        <a href="#"> Tetap masuk</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="log-reg-button-long-0">
                                                <button> LOGIN </button>
                                            </div>

                                            <div class="log-reg-atau">
                                                <img src="../img/icon/ataw.png">
                                            </div>

                                            <div class="log-reg-button-long">
                                                <button style="background-color: #294D80;"> Login Facebook </button>
                                                <img src="../img/icon/fb.png">
                                            </div>

                                            <div class="log-reg-button-long">
                                                <button style="background-color: #870206;"> Login Google+ </button>
                                                <img src="../img/icon/gplus.png">
                                            </div>
                                        </div>

                                        <div class="log-reg-right">
                                            <div class="log-reg-title">
                                                DAFTAR
                                            </div>

                                            <div class="log-reg-subtitle">
                                                Daftar akun makassarterkini.com untuk memulai.
                                            </div>

                                            <div class="log-reg-toReg">
                                                <div class="log-reg-subtitle">
                                                    Masukkan email Anda!
                                                </div>

                                                <div class="log-reg-regbar">
                                                    <input type="text" placeholder="Example:useranda@gmail.com">
                                                </div>

                                                <div class="log-reg-button">
                                                    <button> DAFTAR </button>
                                                    <div class="log-reg-button-img">
                                                        <img src="../img/icon/send.png">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>
        </section>




        <!--Footer-->
        <!--==================================================-->
        <?php include('_footer.php'); ?>






    </body>
</html>
