<!DOCTYPE html>
<html>
    <head>
        <title>Profile Member</title>
        <?php include('_css.php'); ?>
        <style type="text/css">
        .form-horizontal .form-group {
            margin-left: 0px;
            margin-right: 0px;
        }
        .tab-pane {
            position: relative;
        }
        div.upload {
            width: 100px;
            height: 38px;
            background: url(https://lh6.googleusercontent.com/-dqTIJRTqEAQ/UJaofTQm3hI/AAAAAAAABHo/w7ruR1SOIsA/s157/upload.png) no-repeat;
            background-size: 100px 38px;
            overflow: hidden;
        }
        div.upload input {
            display: block !important;
            width: 100px !important;
            height: 38px !important;
            opacity: 0 !important;
            overflow: hidden !important;
        }
        </style>
    </head>
    <body class="body-offcanvas">
        <!-- Advertisement Top -->
        <!--==================================================-->
        <!-- Header -->
        <!--==================================================-->
        <?php include('_header.php'); ?>


        <!--Content-->
        <!--==================================================-->
        <section class="s-main-content">

        <div class="s-cover-picture-cnt" style="background: url('https://www.limexb360.co.uk/media//backgroundimages/g/a/galaxy-wallpapers-10.jpg') no-repeat center center; background-size: cover; ">

            <div class="s-profile-cnt">
            <div class="pull-left btn btn-default s-cc-btn" style="">
                    Change Cover
                </div>

                <h2 class="s-pn pull-right" style="">
                    Username Usermeh
                </h2>
                <div class="s-pp" style="background: url('http://lawsondentistry.com/wp-content/uploads/blog/2013/10/Smiling-Guy-Beard.jpg') no-repeat center center; background-size: cover; "></div>
            </div>
        </div>
        <div style="width: 100%; height: auto; display: block; margin-top: 30px; margin-bottom: 30px; overflow: hidden;">
            <ul class="nav nav-tabs s-pp-nav-left-profile" id="myTab00" style="float: left; width: 252px;">
                <li class="active" style="width: inherit; display: block;"><a data-target="#m-pa" data-toggle="tab" style="">Profil Anda</a></li>
                <li  style="width: inherit; display: block;"><a data-target="#m-ep" data-toggle="tab" style="">Edit Profil</a></li>
                <li  style="width: inherit; display: block;"><a data-target="#m-kb" data-toggle="tab" style="">Kirim Berita</a></li>
                <li  style="width: inherit; display: block;"><a data-target="#m-kbf" data-toggle="tab" style="">Kirim Berita Foto</a></li>
                <li  style="width: inherit; display: block;"><a data-target="#m-kbv" data-toggle="tab" style="">Kirim Berita Video</a></li>
            </ul>

            <div class="tab-content pull-left" style="">
                <div class="tab-pane active" id="m-pa">
                    <div class="s-container-573" style="">
                        <ul class="nav nav-tabs s-pp-nav-pa" id="myTab">
                            <li><a data-target="#profile" data-toggle="tab">Profil</a></li>
                            <li><a data-target="#artikel" data-toggle="tab">Artikel</a></li>
                            <li><a data-target="#settings" data-toggle="tab">Foto</a></li>
                            <li><a data-target="#video" data-toggle="tab">Video</a></li>
                        </ul>

                        <div class="tab-content s-profile-user">
                            <div class="tab-pane active" id="profile">
                                <div class="form-group">
                                    <label>Username</label>
                                    <p class="form-control-static">Someone</p>
                                </div>
                                <div class="form-group">
                                    <label>Email</label>
                                    <p class="form-control-static">someone@somehow.com</p>
                                </div>
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <p class="form-control-static">Someone Somehow</p>
                                </div>
                                <div class="form-group">
                                    <label>Display Name</label>
                                    <p class="form-control-static">Someone Somehow</p>
                                </div>
                                <div class="form-group">
                                    <label>Short Bio</label>
                                    <p class="form-control-static">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                </div>
                                <div class="form-group">
                                    <label>Birthday</label>
                                    <p class="form-control-static">01-01-1990</p>
                                </div>
                                <div class="form-group">
                                    <label>Jenis Kelamin</label>
                                    <p class="form-control-static">Pria</p>
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <p class="form-control-static">09876321</p>
                                </div>
                            </div>
                            <div class="tab-pane" id="artikel">
                                <div class="s-berita-terkini-container">
                                    <a href="#" class="news-module-hor">
                                        <div class="news-module-hor-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-375124.jpg">
                                        </div>

                                        <div class="news-module-hor-content font-sourceSansPro" style="width: 312px; padding-top: 0px;">
                                            <div class="news-module-hor-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>

                                            <div class="news-module-hor-desc">
                                                MAKASSAR – Pasca penangkapan 15 ton ikan berformalin oleh polisi perairan Polda Sulawesi Selatan, Kepala …
                                            </div>

                                            <div class="news-module-hor-etc">
                                                <div class="news-module-hor-tag pull-left">
                                                    SPONSOR
                                                    <div class="orange-tooltip">
                                                        <img src="../img/icon/orgTooltip.png">
                                                    </div>
                                                </div>

                                                <div class="news-module-hor-info pull-left">
                                                    Info kota
                                                </div>

                                                <div class="news-module-hor-icon pull-left">
                                                    <img src="../img/icon/clock.png">
                                                </div>

                                                <div class="news-module-hor-time pull-left">
                                                    9 menit yang lalu
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                    <a href="#" class="news-module-hor">
                                        <div class="news-module-hor-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-375123.jpg">
                                        </div>

                                        <div class="news-module-hor-content font-sourceSansPro" style="width: 312px; padding-top: 0px;">
                                            <div class="news-module-hor-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>

                                            <div class="news-module-hor-desc">
                                                MAKASSAR – Pasca penangkapan 15 ton ikan berformalin oleh polisi perairan Polda Sulawesi Selatan, Kepala …
                                            </div>

                                            <div class="news-module-hor-etc">
                                                <div class="news-module-hor-tag pull-left">
                                                    SPONSOR
                                                    <div class="orange-tooltip">
                                                        <img src="../img/icon/orgTooltip.png">
                                                    </div>
                                                </div>

                                                <div class="news-module-hor-info pull-left">
                                                    Info kota
                                                </div>

                                                <div class="news-module-hor-icon pull-left">
                                                    <img src="../img/icon/clock.png">
                                                </div>

                                                <div class="news-module-hor-time pull-left">
                                                    9 menit yang lalu
                                                </div>
                                            </div>
                                        </div>
                                    </a>

                                    <a href="#" class="news-module-hor">
                                        <div class="news-module-hor-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-375122.jpg">
                                        </div>

                                        <div class="news-module-hor-content font-sourceSansPro" style="width: 312px; padding-top: 0px;">
                                            <div class="news-module-hor-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>

                                            <div class="news-module-hor-desc">
                                                MAKASSAR – Pasca penangkapan 15 ton ikan berformalin oleh polisi perairan Polda Sulawesi Selatan, Kepala …
                                            </div>

                                            <div class="news-module-hor-etc">
                                                <div class="news-module-hor-tag pull-left">
                                                    SPONSOR
                                                    <div class="orange-tooltip">
                                                        <img src="../img/icon/orgTooltip.png">
                                                    </div>
                                                </div>

                                                <div class="news-module-hor-info pull-left">
                                                    Info kota
                                                </div>

                                                <div class="news-module-hor-icon pull-left">
                                                    <img src="../img/icon/clock.png">
                                                </div>

                                                <div class="news-module-hor-time pull-left">
                                                    9 menit yang lalu
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="tab-pane" id="settings">
                            <div class="s-video-terkini-container s-foto-video-profile" style="text-align: justify;">
                                    <div class="news-module-ver">
                                        <div class="news-module-ver-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-37097.jpg">
                                            <span class="foto-count">7</span>
                                        </div>

                                        <div class="news-module-ver-content font-sourceSansPro">
                                            <div class="news-module-ver-etc">
                                                <div class="news-module-ver-time pull-left">
                                                    Telah dilihat 400 kali
                                                </div>
                                            </div>

                                            <div class="news-module-ver-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>

                                        </div>
                                    </div>

                                    <div class="news-module-ver">
                                        <div class="news-module-ver-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-18952.jpg">
                                            <span class="foto-count">7</span>
                                        </div>

                                        <div class="news-module-ver-content font-sourceSansPro">
                                            <div class="news-module-ver-etc">
                                                <div class="news-module-ver-time pull-left">
                                                    Telah dilihat 400 kali
                                                </div>
                                            </div>

                                            <div class="news-module-ver-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>
                                        </div>
                                    </div>

                                    <div class="news-module-ver">
                                        <div class="news-module-ver-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-171934.jpg">
                                            <span class="foto-count">7</span>
                                        </div>

                                        <div class="news-module-ver-content font-sourceSansPro">
                                            <div class="news-module-ver-etc">
                                                <div class="news-module-ver-time pull-left">
                                                    Telah dilihat 400 kali
                                                </div>
                                            </div>

                                            <div class="news-module-ver-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>
                                        </div>
                                    </div>
                                    <div class="news-module-ver">
                                        <div class="news-module-ver-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-24622.jpg">
                                            <span class="foto-count">7</span>
                                        </div>

                                        <div class="news-module-ver-content font-sourceSansPro">
                                            <div class="news-module-ver-etc">
                                                <div class="news-module-ver-time pull-left">
                                                    Telah dilihat 400 kali
                                                </div>
                                            </div>

                                            <div class="news-module-ver-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>
                                        </div>
                                    </div>

                                    <div class="news-module-ver">
                                        <div class="news-module-ver-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-52206.jpg">
                                            <span class="foto-count">7</span>
                                        </div>

                                        <div class="news-module-ver-content font-sourceSansPro">
                                            <div class="news-module-ver-etc">
                                                <div class="news-module-ver-time pull-left">
                                                    Telah dilihat 400 kali
                                                </div>
                                            </div>

                                            <div class="news-module-ver-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>
                                        </div>
                                    </div>

                                    <div class="news-module-ver">
                                        <div class="news-module-ver-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-108489.jpg">
                                            <span class="foto-count">7</span>
                                        </div>

                                        <div class="news-module-ver-content font-sourceSansPro">
                                            <div class="news-module-ver-etc">
                                                <div class="news-module-ver-time pull-left">
                                                    Telah dilihat 400 kali
                                                </div>
                                            </div>

                                            <div class="news-module-ver-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="video">
                            <div class="s-video-terkini-container s-foto-video-profile" style="text-align: justify;">
                                    <div class="news-module-ver">
                                        <div class="news-module-ver-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-24622.jpg">
                                            <span class="duration">01:06</span>
                                        </div>

                                        <div class="news-module-ver-content font-sourceSansPro">
                                            <div class="news-module-ver-etc">
                                                <div class="news-module-ver-time pull-left">
                                                    Telah dilihat 400 kali
                                                </div>
                                            </div>

                                            <div class="news-module-ver-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>
                                        </div>
                                    </div>

                                    <div class="news-module-ver">
                                        <div class="news-module-ver-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-52206.jpg">
                                            <span class="duration">01:06</span>
                                        </div>

                                        <div class="news-module-ver-content font-sourceSansPro">
                                            <div class="news-module-ver-etc">
                                                <div class="news-module-ver-time pull-left">
                                                    Telah dilihat 400 kali
                                                </div>
                                            </div>

                                            <div class="news-module-ver-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>
                                        </div>
                                    </div>

                                    <div class="news-module-ver">
                                        <div class="news-module-ver-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-108489.jpg">
                                            <span class="duration">01:06</span>
                                        </div>

                                        <div class="news-module-ver-content font-sourceSansPro">
                                            <div class="news-module-ver-etc">
                                                <div class="news-module-ver-time pull-left">
                                                    Telah dilihat 400 kali
                                                </div>
                                            </div>

                                            <div class="news-module-ver-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>
                                        </div>
                                    </div>
                                    <div class="news-module-ver">
                                        <div class="news-module-ver-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-37097.jpg">
                                            <span class="duration">01:06</span>
                                        </div>

                                        <div class="news-module-ver-content font-sourceSansPro">
                                            <div class="news-module-ver-etc">
                                                <div class="news-module-ver-time pull-left">
                                                    Telah dilihat 400 kali
                                                </div>
                                            </div>

                                            <div class="news-module-ver-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>

                                        </div>
                                    </div>

                                    <div class="news-module-ver">
                                        <div class="news-module-ver-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-18952.jpg">
                                            <span class="duration">01:06</span>
                                        </div>

                                        <div class="news-module-ver-content font-sourceSansPro">
                                            <div class="news-module-ver-etc">
                                                <div class="news-module-ver-time pull-left">
                                                    Telah dilihat 400 kali
                                                </div>
                                            </div>

                                            <div class="news-module-ver-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>
                                        </div>
                                    </div>

                                    <div class="news-module-ver">
                                        <div class="news-module-ver-img">
                                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-171934.jpg">
                                            <span class="duration">01:06</span>
                                        </div>

                                        <div class="news-module-ver-content font-sourceSansPro">
                                            <div class="news-module-ver-etc">
                                                <div class="news-module-ver-time pull-left">
                                                    Telah dilihat 400 kali
                                                </div>
                                            </div>

                                            <div class="news-module-ver-title">
                                                Jejak Sejarah yang Hilang di Makassar
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="tab-pane" id="m-ep">
                    <div class="s-container-573" style="border-left: 2px solid #bfbdbd; padding-left: 15px;">
                        <!-- ------------------ EDIT PROFILE ------------------  -->
                        <form>
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Display Name</label>
                                <input type="text" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Short Bio</label>
                                <textarea class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Birthday</label>
                                <input type="text" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Jenis Kelamin</label>
                                <span class="clearfix"></span>
                                <label class="radio-inline">
                                    <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1"> Pria
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="0"> Wanite
                                </label>
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="tel" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Current Password</label>
                                <input type="password" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group">
                                <label>Re-Type New Password</label>
                                <input type="password" class="form-control" id="" placeholder="">
                            </div>
                            <div class="form-group text-center">
                                <button type="submit" class="btn btn-info" style="background: #ff6600;">Simpan Perubahan</button>
                                <button type="submit" class="btn btn-default">Batal</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="m-kb">
                    <div class="s-container-573" style=" padding-left: 15px;">
                        <!-- ------------------ Kirim Berita ------------------  -->
                        <form class="form-horizontal">

                            <div class="form-group">
                                <div class="col-xs-3">
                                    <label class="control-label">&nbsp;</label>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <button class="btn btn-success" style="padding-left: 30px; padding-right: 30px;">
                                        <img src="../img/icon/profile-pen.png" height="14px" style="margin-right: 10px; margin-top: -3px;">
                                        Save
                                    </button>
                                    <button class="btn btn-default" style="padding-left: 30px; padding-right: 30px;">
                                        <img src="../img/icon/profile-x.png" height="14px" style="margin-right: 10px; margin-top: -3px;">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                            <div style="background: #f3f3f3; padding: 15px;">

                                <div class="form-group">
                                    <div class="col-xs-3 profile-label">
                                        <label class="control-label">Judul Berita</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control profile-input" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-3 profile-label">
                                        <label class="control-label">Kategori Berita</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <select class="form-control profile-input">
                                            <option value="">- Pilih Kategori Berita -</option>
                                            <option value="">0</option>
                                            <option value="">1</option>
                                            <option value="">2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group no-margin">
                                    <div class="col-xs-3 profile-label">
                                        <label class="control-label">Kata Kunci Berita</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control profile-input" id="" placeholder="">
                                        <span class="clearfix"></span>
                                        <span style="font-style: italic;">Tuliskan Kata Kunci Berita dan Pisahkan dengan Tanda (,) Koma </span>
                                    </div>
                                </div>
                                <div class="form-group no-margin">
                                    <div class="col-xs-3 profile-label">
                                        <label class="control-label">Lokasi Berita</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <hr/>
                                    </div>
                                </div>
                                <div class="row" style="margin-left: -15px; margin-right: -15px;">
                                    <div class="col-xs-6" style="padding: 0px 15px;">
                                        <div class="form-group">
                                            <div class="col-xs-6 profile-label">
                                                <label class="control-label">Latitude</label>
                                            </div>
                                            <div class="col-xs-6">
                                                <input type="text" class="form-control profile-input" id="" placeholder="" style="margin-left: 7px;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6" style="padding: 0px 15px;">
                                        <div class="form-group">
                                            <div class="col-xs-6  profile-label">
                                                <label class="control-label">Longitude</label>
                                            </div>
                                            <div class="col-xs-6">
                                                <input type="text" class="form-control profile-input" id="" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-3">
                                        <label class="control-label">&nbsp;</label>
                                    </div>
                                    <div class="col-xs-9">
                                    <div id="googleMap" style="width: 100%; height: 200px;"></div>
                                    </div>
                                </div>
                            </div>

                            <div style="background: #f3f3f3; padding: 5px; display: inline-block; margin-top: 10px;"> <img src="../img/icon/isi-berita.png">Isi Berita</div>
                            <div style="background: #f3f3f3; padding: 15px;">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <textarea class="form-control profile-input" rows="7"></textarea>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="m-kbf">
                    <div class="s-container-573" style=" padding-left: 15px;">
                        <!-- ------------------ Kirim Berita ------------------  -->
                        <form class="form-horizontal">

                            <div class="form-group">
                                <div class="col-xs-3">
                                    <label class="control-label">&nbsp;</label>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <button class="btn btn-success" style="padding-left: 30px; padding-right: 30px;">
                                        <img src="../img/icon/profile-pen.png" height="14px" style="margin-right: 10px; margin-top: -3px;">
                                        Save
                                    </button>
                                    <button class="btn btn-default" style="padding-left: 30px; padding-right: 30px;">
                                        <img src="../img/icon/profile-x.png" height="14px" style="margin-right: 10px; margin-top: -3px;">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                            <div style="background: #f3f3f3; padding: 15px;">

                                <div class="form-group">
                                    <div class="col-xs-3  profile-label">
                                        <label class="control-label">Judul Berita</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control profile-input" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-3  profile-label">
                                        <label class="control-label">Kategori Berita</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <select class="form-control profile-input">
                                            <option value="">- Pilih Kategori Berita -</option>
                                            <option value="">0</option>
                                            <option value="">1</option>
                                            <option value="">2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group no-margin">
                                    <div class="col-xs-3  profile-label">
                                        <label class="control-label">Kata Kunci Berita</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control profile-input" id="" placeholder="">
                                        <span class="clearfix"></span>
                                        <span style="font-style: italic;">Tuliskan Kata Kunci Berita dan Pisahkan dengan Tanda (,) Koma </span>
                                    </div>
                                </div>
                                <div class="form-group no-margin">
                                    <div class="col-xs-3  profile-label">
                                        <label class="control-label">Lokasi Berita</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <hr/>
                                    </div>
                                </div>
                                <div class="row" style="margin-left: -15px; margin-right: -15px;">
                                    <div class="col-xs-6" style="padding: 0px 15px;">
                                        <div class="form-group">
                                            <div class="col-xs-6  profile-label">
                                                <label class="control-label">Latitude</label>
                                            </div>
                                            <div class="col-xs-6">
                                                <input type="text" class="form-control profile-input" id="" placeholder="" style="margin-left: 7px;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6" style="padding: 0px 15px;">
                                        <div class="form-group">
                                            <div class="col-xs-6  profile-label">
                                                <label class="control-label">Longitude</label>
                                            </div>
                                            <div class="col-xs-6">
                                                <input type="text" class="form-control profile-input" id="" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-3">
                                        <label class="control-label">&nbsp;</label>
                                    </div>
                                    <div class="col-xs-9">
                                    <div id="googleMap1" style="width: 100%; height: 200px;"></div>
                                    </div>
                                </div>

                            </div>

                            <div style="background: #f3f3f3; padding: 5px; display: inline-block; margin-top: 10px;"> <img src="../img/icon/isi-berita.png">Isi Berita</div>
                            <div style="background: #f3f3f3; padding: 15px;">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <textarea class="form-control profile-input" rows="7"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 245px; position: absolute; top: 49px; right: -259px; background: #f3f3f3; padding: 10px;">
                                <div class="form-group">
                                    <div class="col-xs-6">
                                        <label class="control-label">Upload Foto</label>
                                    </div>
                                    <div class="col-xs-6">

                                        <div class="upload">
                                            <input type="file" name="upload"/>
                                        </div>

                                    </div>
                                </div>
                                <div class="area-tambah-foto">
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 text-right">
                                        <button class="btn btn-success" id="menambah_foto" type="button">
                                            Tambah Foto
                                        </button>
                                        <button class="btn btn-danger" type="button">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                                <hr>
                                <div style="font-size: 12px; ">
                                    Keterangan :<br/>
                                    1. Tombol Tambah Foto untuk menambahkan Jumlah Foto<br/>
                                    2. Upload File Type : jpg, png, gif<br/>
                                    3. Upload File Maksimum Ukuran : 200 kb
                                </div>
                                <script type="text/javascript">
                                $(document).ready(function(){
                                    var tmbh = '<div class="form-group">';
                                    tmbh += '<div class="col-xs-6">';
                                    tmbh += '    <label class="control-label">&nbsp;</label>';
                                    tmbh += '</div>';
                                    tmbh += '<div class="col-xs-6">';

                                    tmbh += '<div class="upload">';
                                    tmbh += '        <input type="file" name="upload"/>';
                                    tmbh += '    </div>';

                                     tmbh += ' </div>';
                                    tmbh += '</div>';
                                    $('#menambah_foto').on('click', function(){
                                        $('.area-tambah-foto').append(tmbh);
                                    })
                                });
                                </script>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="tab-pane" id="m-kbv">
                    <div class="s-container-573" style=" padding-left: 15px;">
                        <!-- ------------------ Kirim Berita ------------------  -->
                        <form class="form-horizontal">

                            <div class="form-group">
                                <div class="col-xs-3">
                                    <label class="control-label">&nbsp;</label>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <button class="btn btn-success" style="padding-left: 30px; padding-right: 30px;">
                                        <img src="../img/icon/profile-pen.png" height="14px" style="margin-right: 10px; margin-top: -3px;">
                                        Save
                                    </button>
                                    <button class="btn btn-default" style="padding-left: 30px; padding-right: 30px;">
                                        <img src="../img/icon/profile-x.png" height="14px" style="margin-right: 10px; margin-top: -3px;">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                            <div style="background: #f3f3f3; padding: 15px;">

                                <div class="form-group">
                                    <div class="col-xs-3 profile-label">
                                        <label class="control-label">Judul Berita</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control profile-input" id="" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-3 profile-label">
                                        <label class="control-label">Kategori Berita</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <select class="form-control profile-input">
                                            <option value="">- Pilih Kategori Berita -</option>
                                            <option value="">0</option>
                                            <option value="">1</option>
                                            <option value="">2</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group no-margin">
                                    <div class="col-xs-3 profile-label">
                                        <label class="control-label">Kata Kunci Berita</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <input type="text" class="form-control profile-input" id="" placeholder="">
                                        <span class="clearfix"></span>
                                        <span style="font-style: italic;">Tuliskan Kata Kunci Berita dan Pisahkan dengan Tanda (,) Koma </span>
                                    </div>
                                </div>
                                <div class="form-group no-margin">
                                    <div class="col-xs-3 profile-label">
                                        <label class="control-label">Lokasi Berita</label>
                                    </div>
                                    <div class="col-xs-9">
                                        <hr/>
                                    </div>
                                </div>
                                <div class="row" style="margin-left: -15px; margin-right: -15px;">
                                    <div class="col-xs-6" style="padding: 0px 15px;">
                                        <div class="form-group">
                                            <div class="col-xs-6 profile-label">
                                                <label class="control-label">Latitude</label>
                                            </div>
                                            <div class="col-xs-6">
                                                <input type="text" class="form-control profile-input" id="" placeholder="" style="margin-left: 7px;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6" style="padding: 0px 15px;">
                                        <div class="form-group">
                                            <div class="col-xs-6 profile-label">
                                                <label class="control-label">Longitude</label>
                                            </div>
                                            <div class="col-xs-6">
                                                <input type="text" class="form-control profile-input" id="" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-xs-3">
                                        <label class="control-label">&nbsp;</label>
                                    </div>
                                    <div class="col-xs-9">
                                    <div id="googleMap2" style="width: 100%; height: 200px;"></div>
                                    </div>
                                </div>

                            </div>

                            <div style="background: #f3f3f3; padding: 5px; display: inline-block; margin-top: 10px;"> <img src="../img/icon/isi-berita.png">Isi Berita</div>
                            <div style="background: #f3f3f3; padding: 15px;">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <textarea class="form-control profile-input" rows="7"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div style="width: 245px; position: absolute; top: 49px; right: -259px; background: #f3f3f3; padding: 10px;">
                                <div class="form-group">
                                    <div class="col-xs-6">
                                        <label class="control-label">Upload Video</label>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="upload">
                                            <input type="file" name="upload"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="area-tambah-video">
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 text-right">
                                        <button class="btn btn-success" id="menambah_video" type="button">
                                            Tambah Video
                                        </button>
                                        <button class="btn btn-danger" type="button">
                                            Upload
                                        </button>
                                    </div>
                                </div>
                                <hr>
                                <div style="font-size: 12px; ">
                                    Keterangan :<br/>
                                    1. Tombol Tambah Video untuk menambahkan Jumlah Video<br/>
                                    2. Upload File Type : Mp4, Mp3<br/>
                                    3. Upload File Maksimum Ukuran : 100Mb
                                </div>
                                <script type="text/javascript">
                                $(document).ready(function(){
                                    var tmbh = '<div class="form-group">';
                                    tmbh += '<div class="col-xs-6">';
                                    tmbh += '    <label class="control-label">&nbsp;</label>';
                                    tmbh += '</div>';
                                    tmbh += '<div class="col-xs-6">';
                                    tmbh += '<div class="upload">';
                                    tmbh += '        <input type="file" name="upload"/>';
                                    tmbh += '    </div>';

                                     tmbh += ' </div>';
                                    tmbh += '</div>';
                                    $('#menambah_video').on('click', function(){
                                        $('.area-tambah-video').append(tmbh);
                                    })
                                });
                                </script>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <span class="clearfix"></span>

            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

        </section>




        <!--Footer-->
        <!--==================================================-->
        <?php include('_footer.php'); ?>

        <script src="http://maps.googleapis.com/maps/api/js"></script>
        <script>
            function initialize() {
                var mapProp = {
                    center:new google.maps.LatLng(51.508742,-0.120850),
                    zoom:5,
                    mapTypeId:google.maps.MapTypeId.ROADMAP
                };
                var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
                var map1=new google.maps.Map(document.getElementById("googleMap1"),mapProp);
                var map2=new google.maps.Map(document.getElementById("googleMap2"),mapProp);
            }
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>
    </body>
</html>
