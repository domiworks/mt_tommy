
<div class="container-fluid">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                <div class="news-head-bt">
                    <img src="../img/icon/foto-terkini.png" style="width: 105px; height: 34px;">

                    <div class="redspan">
                        <div class="greyspan">
                        </div>
                    </div>

                    <div style="position: relative; float: right; text-align: center; margin-top: -4px;">
                        <a href="#" class="pull-left">
                            <span style="font-size: 12px; color: #ec2227;">LIHAT SEMUA</span>
                            <img src="../img/icon/all-window-red.png" height="10px">
                        </a>
                    </div>
                </div>


                <div class="s-video-terkini-container" style="text-align: justify;">
                    <div class="news-module-ver">
                        <div class="news-module-ver-img">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-37097.jpg">
                            <span class="foto-count">7</span>
                        </div>

                        <div class="news-module-ver-content font-sourceSansPro">
                            <div class="news-module-ver-etc">
                                <div class="news-module-ver-time pull-left">
                                    Telah dilihat 400 kali
                                </div>
                            </div>

                            <div class="news-module-ver-title">
                                Jejak Sejarah yang Hilang di Makassar
                            </div>

                        </div>
                    </div>

                    <div class="news-module-ver">
                        <div class="news-module-ver-img">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-18952.jpg">
                            <span class="foto-count">7</span>
                        </div>

                        <div class="news-module-ver-content font-sourceSansPro">
                            <div class="news-module-ver-etc">
                                <div class="news-module-ver-time pull-left">
                                    Telah dilihat 400 kali
                                </div>
                            </div>

                            <div class="news-module-ver-title">
                                Jejak Sejarah yang Hilang di Makassar
                            </div>
                        </div>
                    </div>

                    <div class="news-module-ver">
                        <div class="news-module-ver-img">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-171934.jpg">
                            <span class="foto-count">7</span>
                        </div>

                        <div class="news-module-ver-content font-sourceSansPro">
                            <div class="news-module-ver-etc">
                                <div class="news-module-ver-time pull-left">
                                    Telah dilihat 400 kali
                                </div>
                            </div>

                            <div class="news-module-ver-title">
                                Jejak Sejarah yang Hilang di Makassar
                            </div>
                        </div>
                    </div>
                    <div class="news-module-ver">
                        <div class="news-module-ver-img">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-24622.jpg">
                            <span class="foto-count">7</span>
                        </div>

                        <div class="news-module-ver-content font-sourceSansPro">
                            <div class="news-module-ver-etc">
                                <div class="news-module-ver-time pull-left">
                                    Telah dilihat 400 kali
                                </div>
                            </div>

                            <div class="news-module-ver-title">
                                Jejak Sejarah yang Hilang di Makassar
                            </div>
                        </div>
                    </div>

                    <div class="news-module-ver">
                        <div class="news-module-ver-img">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-52206.jpg">
                            <span class="foto-count">7</span>
                        </div>

                        <div class="news-module-ver-content font-sourceSansPro">
                            <div class="news-module-ver-etc">
                                <div class="news-module-ver-time pull-left">
                                    Telah dilihat 400 kali
                                </div>
                            </div>

                            <div class="news-module-ver-title">
                                Jejak Sejarah yang Hilang di Makassar
                            </div>
                        </div>
                    </div>

                    <div class="news-module-ver">
                        <div class="news-module-ver-img">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-108489.jpg">
                            <span class="foto-count">7</span>
                        </div>

                        <div class="news-module-ver-content font-sourceSansPro">
                            <div class="news-module-ver-etc">
                                <div class="news-module-ver-time pull-left">
                                    Telah dilihat 400 kali
                                </div>
                            </div>

                            <div class="news-module-ver-title">
                                Jejak Sejarah yang Hilang di Makassar
                            </div>
                        </div>
                    </div>
                </div>



                <div class="news-bottom-button">
                    <div class="bottom-lb">
                        <div class="button-lb">
                            <img src="../img/icon/more.png">
                            <a href="javascript:;" class="f-video-terkini-lebih-banyak">
                                LEBIH BANYAK
                            </a>
                        </div>

                    </div>

                    <div class="bottom-ls">
                        <div class="button-ls">
                            <img src="../img/icon/all-window.png">
                            <a href="">
                                LIHAT SEMUA
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var htmlContent = $('.s-video-terkini-container').html();
        $('.f-video-terkini-lebih-banyak').on('click', function(){
            $('.s-video-terkini-container').append(htmlContent);
        });
    });
</script>


