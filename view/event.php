<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Advertisement Top -->
        <!--==================================================-->
        <!-- Header -->
        <!--==================================================-->
        <?php include('_header.php'); ?>


        <!--Content-->
        <!--==================================================-->
        <section class="s-main-content">

            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

            <div class="container-fluid">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                            <div class="" style="position: relative;">

                                <div class="s-fancy-header-red">
                                    <span class="s-title">Event Terkini</span>
                                </div>
                                <span class="clearfix"></span>
                                <div style="margin-bottom: 15px; overflow: hidden;">
                                    <div class="pull-left text-right" style="width: 195px;">
                                        <img src="../img/icon/big-2016.png">
                                    </div>
                                    <div class="pull-right" style="width: calc(100% - 195px);">
                                        <div class="form-group pull-left" style="margin-top: 18px; margin-left: 65px;">
                                            <input class="form-control pull-left" placeholder="Ketik nama event" style="width: 530px; border-top-right-radius: 0px; border-bottom-right-radius: 0px;" />
                                            <button class="btn pull-left" style="background: #1cc4be; border-top-left-radius: 0px; border-bottom-left-radius: 0px; padding: 0px; height: 34px; width: 34px; ">
                                                <img src="../img/icon/mag-white.png" class="pull-left" style="margin-left: 9px;">
                                            </button>
                                        </div>
                                        <div class="form-group pull-right" style="margin-top: 18px; line-height: 34px;">
                                            <span><strong>Sortir berdasarkan:</strong></span>
                                            <select>
                                                <option value="0">Semua kategori</option>
                                                <option value="1">Semua kategori</option>
                                                <option value="2">Semua kategori</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <span class="clearfix"></span>
                                <ul class="nav nav-tabs s-pp-nav-event pull-left" id="myTab00" style=""> <!-- f-flyLeft-nav flyLeftAbsolute -->
                                    <li class="active" style="width: inherit; display: block;"><a href="#an0" data-target="#an0" data-toggle="tab">Januari</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#an1" data-target="#an1" data-toggle="tab">Februari</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#an2" data-target="#an2" data-toggle="tab">Maret</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-kbf" data-target="#m-kbf" data-toggle="tab">April</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-kbv" data-target="#m-kbv" data-toggle="tab">Juni</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-dst" data-target="#m-dst" data-toggle="tab">Juli</a></li>

                                    <li class="" style="width: inherit; display: block;"><a href="#m-kbfa" data-target="#m-kbfa" data-toggle="tab">Agustus</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-kbvb" data-target="#m-kbvb" data-toggle="tab">September</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-dstc" data-target="#m-dstc" data-toggle="tab">Oktober</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-lifestyle" data-target="#m-lifestyle" data-toggle="tab">November</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-pendidikan" data-target="#m-pendidikan" data-toggle="tab">Desember</a></li>
                                </ul>

                                <div class="tab-content pull-right" style="width: calc(100% - 220px); display: block;">
                                    <div class="tab-pane active" id="an0" style="padding-top: 10px; ">
                                        <?php for($i = 0; $i < 5; $i++){ ?>
                                        <div class="" style="background: #f2f4f6; width: 100%; height: 164px; margin-bottom: 20px;">
                                            <img class="pull-left" src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-377353.jpg" style="width: 247px; height: 164px;">
                                            <div class="pull-left" style="width: calc(100% - 441px); padding: 10px 20px;">
                                                <h4 style="margin-top: 0px;">
                                                    Sayyang Pattudu
                                                </h4>
                                                <p style="max-height: 84px; overflow: hidden;">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                </p>
                                                <span class="clearfix"></span>
                                                <div class="pull-left" style="margin-right: 20px;">
                                                    <img src="../img/icon/event-location.png" class="pull-left" style="margin-right: 5px; margin-top: 3px;">
                                                    <span style="font-weight: 700; font-size: 12px;">Polewali mandar</span>
                                                </div>
                                                <div class="pull-left" style="margin-right: 20px;">
                                                    <img src="../img/icon/event-date.png" class="pull-left" style="margin-right: 5px; margin-top: 3px;">
                                                    <span style="font-weight: 700; font-size: 12px;">31 Maret 2016</span>
                                                </div>
                                                <div class="pull-left" style="margin-right: 20px;">
                                                    <img src="../img/icon/event-time.png" class="pull-left" style="margin-right: 5px; margin-top: 3px;">
                                                    <span style="font-weight: 700; font-size: 12px;">Pkl 10.00 - 16.00 wita</span>
                                                </div>
                                            </div>
                                            <div class="pull-left" style="width: 194px; padding: 10px 20px; padding-top: 30px;">
                                                <span style="color: #303986; font-weight: 700; font-size: 32px;">6.0</span><span style="color: #303986; font-weight: 700; font-size: 18px;">/10</span>
                                                <span class="clearfix"></span>
                                                <div>
                                                    <span style="color: #303986;">Lumayan</span>

                                                    <div class="pull-right text-right">
                                                        <img src="../img/icon/star-on.png" width="10px" />
                                                        <img src="../img/icon/star-on.png" width="10px" />
                                                        <img src="../img/icon/star-on.png" width="10px" />
                                                        <img src="../img/icon/star-off.png" width="10px" />
                                                        <img src="../img/icon/star-off.png" width="10px" />
                                                    </div>

                                                </div>
                                                <span class="clearfix"></span>
                                                <img src="../img/icon/selengkapnya.png" width="156px" style="margin-top: 15px;"/>

                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="tab-pane " id="an1" style="padding-top: 10px; ">

                                        <?php for($i = 0; $i < 5; $i++){ ?>
                                        <div class="" style="background: #f2f4f6; width: 100%; height: 164px; margin-bottom: 20px;">
                                            <img class="pull-left" src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-377358.jpg" style="width: 247px; height: 164px;">
                                            <div class="pull-left" style="width: calc(100% - 441px); padding: 10px 20px;">
                                                <h4 style="margin-top: 0px;">
                                                    Sayyang Pattudu
                                                </h4>
                                                <p style="max-height: 84px; overflow: hidden;">
                                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                </p>
                                                <span class="clearfix"></span>
                                                <div class="pull-left" style="margin-right: 20px;">
                                                    <img src="../img/icon/event-location.png" class="pull-left" style="margin-right: 5px; margin-top: 3px;">
                                                    <span style="font-weight: 700; font-size: 12px;">Polewali mandar</span>
                                                </div>
                                                <div class="pull-left" style="margin-right: 20px;">
                                                    <img src="../img/icon/event-date.png" class="pull-left" style="margin-right: 5px; margin-top: 3px;">
                                                    <span style="font-weight: 700; font-size: 12px;">31 Maret 2016</span>
                                                </div>
                                                <div class="pull-left" style="margin-right: 20px;">
                                                    <img src="../img/icon/event-time.png" class="pull-left" style="margin-right: 5px; margin-top: 3px;">
                                                    <span style="font-weight: 700; font-size: 12px;">Pkl 10.00 - 16.00 wita</span>
                                                </div>
                                            </div>
                                            <div class="pull-left" style="width: 194px; padding: 10px 20px; padding-top: 30px;">
                                                <span style="color: #303986; font-weight: 700; font-size: 32px;">6.0</span><span style="color: #303986; font-weight: 700; font-size: 18px;">/10</span>
                                                <span class="clearfix"></span>
                                                <div>
                                                    <span style="color: #303986;">Lumayan</span>

                                                    <div class="pull-right text-right">
                                                        <img src="../img/icon/star-on.png" width="10px" />
                                                        <img src="../img/icon/star-on.png" width="10px" />
                                                        <img src="../img/icon/star-on.png" width="10px" />
                                                        <img src="../img/icon/star-off.png" width="10px" />
                                                        <img src="../img/icon/star-off.png" width="10px" />
                                                    </div>

                                                </div>
                                                <span class="clearfix"></span>
                                                <img src="../img/icon/selengkapnya.png" width="156px" style="margin-top: 15px;"/>

                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="tab-pane" id="an2" style="padding-top: 10px; ">
                                        4
                                    </div>
                                    <div class="tab-pane" id="m-kbf" style="padding-top: 10px; ">
                                        5
                                    </div>
                                    <div class="tab-pane " id="m-kbv" style="padding-top: 10px; ">
                                        6
                                    </div>
                                    <div class="tab-pane" id="m-dst" style="padding-top: 10px; ">
                                        7
                                    </div>

                                    <div class="tab-pane" id="m-kbfa" style="padding-top: 10px; ">
                                        8
                                    </div>
                                    <div class="tab-pane" id="m-kbvb" style="padding-top: 10px; ">
                                        9
                                    </div>
                                    <div class="tab-pane" id="m-dstc" style="padding-top: 10px; ">
                                        10
                                    </div>

                                    <div class="tab-pane" id="m-lifestyle" style="padding-top: 10px; ">
                                        11
                                    </div>
                                    <div class="tab-pane" id="m-pendidikan" style="padding-top: 10px; ">
                                        12
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                    </div>
                </div>
            </div>


            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

        </section>




        <!--Footer-->
        <!--==================================================-->
        <?php include('_footer.php'); ?>
        <style type="text/css">
            #chartdiv {
                width       : 100%;
                height      : 500px;
                font-size   : 11px;
            }
        </style>
        <script type="text/javascript">
            var chart = AmCharts.makeChart( "chartdiv", {
              "type": "pie",
              "theme": "light",
              "dataProvider": [ {
                "country": "Lithuania",
                "litres": 501.9
            }, {
                "country": "Czech Republic",
                "litres": 301.9
            }, {
                "country": "Ireland",
                "litres": 201.1
            }, {
                "country": "Germany",
                "litres": 165.8
            }, {
                "country": "Australia",
                "litres": 139.9
            }, {
                "country": "Austria",
                "litres": 128.3
            }, {
                "country": "UK",
                "litres": 99
            }, {
                "country": "Belgium",
                "litres": 60
            }, {
                "country": "The Netherlands",
                "litres": 50
            } ],
            "valueField": "litres",
            "titleField": "country",
            "balloon":{
             "fixedPosition":true
         },
         "export": {
            "enabled": true
        }
    } );
</script>

    </body>
</html>
