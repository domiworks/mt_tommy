
<div class="container-fluid">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                <div class="news-head-bt">
                    <img src="../img/icon/citizen-report.png" style="width: 106px; height: 34px;">

                    <div class="redspan">
                        <div class="greyspan">
                        </div>
                    </div>
                </div>
                <div class="s-berita-terkini-container">
                    <a href="#" class="news-module-hor">
                        <div class="news-module-hor-img">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-375124.jpg">
                        </div>

                        <div class="news-module-hor-content font-sourceSansPro">
                            <div class="news-module-hor-title">
                                Jejak Sejarah yang Hilang di Makassar
                            </div>

                            <div class="news-module-hor-desc">
                                MAKASSAR – Pasca penangkapan 15 ton ikan berformalin oleh polisi perairan Polda Sulawesi Selatan, Kepala …
                            </div>

                            <div class="news-module-hor-etc">
                                <div class="news-module-hor-tag pull-left">
                                    SPONSOR
                                    <div class="orange-tooltip">
                                        <img src="../img/icon/orgTooltip.png">
                                    </div>
                                </div>

                                <div class="news-module-hor-info pull-left">
                                    Info kota
                                </div>

                                <div class="news-module-hor-icon pull-left">
                                    <img src="../img/icon/clock.png">
                                </div>

                                <div class="news-module-hor-time pull-left">
                                    9 menit yang lalu
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="#" class="news-module-hor">
                        <div class="news-module-hor-img">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-375123.jpg">
                        </div>

                        <div class="news-module-hor-content font-sourceSansPro">
                            <div class="news-module-hor-title">
                                Jejak Sejarah yang Hilang di Makassar
                            </div>

                            <div class="news-module-hor-desc">
                                MAKASSAR – Pasca penangkapan 15 ton ikan berformalin oleh polisi perairan Polda Sulawesi Selatan, Kepala …
                            </div>

                            <div class="news-module-hor-etc">
                                <div class="news-module-hor-tag pull-left">
                                    SPONSOR
                                    <div class="orange-tooltip">
                                        <img src="../img/icon/orgTooltip.png">
                                    </div>
                                </div>

                                <div class="news-module-hor-info pull-left">
                                    Info kota
                                </div>

                                <div class="news-module-hor-icon pull-left">
                                    <img src="../img/icon/clock.png">
                                </div>

                                <div class="news-module-hor-time pull-left">
                                    9 menit yang lalu
                                </div>
                            </div>
                        </div>
                    </a>

                    <a href="#" class="news-module-hor">
                        <div class="news-module-hor-img">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-375122.jpg">
                        </div>

                        <div class="news-module-hor-content font-sourceSansPro">
                            <div class="news-module-hor-title">
                                Jejak Sejarah yang Hilang di Makassar
                            </div>

                            <div class="news-module-hor-desc">
                                MAKASSAR – Pasca penangkapan 15 ton ikan berformalin oleh polisi perairan Polda Sulawesi Selatan, Kepala …
                            </div>

                            <div class="news-module-hor-etc">
                                <div class="news-module-hor-tag pull-left">
                                    SPONSOR
                                    <div class="orange-tooltip">
                                        <img src="../img/icon/orgTooltip.png">
                                    </div>
                                </div>

                                <div class="news-module-hor-info pull-left">
                                    Info kota
                                </div>

                                <div class="news-module-hor-icon pull-left">
                                    <img src="../img/icon/clock.png">
                                </div>

                                <div class="news-module-hor-time pull-left">
                                    9 menit yang lalu
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="news-bottom-button">
                    <div class="bottom-lb">
                        <div class="button-lb">
                            <img src="../img/icon/more.png">
                            <a href="javascript:;" class="f-berita-terkini-lebih-banyak">
                                LEBIH BANYAK
                            </a>
                        </div>

                    </div>

                    <div class="bottom-ls">
                        <div class="button-ls">
                            <img src="../img/icon/all-window.png">
                            <a href="">
                                LIHAT SEMUA
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var htmlContent = $('.s-berita-terkini-container').html();
        $('.f-berita-terkini-lebih-banyak').on('click', function(){
            $('.s-berita-terkini-container').append(htmlContent);
        });
    });
</script>
