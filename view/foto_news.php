<!DOCTYPE html>
<html>
    <head>
        <title>Foto News</title>
        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Advertisement Top -->
        <!--==================================================-->
        <!-- Header -->
        <!--==================================================-->
        <?php include('_header.php'); ?>
        <style type="text/css">
            .s-big-carousel .carousel-inner .item .s-news-node:nth-child(1) {
                width: 487px;
            }
        </style>
        <!--Content-->
        <!--==================================================-->
        <div class="adv adv-160-600 adv-top-left" style="top: 131px;">

        </div>

        <!-- Adv Right -->
        <!--==================================================-->
        <div class="adv adv-160-600 adv-top-right" style="top: 131px;">

        </div>
        <script type="text/javascript">

            function advFixer(){
                var marginLeft = ($(window).width() - 1100)/2;
                var fixer = (marginLeft - 160 - 25);
                $('.adv-top-left').css('left', fixer);
                $('.adv-top-right').css('right', fixer);
            }

            advFixer();

            $(document).ready(function() {

                $(window).resize(function(){
                    advFixer();
                });

            });
        </script>
        <section class="s-main-content" style="overflow: hidden;">

            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

            <!-- Adv Left -->
            <!--==================================================-->


            <!-- Middel Content -->
            <!--==================================================-->
                <div class="s-col-2-per-3 pull-left f-content-area" style="/*background: #123;*/">
                <!-- Big Carousel -->
                <!--==================================================-->
                <div class="s-big-carousel" style="width: 790px;">
                    <div id="main-carousel" class="carousel slide" data-ride="carousel" style="height: inherit; position: relative;">

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox" style="height: inherit;">
                            <div class="item active" style="">
                                <?php for($i=0; $i < 3; $i++){ ?>
                                <a href="#" class="s-news-node" style="background: url('https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-242882.jpg') no-repeat center center; background-size: cover; ">

                                    <div class="" style="width: 100%; height: 100%; overflow: hidden; float: left; position: relative;">
                                        <span class="tag-top">Internasional</span>
                                    </div>
                                    <div class="news-module-ver-content" style="width: 100%; float: left; padding-left: 15px; position: absolute; bottom: 3px; left: 0px;">
                                        <div class="news-module-ver-title" style="color: #fff; font-size: 20px; width: 70%;">
                                            Begini Kondisi Gedung di Taiwan Usai Digoyang Gempa 6,4 SR
                                        </div>
                                        <div class="news-module-ver-etc">
                                            <div class="news-module-ver-time pull-left" style="margin-top: 0px; color: #fff; font-size: 14px;">
                                        <span class="s-clock-white-filled pull-left"></span>
                                        <span class="pull-left">13 Februari 2016 18:10</span>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="foto-count">7</span>

                                </a>
                                <?php } ?>
                            </div>
                            <div class="item" style="color: #fff; height: inherit;">
                                <?php for($i=0; $i < 3; $i++){ ?>
                                <a href="#" class="s-news-node" style="background: url('https://s-media-cache-ak0.pinimg.com/736x/16/0b/c7/160bc7a33fff836a3a85950c79a614e9.jpg') no-repeat center center; background-size: cover; ">

                                    <div class="" style="width: 100%; height: 100%; overflow: hidden; float: left; position: relative;">
                                        <span class="tag-top">Peristiwa</span>
                                    </div>
                                    <div class="news-module-ver-content" style="width: 100%; float: left; padding-left: 15px; position: absolute; bottom: 3px; left: 0px;">
                                        <div class="news-module-ver-title" style="color: #fff; font-size: 20px; width: 70%;">
                                            Tuntut Perubahan Status, 5 Guru Honorer Meninggal Dunia
                                        </div>
                                        <div class="news-module-ver-etc">
                                            <div class="news-module-ver-time pull-left" style="margin-top: 0px; color: #fff; font-size: 14px;">
                                        <span class="s-clock-white-filled pull-left"></span>
                                        <span class="pull-left">13 Februari 2016 18:10</span>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="foto-count">7</span>

                                </a>
                                <?php } ?>
                            </div>
                            <div class="item" style="color: #fff; height: inherit;">
                                <?php for($i=0; $i < 3; $i++){ ?>
                                <a href="#" class="s-news-node" style="background: url('https://lh4.ggpht.com/t0VAk0yMLwPg2jXCZ5HPMJAKH9Z-1fDcFGciAbQSqbTn3Xd_0OIeXbE7pUV02khWDw=h900') no-repeat center center; background-size: cover; ">

                                    <div class="" style="width: 100%; height: 100%; overflow: hidden; float: left; position: relative;">
                                        <span class="tag-top">Peristiwa</span>
                                    </div>
                                    <div class="news-module-ver-content" style="width: 100%; float: left; padding-left: 15px; position: absolute; bottom: 3px; left: 0px;">
                                        <div class="news-module-ver-title" style="color: #fff; font-size: 20px; width: 70%;">
                                            Daeng Azis pentolan kalijodo
                                        </div>
                                        <div class="news-module-ver-etc">
                                            <div class="news-module-ver-time pull-left" style="margin-top: 0px; color: #fff; font-size: 14px;">
                                        <span class="s-clock-white-filled pull-left"></span>
                                        <span class="pull-left">13 Februari 2016 18:10</span>
                                            </div>
                                        </div>
                                    </div>
                                    <span class="foto-count">7</span>

                                </a>
                                <?php } ?>
                            </div>
                        </div>

                        <!-- Controls -->
                        <a class="s-left-btn" href="#main-carousel" role="button" data-slide="prev" style="">
                            <img src="../img/icon/big-car-left.png">
                        </a>
                        <a class="s-right-btn" href="#main-carousel" role="button" data-slide="next">
                            <img src="../img/icon/big-car-right.png">
                        </a>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function() {
                        $('#main-carousel').carousel('pause');
                    });
                </script>
                <div class="adv adv-790-100">
                    ADVERTISE 790 x 100
                </div>
                <?php include('node_foto_terkini.php'); ?>
                <?php include('node_foto_rekomendasi.php'); ?>
                <div class="adv adv-790-100">
                    ADVERTISE 790 x 100
                </div>
            </div>

            <!-- Middel Content -->
            <!--==================================================-->
            <div class="s-col-1-per-3 pull-right f-extra-area" style="">
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php include('node_berita_pop.php'); ?>
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php include('node_event_terkini.php'); ?>
                <?php for($i=0; $i < 0; $i++){ ?>
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php } ?>
             </div>



        </section>




        <!--Footer-->
        <!--==================================================-->
        <?php include('_footer.php'); ?>





    </body>
</html>
