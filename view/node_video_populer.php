
<div class="container-fluid">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                <div class="news-head-bt">
                    <img src="../img/icon/video-pop.png" style="width: 116px; height: 34px;">

                    <div class="redspan">
                        <div class="greyspan">
                        </div>
                    </div>

                    <div style="position: relative; float: right; text-align: center; margin-top: -4px;">
                        <a href="#" class="pull-left">
                            <span style="font-size: 12px; color: #ec2227;">LIHAT SEMUA</span>
                            <img src="../img/icon/all-window-red.png" height="10px">
                        </a>
                        <span class="pull-left" style="margin-left: 5px; margin-right: 5px;">atau</span>
                        <a class="s-tick" href="#video-populer-carousel" role="button" data-slide="prev" style="top: 7px; right: 36px;">
                            <img src="../img/icon/news-tick-left.png" width="12px">
                        </a>
                        <a class="s-tick" href="#video-populer-carousel" role="button" data-slide="next" style="top: 7px; right: 16px;">
                            <img src="../img/icon/news-tick-right.png" width="12px">
                        </a>
                    </div>
                </div>


                <div id="video-populer-carousel" class="carousel slide" data-ride="carousel" style="height: inherit; position: relative;">
                    <div class="carousel-inner" role="listbox">
                        <div class="item active ">
                            <div class="s-news-node s-news-xs pull-left" style="width: 565px; margin-bottom: 10px; position: relative;">
                                <div class="" style="width: 565px; height: 412px; overflow: hidden; float: left; position: relative;">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-242882.jpg" style="width: 100%; width: 565px; height: 412px;">
                                    <img src="../img/icon/play-icon.png" width="86px" style="position: absolute; top: 148px; left: 238px; ">
                                </div>
                                <div class="news-module-ver-content" style="width: 565px; float: left; padding-left: 15px; position: absolute; bottom: 3px; left: 0px;">
                                    <div class="news-module-ver-title" style="color: #fff; font-size: 20px; width: 70%;">
                                        Unggah Foto Haters yang Hina Chika Jessica,
                                        Deddy Corbuzier Dikritik Netizen
                                    </div>
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left" style="margin-top: 0px; color: #fff; font-size: 14px;">
                                            Dilihat 29.000 kali
                                        </div>
                                    </div>
                                </div>
                                <span style="background: #808184; padding: 10px 15px; position: absolute; right: 5px; bottom: 5px; color: #fff;">01:06</span>
                            </div>


                            <div class="s-news-node s-news-xs pull-right" style="width: 211px; margin-bottom: 10px;">
                                <div class="" style="width: 211px; height: 145px; overflow: hidden; float: left; position: relative;">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-38480.jpg" style="width: 100%; width: 211px; height: 145px;">
                                    <img src="../img/icon/white-video.png" width="45" style="position: absolute; top: 51px; left: 82px;">
                                </div>
                                <div class="news-module-ver-content" style="width: 211px; float: left; ">
                                    <div class="news-module-ver-etc">
                                    <div class="news-module-ver-time pull-left" style="margin-top: 0px; font-size: 13px;">
                                            Dilihat 108.ss000 kali
                                        </div>
                                    </div>
                                    <div class="news-module-ver-title" style="padding: 0px;">
                                        Diduga Makan Buaya, Ayam di Bone Ditembak Mati
                                    </div>

                                </div>
                            </div>
                            <div class="s-news-node s-news-xs pull-right" style="width: 211px; margin-bottom: 10px;">
                                <div class="" style="width: 211px; height: 145px; overflow: hidden; float: left; position: relative;">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-72987.jpg" style="width: 100%; width: 211px; height: 145px;">
                                    <img src="../img/icon/white-video.png" width="45" style="position: absolute; top: 51px; left: 82px;">
                                </div>
                                <div class="news-module-ver-content" style="width: 211px; float: left; ">
                                    <div class="news-module-ver-etc">
                                    <div class="news-module-ver-time pull-left" style="margin-top: 0px; font-size: 13px;">
                                            Dilihat 12.000 kali
                                        </div>
                                    </div>
                                    <div class="news-module-ver-title" style="padding: 0px;">
                                        Grand Canyon Guys
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="item ">
                            <div class="s-news-node s-news-xs pull-left" style="width: 565px; margin-bottom: 10px; position: relative;">
                                <div class="" style="width: 565px; height: 412px; overflow: hidden; float: left; position: relative;">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-118900.jpg" style="width: 100%; width: 565px; height: 412px;">
                                    <img src="../img/icon/play-icon.png" width="86px" style="position: absolute; top: 148px; left: 238px; ">
                                </div>
                                <div class="news-module-ver-content" style="width: 565px; float: left; padding-left: 15px; position: absolute; bottom: 3px; left: 0px;">
                                    <div class="news-module-ver-title" style="color: #fff; font-size: 20px; width: 70%;">
                                        Unggah Foto Haters yang Hina Chika Jessica,
                                        Deddy Corbuzier Dikritik Netizen
                                    </div>
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left" style="margin-top: 0px; color: #fff; font-size: 14px;">
                                            Dilihat 29.000 kali
                                        </div>
                                    </div>
                                </div>
                                <span style="background: #808184; padding: 10px 15px; position: absolute; right: 5px; bottom: 5px; color: #fff;">01:06</span>
                            </div>


                            <div class="s-news-node s-news-xs pull-right" style="width: 211px; margin-bottom: 10px;">
                                <div class="" style="width: 211px; height: 145px; overflow: hidden; float: left; position: relative;">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-242886.jpg" style="width: 100%; width: 211px; height: 145px;">
                                    <img src="../img/icon/white-video.png" width="45" style="position: absolute; top: 51px; left: 82px;">
                                </div>
                                <div class="news-module-ver-content" style="width: 211px; float: left; ">
                                    <div class="news-module-ver-etc">
                                    <div class="news-module-ver-time pull-left" style="margin-top: 0px; font-size: 13px;">
                                            Dilihat 108.ss000 kali
                                        </div>
                                    </div>
                                    <div class="news-module-ver-title" style="padding: 0px;">
                                        Diduga Makan Buaya, Ayam di Bone Ditembak Mati
                                    </div>

                                </div>
                            </div>
                            <div class="s-news-node s-news-xs pull-right" style="width: 211px; margin-bottom: 10px;">
                                <div class="" style="width: 211px; height: 145px; overflow: hidden; float: left; position: relative;">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-63677.jpg" style="width: 100%; width: 211px; height: 145px;">
                                    <img src="../img/icon/white-video.png" width="45" style="position: absolute; top: 51px; left: 82px;">
                                </div>
                                <div class="news-module-ver-content" style="width: 211px; float: left; ">
                                    <div class="news-module-ver-etc">
                                    <div class="news-module-ver-time pull-left" style="margin-top: 0px; font-size: 13px;">
                                            Dilihat 12.000 kali
                                        </div>
                                    </div>
                                    <div class="news-module-ver-title" style="padding: 0px;">
                                        Grand Canyon Guys
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- <a href="#" class="item ">
                        </a> -->
                    </div>

                </div>





            </div>
        </div>
    </div>
</div>



