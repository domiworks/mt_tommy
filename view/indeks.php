<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Advertisement Top -->
        <!--==================================================-->
        <!-- Header -->
        <!--==================================================-->
        <?php include('_header.php'); ?>


        <!--Content-->
        <!--==================================================-->
        <section class="s-main-content">

            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

            <div class="container-fluid">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                            <div class="" style="position: relative;">

                                <div class="s-fancy-header-red">
                                    <span class="s-title">Indeks Kanal</span>
                                    <ul class="list-unstyled s-sub-menu" style="">
                                        <div class="pull-left" style="margin-top: 7px;">
                                            <input class="pull-left" type="text" style="width: 154px; height: 20px; color: #000; margin-left: 30px; border-top-left-radius: 4px;  border-bottom-left-radius: 4px; ">
                                            <button class="pull-left" type="button" style="padding: 0px; width: 20px; height: 20px; border: 0px; background: #ed1c24; border-top-right-radius: 4px;  border-bottom-right-radius: 4px;">
                                                    <img src="../img/icon/search-20px.png" class="pull-left">
                                            </button>
                                        </div>
                                        <div class="pull-right">
                                            <div class="pull-left">
                                                <span style="margin-left: 15px; margin-right: 15px">
                                                    From
                                                </span>
                                                <select class="" style="color: #000;">
                                                    <?php for($i=1; $i < 32; $i++){ ?>
                                                    <option value="<?php echo $i; ?>" style="color: #000;"><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <select class="" style="color: #000;">
                                                    <option value="01" style="color: #000;">Jan</option>
                                                    <option value="02" style="color: #000;">Feb</option>
                                                    <option value="03" style="color: #000;">Mar</option>
                                                    <option value="04" style="color: #000;">Apr</option>
                                                    <option value="05" style="color: #000;">Mei</option>
                                                    <option value="06" style="color: #000;">Jun</option>
                                                    <option value="07" style="color: #000;">Jul</option>
                                                    <option value="08" style="color: #000;">Agu</option>
                                                    <option value="09" style="color: #000;">Sep</option>
                                                    <option value="10" style="color: #000;">Okt</option>
                                                    <option value="11" style="color: #000;">Nov</option>
                                                    <option value="12" style="color: #000;">Des</option>
                                                </select>
                                                <select class="" style="color: #000;">
                                                    <?php for($i=2016; $i < 2021; $i++){ ?>
                                                    <option value="<?php echo $i; ?>" style="color: #000;"><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="pull-left">
                                                <span style="margin-left: 15px; margin-right: 15px">
                                                    To
                                                </span>
                                                <select class="" style="color: #000;">
                                                    <?php for($i=1; $i < 32; $i++){ ?>
                                                    <option value="<?php echo $i; ?>" style="color: #000;"><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <select class="" style="color: #000;">
                                                    <option value="01" style="color: #000;">Jan</option>
                                                    <option value="02" style="color: #000;">Feb</option>
                                                    <option value="03" style="color: #000;">Mar</option>
                                                    <option value="04" style="color: #000;">Apr</option>
                                                    <option value="05" style="color: #000;">Mei</option>
                                                    <option value="06" style="color: #000;">Jun</option>
                                                    <option value="07" style="color: #000;">Jul</option>
                                                    <option value="08" style="color: #000;">Agu</option>
                                                    <option value="09" style="color: #000;">Sep</option>
                                                    <option value="10" style="color: #000;">Okt</option>
                                                    <option value="11" style="color: #000;">Nov</option>
                                                    <option value="12" style="color: #000;">Des</option>
                                                </select>
                                                <select class="" style="color: #000;">
                                                    <?php for($i=2016; $i < 2021; $i++){ ?>
                                                    <option value="<?php echo $i; ?>" style="color: #000;"><?php echo $i; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <button class="pull-left" type="button" style="width: 51px; height: 18px; padding: 0px; border: 0px; margin-top: 7px; margin-left: 15px; margin-right: 15px;">
                                                <img src="../img/icon/cari-orange.png" class="pull-left">
                                            </button>
                                        </div>
                                    </ul>
                                </div>
                                <span class="clearfix"></span>
                                <p class="text-center" style="width: 100%; border-bottom: 1px solid #bbbdbf; padding-bottom: 16px;">
                                        Indeks Berita Hari Ini : Sabtu, 20 Februari 2016
                                </p>
                                <span class="clearfix"></span>
                                <ul class="nav nav-tabs s-pp-nav-left pull-left" id="myTab00" style=""> <!-- f-flyLeft-nav flyLeftAbsolute -->
                                    <li class="" style="width: inherit; display: block;"><a href="#an0" data-target="#an0" data-toggle="tab">Keseluruhan</a></li>
                                    <li class="active" style="width: inherit; display: block;"><a href="#an1" data-target="#an1" data-toggle="tab">Nasional</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#an2" data-target="#an2" data-toggle="tab">Pemilu</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-kbf" data-target="#m-kbf" data-toggle="tab">Daerah</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-kbv" data-target="#m-kbv" data-toggle="tab">Ekonomi</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-dst" data-target="#m-dst" data-toggle="tab">Internasional</a></li>

                                    <li class="" style="width: inherit; display: block;"><a href="#m-kbfa" data-target="#m-kbfa" data-toggle="tab">Olahraga</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-kbvb" data-target="#m-kbvb" data-toggle="tab">Entertaiment</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-dstc" data-target="#m-dstc" data-toggle="tab">Teknologi</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-lifestyle" data-target="#m-lifestyle" data-toggle="tab">Lifestyle</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-pendidikan" data-target="#m-pendidikan" data-toggle="tab">Pendidikan</a></li>

                                    <li class="" style="width: inherit; display: block;"><a href="#m-kriminal" data-target="#m-kriminal" data-toggle="tab">Kriminal</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-features" data-target="#m-features" data-toggle="tab">Features</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-top-story" data-target="#m-top-story" data-toggle="tab">Top Story</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-jabodetabek" data-target="#m-jabodetabek" data-toggle="tab">Jabodetabek</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-historiana" data-target="#m-historiana" data-toggle="tab">Historiana</a></li>

                                    <li class="" style="width: inherit; display: block;"><a href="#m-english" data-target="#m-english" data-toggle="tab">English</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-video" data-target="#m-video" data-toggle="tab">Video</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-foto" data-target="#m-foto" data-toggle="tab">Foto</a></li>
                                </ul>

                                <div class="tab-content pull-right" style="width: calc(100% - 160px); display: block;">
                                    <div class="tab-pane" id="an0" style="padding-top: 10px; ">
                                        1
                                    </div>
                                    <div class="tab-pane active" id="an1" style="padding-top: 10px; ">
                                        <?php for($i=0; $i < 20; $i++){ ?>
                                        <div class="baris-indeks" style="">
                                            <div class="waktu" style="">
                                                <span>14.47 WIB</span>
                                                <span class="clearfix"></span>
                                                <span>Sabtu, 20 2 2016</span>
                                            </div>
                                            <div class="konektor" style="">
                                                <span class="node" style="">
                                                </span>
                                            </div>
                                            <div class="berita" style="">
                                                <span style="text-transform: uppercase;">
                                                    Nasional
                                                </span>
                                                <span class="clearfix"></span>
                                                <span>
                                                    KPK Usut Putusan Kasasi Permainan Tersangka Suap di MA
                                                </span>
                                            </div>
                                        </div>
                                        <?php } ?>

                                    </div>
                                    <div class="tab-pane" id="an2" style="padding-top: 10px; ">
                                        3
                                    </div>
                                    <div class="tab-pane" id="m-kbf" style="padding-top: 10px; ">
                                        4
                                    </div>
                                    <div class="tab-pane " id="m-kbv" style="padding-top: 10px; ">
                                        5
                                    </div>
                                    <div class="tab-pane" id="m-dst" style="padding-top: 10px; ">
                                        6
                                    </div>

                                    <div class="tab-pane" id="m-kbfa" style="padding-top: 10px; ">
                                        7
                                    </div>
                                    <div class="tab-pane" id="m-kbvb" style="padding-top: 10px; ">
                                        8
                                    </div>
                                    <div class="tab-pane" id="m-dstc" style="padding-top: 10px; ">
                                        9
                                    </div>

                                    <div class="tab-pane" id="m-lifestyle" style="padding-top: 10px; ">
                                        10
                                    </div>
                                    <div class="tab-pane" id="m-pendidikan" style="padding-top: 10px; ">
                                        11
                                    </div>
                                    <div class="tab-pane" id="m-kriminal" style="padding-top: 10px; ">
                                        12
                                    </div>
                                    <div class="tab-pane" id="m-features" style="padding-top: 10px; ">
                                       13
                                    </div>
                                    <div class="tab-pane" id="m-top-story" style="padding-top: 10px; ">
                                        14
                                    </div>
                                    <div class="tab-pane" id="m-jabodetabek" style="padding-top: 10px; ">
                                        15
                                    </div>
                                    <div class="tab-pane" id="m-historiana" style="padding-top: 10px; ">
                                       16
                                    </div>
                                    <div class="tab-pane" id="m-english" style="padding-top: 10px; ">
                                        17
                                    </div>
                                    <div class="tab-pane" id="m-video" style="padding-top: 10px; ">
                                        18
                                    </div>
                                    <div class="tab-pane" id="m-foto" style="padding-top: 10px; ">
                                        19
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                    </div>
                </div>
            </div>


            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

        </section>




        <!--Footer-->
        <!--==================================================-->
        <?php include('_footer.php'); ?>
        <style type="text/css">
            #chartdiv {
                width       : 100%;
                height      : 500px;
                font-size   : 11px;
            }
        </style>
        <script type="text/javascript">
            var chart = AmCharts.makeChart( "chartdiv", {
              "type": "pie",
              "theme": "light",
              "dataProvider": [ {
                "country": "Lithuania",
                "litres": 501.9
            }, {
                "country": "Czech Republic",
                "litres": 301.9
            }, {
                "country": "Ireland",
                "litres": 201.1
            }, {
                "country": "Germany",
                "litres": 165.8
            }, {
                "country": "Australia",
                "litres": 139.9
            }, {
                "country": "Austria",
                "litres": 128.3
            }, {
                "country": "UK",
                "litres": 99
            }, {
                "country": "Belgium",
                "litres": 60
            }, {
                "country": "The Netherlands",
                "litres": 50
            } ],
            "valueField": "litres",
            "titleField": "country",
            "balloon":{
             "fixedPosition":true
         },
         "export": {
            "enabled": true
        }
    } );
</script>

    </body>
</html>
