<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Advertisement Top -->
        <!--==================================================-->
        <!-- Header -->
        <!--==================================================-->
        <?php include('_header.php'); ?>


        <!--Content-->
        <!--==================================================-->
        <section class="s-main-content">

            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

            <div class="container-fluid">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                            <div class="" style="position: relative;">
                                <ul class="nav nav-tabs s-pp-nav-right f-flyright-nav flyRightAbsolute" id="myTab00">
                                    <li class="active" style="width: inherit; display: block;"><a href="#m-pa" data-target="#m-pa" data-toggle="tab">Tentang Kami</a></li>
                                    <li  style="width: inherit; display: block;"><a href="#m-ep" data-target="#m-ep" data-toggle="tab">Positioning</a></li>
                                    <li  style="width: inherit; display: block;"><a href="#m-kb" data-target="#m-kb" data-toggle="tab">Differensiasi</a></li>
                                    <li  style="width: inherit; display: block;"><a href="#m-kbf" data-target="#m-kbf" data-toggle="tab">Tagline</a></li>
                                    <li  style="width: inherit; display: block;"><a href="#m-kbv" data-target="#m-kbv" data-toggle="tab">Survey / Pembaca</a></li>
                                    <li  style="width: inherit; display: block;"><a href="#m-dst" data-target="#m-dst" data-toggle="tab">Distribusi</a></li>
                                </ul>

                                <div class="tab-content pull-left" style="width: calc(100% - 228px); margin-right: 20px; display: block; border-right: 2px solid #bfbdbd;">
                                    <div class=" active" id="m-pa" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Tentang Kami</span>
                                        </div>
                                        Diterbitkan pada tahun 2003, Makassar Terkini merupakan majalah bulanan komunitas dalam bahasa Indonesia untuk wilayah Makassar. Tujuan penerbitan majalah ini adalah sebagai media komunikasi dan informasi antar warga di Makassar.

                                        Majalah Makassar Terkini adalah satu-satunya majalah komunitas yang menjangkau komunitas kaum terpelajar dan memiliki tingkat kesejahteraan hidup yang baik.

                                        Dengan berfokus pada kualitas, cakupan berita Makassar Terkini meliputi topik-topik seputar info metro, info bisnis & dagang, info wisata & hiburan, info keluarga dan info teknologi.

                                        Makassar Terkini telah menjadi peluang penting bagi para pengiklan yang ingin menjangkau komunitas Makasar
                                    </div>
                                    <div class="" id="m-ep" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Positioning</span>
                                        </div>
                                        Majalah komunitas Kawasan Makassar
                                    </div>
                                    <div class="" id="m-kb" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Diferensiasi</span>
                                        </div>
                                        1. Pelopor dan market leader majalah komunitas di kawasan Makassar
                                        2. Dikelola secara profesional dengan fokus pada kualitas
                                        3. Majalah dengan jumlah oplah terbesar di kawasan Makassar
                                    </div>
                                    <div class="" id="m-kbf" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Tagline</span>
                                        </div>
                                        Semakin informatif, efektif dan terpercaya!
                                    </div>
                                    <div class="" id="m-kbv" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Survey Pembaca</span>
                                        </div>
                                        <div id="chartdiv"></div>
                                    </div>
                                    <div class="" id="m-dst" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Distribusi</span>
                                        </div>
                                        Transportasi, Asuransi,  Elektronik &amp; Komputer, Fashion, Finance, Food &amp; Drink, Hotel, Jasa, Kesehatan &amp; Kecantikan, Komunikasi, Media, Otomotif, Pendidikan, Perlengkapan Rumah Tangga, Property, Kantor Pemerintahan
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                    </div>
                </div>
            </div>


            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

        </section>




        <!--Footer-->
        <!--==================================================-->
        <?php include('_footer.php'); ?>
        <style type="text/css">
            #chartdiv {
                width       : 100%;
                height      : 500px;
                font-size   : 11px;
            }
        </style>
        <script type="text/javascript">
            var chart = AmCharts.makeChart( "chartdiv", {
              "type": "pie",
              "theme": "light",
              "dataProvider": [ {
                "country": "Lithuania",
                "litres": 501.9
            }, {
                "country": "Czech Republic",
                "litres": 301.9
            }, {
                "country": "Ireland",
                "litres": 201.1
            }, {
                "country": "Germany",
                "litres": 165.8
            }, {
                "country": "Australia",
                "litres": 139.9
            }, {
                "country": "Austria",
                "litres": 128.3
            }, {
                "country": "UK",
                "litres": 99
            }, {
                "country": "Belgium",
                "litres": 60
            }, {
                "country": "The Netherlands",
                "litres": 50
            } ],
            "valueField": "litres",
            "titleField": "country",
            "balloon":{
             "fixedPosition":true
         },
         "export": {
            "enabled": true
        }
    } );
</script>

    </body>
</html>
