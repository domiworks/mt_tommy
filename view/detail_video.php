<!DOCTYPE html>
<html>
<head>
    <title>Detail Video</title>
    <?php include('_css.php'); ?>

    <style type="text/css">
    .shareFixed {
        position: fixed; /*top: 130px;*/ width: 80px;  border-top-left-radius: 5px; border-bottom-left-radius; border: 1px solid #ed1c24; padding-bottom: 10px;
    }
    .shareAbsolute {
        position: absolute; /*top: 220px;*/ transform: translateY(0px); width: 80px;  border-top-left-radius: 5px; border-bottom-left-radius; border: 1px solid #ed1c24; padding-bottom: 10px;
    }
    </style>
</head>
<body class="body-offcanvas">
    <!--==================================================-->
    <!-- Header -->
    <!--==================================================-->
    <?php include('_header.php'); ?>

    <!--Content-->
    <!--==================================================-->
    <div class="share-container shareAbsolute" style="">
        <div class="s-share-counter text-center" style="background: #ed1c24; line-height: 30px;">
            <span style="color: #fff; line-height: 30px; font-size: 16px;">143</span>
            <span style="color: #fff; line-height: 30px; font-size: 10px;">Share</span>
        </div>
        <a href="#" style="background: url('../img/icon/side-fb.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
        <a href="#" style="background: url('../img/icon/side-twitter.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
        <a href="#" style="background: url('../img/icon/side-gp.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
        <a href="#" style="background: url('../img/icon/side-path.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
        <a href="#" style="background: url('../img/icon/side-instagram.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
    </div>
    <script type="text/javascript">

        function sharePositioning(){
            var marginLeft = ($(window).width() - 1100)/2;
            var leftFixed = (marginLeft - 80 - 15);
            $('.share-container').css('left', leftFixed);
        }

        sharePositioning();

        $(document).ready(function() {

            $(window).resize(function(){
                sharePositioning();
            });


            $(window).scroll(function () {
                if ( $(window).scrollTop() + $('.s-header').height() > $('.s-header').height() ) {
                    $('.share-container').addClass('shareFixed');
                    $('.share-container').removeClass('shareAbsolute');
                } else {
                    $('.share-container').removeClass('shareFixed');
                    $('.share-container').addClass('shareAbsolute');
                }
            });


        });
    </script>
    <section class="s-main-content">



        <!-- Adv Middle Top -->
        <!--==================================================-->
        <div class="adv adv-1100-90">
            ADVERTISE 1100 x 90
        </div>

        <!-- Middle Content -->
        <!--==================================================-->
        <div class="s-col-2-per-3 pull-left" style="/*background: #123;*/">
            <iframe width="790" height="466" src="https://www.youtube.com/embed/sIo3tYrd_5w" frameborder="0" allowfullscreen></iframe>

            <div style="background: #fefbe1; padding: 10px 20px;">
                <h4>
                    Deskripsi Video :
                </h4>
                <p>
                    Unggah Foto Haters yang Hina Chika Jessica, Deddy Corbuzier Dikritik Netizen
                </p>

            </div>

            <div class="adv adv-790-100">
                ADVERTISE 790 x 100
            </div>
            <iframe id="fe6ea43817dec8" name="f1f490dae234154" scrolling="no" title="Facebook Social Plugin" class="fb_ltr" src="https://www.facebook.com/plugins/comments.php?api_key=113869198637480&amp;channel_url=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D42%23cb%3Df154d65c633ed2c%26domain%3Ddevelopers.facebook.com%26origin%3Dhttps%253A%252F%252Fdevelopers.facebook.com%252Ff3a4d796e7cccfc%26relation%3Dparent.parent&amp;href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2Fcomments%23configurator&amp;locale=en_US&amp;numposts=5&amp;sdk=joey&amp;version=v2.6&amp;width=550" style="border: none; overflow: hidden; height: 1500px; width: 100%;"></iframe>
            <div style="background: #fad4bb; padding: 15px 30px; overflow: hidden;">
                <div class="pull-left">
                    <img src="../img/icon/news-ltr.png">
                </div>

                <div class="pull-left" style="margin-left: 30px;">
                    <h2 style="margin: 0px; color: #ed1c24; font-weight: 700;">Subscribe !!!</h2>
                    <p>
                        Jangan sampai ketinggalan berita terkini, <br/>
                        langganan newsletter kami sekarang!
                    </p>
                </div>
                <div style="margin-top: 22px;">
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Masukkan email anda" aria-describedby="basic-addon2">
                      <a href="#" class="input-group-addon" id="basic-addon2" style="background: #ff6600; color: #fff;">Kirim</a>
                  </div>
              </div>
            </div>

            <?php include('node_video_lainnya.php'); ?>
        </div>


        <!-- Middle Content -->
        <!--==================================================-->
        <div class="s-col-1-per-3 pull-right" style="">
            <div class="adv adv-300-250">
                ADVERTISE 300 x 250
            </div>
            <?php include('node_berita_pop.php'); ?>
            <div class="adv adv-300-250">
                ADVERTISE 300 x 250
            </div>
            <?php include('node_event_terkini.php'); ?>
            <?php for($i=0; $i < 10; $i++){ ?>
            <div class="adv adv-300-250">
                ADVERTISE 300 x 250
            </div>
            <?php } ?>
        </div>



    </section>
        <span class="clearfix"></span>
        <div class="adv adv-1100-90">
            ADVERTISE 1100 x 90
        </div>





    <!--Footer-->
    <!--==================================================-->
    <?php include('_footer.php'); ?>





</body>
</html>
