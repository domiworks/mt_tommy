<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Advertisement Top -->
        <!--==================================================-->
        <!-- Header -->
        <!--==================================================-->
        <?php include('_header.php'); ?>


        <!--Content-->
        <!--==================================================-->
        <section class="s-main-content">

            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

            <div class="container-fluid">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                            <div class="" style="position: relative;">
                                <ul class="nav nav-tabs s-pp-nav-right f-flyright-nav flyRightAbsolute" id="myTab00" style="">
                                    <li class="active" style="width: inherit; display: block;"><a href="#an0" data-target="#an0" data-toggle="tab">Ruang Lingkup</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#an1" data-target="#an1" data-toggle="tab">Verifikasi</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#an2" data-target="#an2" data-toggle="tab">Isi Buatan Pengguna</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-kbf" data-target="#m-kbf" data-toggle="tab">Ralat, Koreksi, dan Hak Jawab</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-kbv" data-target="#m-kbv" data-toggle="tab">Pencabutan Berita</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-dst" data-target="#m-dst" data-toggle="tab">Iklan</a></li>

                                    <li class="" style="width: inherit; display: block;"><a href="#m-kbfa" data-target="#m-kbfa" data-toggle="tab">Hak Cipta</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-kbvb" data-target="#m-kbvb" data-toggle="tab">Pencantuman Pedoman</a></li>
                                    <li class="" style="width: inherit; display: block;"><a href="#m-dstc" data-target="#m-dstc" data-toggle="tab">Sengketa</a></li>
                                </ul>

                                <div class="tab-content pull-left" style="width: calc(100% - 228px); margin-right: 20px; display: block; border-right: 2px solid #bfbdbd;">
                                    <div class="" id="an0" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Ruang Lingkup</span>
                                        </div>
                                    </div>
                                    <div class="" id="an1" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Verifikasi</span>
                                        </div>
                                    </div>
                                    <div class="" id="an2" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Isi Buatan Pengguna</span>
                                        </div>
                                    </div>
                                    <div class="" id="m-kbf" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Ralat, Koreksi, dan Hak Jawab</span>
                                        </div>
                                    </div>
                                    <div class="active" id="m-kbv" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Pencabutan Berita</span>
                                        </div>
                                        <p>
                                                Kemerdekaan berpendapat, kemerdekaan berekspresi, dan kemerdekaan pers adalah hak asasi manusia yang dilindungi Pancasila, Undang-Undang Dasar 1945, dan Deklarasi Universal Hak Asasi Manusia PBB. Keberadaan media siber di Indonesia juga merupakan bagian dari kemerdekaan berpendapat, kemerdekaan berekspresi, dan kemerdekaan pers.

                                                </br></br>Media siber memiliki karakter khusus sehingga memerlukan pedoman agar pengelolaannya dapat dilaksanakan secara profesional, memenuhi fungsi, hak, dan kewajibannya sesuai Undang-Undang Nomor 40 Tahun 1999 tentang Pers dan Kode Etik Jurnalistik. Untuk itu Dewan Pers bersama organisasi pers, pengelola media siber, dan masyarakat menyusun Pedoman Pemberitaan Media Siber sebagai berikut:

                                                </br></br>

                                                <span class="tk-demografi-head">
                                                    1. Ruang Lingkup
                                                </span>
                                                <p class="tk-demografi-content">
                                                    Media Siber adalah segala bentuk media yang menggunakan wahana internet dan melaksanakan kegiatan jurnalistik, serta memenuhi persyaratan Undang-Undang Pers dan Standar Perusahaan Pers yang ditetapkan Dewan Pers.

                                                    </br></br>Isi Buatan Pengguna (User Generated Content) adalah segala isi yang dibuat dan atau dipublikasikan oleh pengguna media siber, antara lain, artikel, gambar, komentar, suara, video dan berbagai bentuk unggahan yang melekat pada media siber, seperti blog, forum, komentar pembaca atau pemirsa, dan bentuk lain.
                                                </p>

                                                <span class="tk-demografi-head">
                                                    2. Verifikasi dan keberimbangan berita
                                                </span>
                                                <p class="tk-demografi-content">
                                                    Pada prinsipnya setiap berita harus melalui verifikasi.</br>
                                                    Berita yang dapat merugikan pihak lain memerlukan verifikasi pada berita yang sama untuk memenuhi prinsip akurasi dan keberimbangan.

                                                    </br></br>

                                                    Ketentuan dalam butir (a) di atas dikecualikan, dengan syarat:
                                                    </br>Berita benar-benar mengandung kepentingan publik yang bersifat mendesak;
                                                    </br>Sumber berita yang pertama adalah sumber yang jelas disebutkan identitasnya, kredibel dan kompeten;
                                                    </br>Subyek berita yang harus dikonfirmasi tidak diketahui keberadaannya dan atau tidak dapat diwawancarai;
                                                    </br>Media memberikan penjelasan kepada pembaca bahwa berita tersebut masih memerlukan verifikasi lebih lanjut yang diupayakan dalam waktu secepatnya. Penjelasan dimuat pada bagian akhir dari berita yang sama, di dalam kurung dan menggunakan huruf miring.
                                                    </br>Setelah memuat berita sesuai dengan butir (c), media wajib meneruskan upaya verifikasi, dan setelah verifikasi didapatkan, hasil verifikasi dicantumkan pada berita pemutakhiran (update) dengan tautan pada berita yang belum terverifikasi.
                                                </p>

                                                <span class="tk-demografi-head">
                                                    3. Isi Buatan Pengguna (User Generated Content)
                                                </span>
                                                <p class="tk-demografi-content">
                                                    Media siber wajib mencantumkan syarat dan ketentuan mengenai Isi Buatan Pengguna yang tidak bertentangan dengan Undang-Undang No. 40 tahun 1999 tentang Pers dan Kode Etik Jurnalistik, yang ditempatkan secara terang dan jelas.
                                                    </br>Media siber mewajibkan setiap pengguna untuk melakukan registrasi keanggotaan dan melakukan proses log-in terlebih dahulu untuk dapat mempublikasikan semua bentuk Isi Buatan Pengguna. Ketentuan mengenai log-in akan diatur lebih lanjut.
                                                    </br>Dalam registrasi tersebut, media siber mewajibkan pengguna memberi persetujuan tertulis bahwa Isi Buatan Pengguna yang dipublikasikan:
                                                    </br>Tidak memuat isi bohong, fitnah, sadis dan cabul;
                                                    </br>Tidak memuat isi yang mengandung prasangka dan kebencian terkait dengan suku, agama, ras, dan antargolongan (SARA), serta menganjurkan tindakan kekerasan;
                                                    </br>Tidak memuat isi diskriminatif atas dasar perbedaan jenis kelamin dan bahasa, serta tidak merendahkan martabat orang lemah, miskin, sakit, cacat jiwa, atau cacat jasmani.
                                                    </br>Media siber memiliki kewenangan mutlak untuk mengedit atau menghapus Isi Buatan Pengguna yang bertentangan dengan butir (c).
                                                    </br>Media siber wajib menyediakan mekanisme pengaduan Isi Buatan Pengguna yang dinilai melanggar ketentuan pada butir (c). Mekanisme tersebut harus disediakan di tempat yang dengan mudah dapat diakses pengguna.
                                                    </br>Media siber wajib menyunting, menghapus, dan melakukan tindakan koreksi setiap Isi Buatan Pengguna yang dilaporkan dan melanggar ketentuan butir (c), sesegera mungkin secara proporsional selambat-lambatnya 2 x 24 jam setelah pengaduan diterima.
                                                    </br>Media siber yang telah memenuhi ketentuan pada butir (a), (b), (c), dan (f) tidak dibebani tanggung jawab atas masalah yang ditimbulkan akibat pemuatan isi yang melanggar ketentuan pada butir (c).
                                                    </br>Media siber bertanggung jawab atas Isi Buatan Pengguna yang dilaporkan bila tidak mengambil tindakan koreksi setelah batas waktu sebagaimana tersebut pada butir (f).
                                                </p>

                                                <span class="tk-demografi-head">
                                                    4. Ralat, Koreksi, dan Hak Jawab
                                                </span>
                                                <p class="tk-demografi-content">
                                                    Ralat, koreksi, dan hak jawab mengacu pada Undang-Undang Pers, Kode Etik Jurnalistik, dan Pedoman Hak Jawab yang ditetapkan Dewan Pers.
                                                    </br>Ralat, koreksi dan atau hak jawab wajib ditautkan pada berita yang diralat, dikoreksi atau yang diberi hak jawab.
                                                    </br>Di setiap berita ralat, koreksi, dan hak jawab wajib dicantumkan waktu pemuatan ralat, koreksi, dan atau hak jawab tersebut.
                                                    </br>Bila suatu berita media siber tertentu disebarluaskan media siber lain, maka:
                                                    </br>Tanggung jawab media siber pembuat berita terbatas pada berita yang dipublikasikan di media siber tersebut atau media siber yang berada di bawah otoritas teknisnya;
                                                    </br>Koreksi berita yang dilakukan oleh sebuah media siber, juga harus dilakukan oleh media siber lain yang mengutip berita dari media siber yang dikoreksi itu;
                                                    </br>Media yang menyebarluaskan berita dari sebuah media siber dan tidak melakukan koreksi atas berita sesuai yang dilakukan oleh media siber pemilik dan atau pembuat berita tersebut, bertanggung jawab penuh atas semua akibat hukum dari berita yang tidak dikoreksinya itu.
                                                    </br>Sesuai dengan Undang-Undang Pers, media siber yang tidak melayani hak jawab dapat dijatuhi sanksi hukum pidana denda paling banyak Rp500.000.000 (Lima ratus juta rupiah).
                                                </p>

                                                <span class="tk-demografi-head">
                                                    5. Pencabutan Berita
                                                </span>
                                                <p class="tk-demografi-content">
                                                    Berita yang sudah dipublikasikan tidak dapat dicabut karena alasan penyensoran dari pihak luar redaksi, kecuali terkait masalah SARA, kesusilaan, masa depan anak, pengalaman traumatik korban atau berdasarkan pertimbangan khusus lain yang ditetapkan Dewan Pers.
                                                    </br>Media siber lain wajib mengikuti pencabutan kutipan berita dari media asal yang telah dicabut.
                                                    </br>Pencabutan berita wajib disertai dengan alasan pencabutan dan diumumkan kepada publik.
                                                </p>

                                                <span class="tk-demografi-head">
                                                    6. Iklan
                                                </span>
                                                <p class="tk-demografi-content">
                                                    Media siber wajib membedakan dengan tegas antara produk berita dan iklan.
                                                    </br>Setiap berita/artikel/isi yang merupakan iklan dan atau isi berbayar wajib mencantumkan keterangan .advertorial., .iklan., .ads., .sponsored., atau kata lain yang menjelaskan bahwa berita/artikel/isi tersebut adalah iklan.
                                                </p>

                                                <span class="tk-demografi-head">
                                                    7. Hak Cipta
                                                </span>
                                                <p class="tk-demografi-content">
                                                    Media siber wajib menghormati hak cipta sebagaimana diatur dalam peraturan perundang-undangan yang berlaku.
                                                </p>

                                                <span class="tk-demografi-head">
                                                    8. Pencantuman Pedoman
                                                </span>
                                                <p class="tk-demografi-content">
                                                    Media siber wajib mencantumkan Pedoman Pemberitaan Media Siber ini di medianya secara terang dan jelas.
                                                </p>

                                                <span class="tk-demografi-head">
                                                    9. Sengketa
                                                </span>
                                                <p class="tk-demografi-content">
                                                    Penilaian akhir atas sengketa mengenai pelaksanaan Pedoman Pemberitaan Media Siber ini diselesaikan oleh Dewan Pers.
                                                </p>
                                                </br>
                                                </br>
                                                <p class="tk-demografi-content">
                                                    Jakarta, 3 Februari 2012
                                                    </br>(Pedoman ini ditandatangani oleh Dewan Pers dan komunitas pers di Jakarta, 3 Februari 2012).
                                                </p>
                                        </p>
                                    </div>
                                    <div class="" id="m-dst" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Iklan</span>
                                        </div>
                                    </div>

                                    <div class="" id="m-kbfa" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Hak Cipta</span>
                                        </div>
                                    </div>
                                    <div class="" id="m-kbvb" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Pencantuman Pedoman</span>
                                        </div>
                                        <div id="chartdiv"></div>
                                    </div>
                                    <div class="" id="m-dstc" style="padding-top: 10px; padding-right: 10px;">
                                        <div class="s-fancy-header-red">
                                            <span class="s-title">Sengketa</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                    </div>
                </div>
            </div>


            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

        </section>




        <!--Footer-->
        <!--==================================================-->
        <?php include('_footer.php'); ?>
        <style type="text/css">
            #chartdiv {
                width       : 100%;
                height      : 500px;
                font-size   : 11px;
            }
        </style>
        <script type="text/javascript">
            var chart = AmCharts.makeChart( "chartdiv", {
              "type": "pie",
              "theme": "light",
              "dataProvider": [ {
                "country": "Lithuania",
                "litres": 501.9
            }, {
                "country": "Czech Republic",
                "litres": 301.9
            }, {
                "country": "Ireland",
                "litres": 201.1
            }, {
                "country": "Germany",
                "litres": 165.8
            }, {
                "country": "Australia",
                "litres": 139.9
            }, {
                "country": "Austria",
                "litres": 128.3
            }, {
                "country": "UK",
                "litres": 99
            }, {
                "country": "Belgium",
                "litres": 60
            }, {
                "country": "The Netherlands",
                "litres": 50
            } ],
            "valueField": "litres",
            "titleField": "country",
            "balloon":{
             "fixedPosition":true
         },
         "export": {
            "enabled": true
        }
    } );
</script>

    </body>
</html>
