
<div class="container-fluid">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                <div class="news-head-bt">
                    <img src="../img/icon/foto-populer.png" style="width: 116px; height: 34px;">

                    <div class="redspan">
                        <div class="greyspan">
                        </div>
                    </div>

                    <div style="position: relative; float: right; text-align: center; margin-top: -4px;">
                        <a href="#" class="pull-left">
                            <span style="font-size: 12px; color: #ec2227;">LIHAT SEMUA</span>
                            <img src="../img/icon/all-window-red.png" height="10px">
                        </a>
                        <span class="pull-left" style="margin-left: 5px; margin-right: 5px;">atau</span>
                        <a class="s-tick" href="#foto-populer-carousel" role="button" data-slide="prev" style="top: 7px; right: 36px;">
                            <img src="../img/icon/news-tick-left.png" width="12px">
                        </a>
                        <a class="s-tick" href="#foto-populer-carousel" role="button" data-slide="next" style="top: 7px; right: 16px;">
                            <img src="../img/icon/news-tick-right.png" width="12px">
                        </a>
                    </div>
                </div>


                <div id="foto-populer-carousel" class="carousel slide" data-ride="carousel" style="height: inherit; position: relative;">
                    <div class="carousel-inner" role="listbox">
                        <div class="item active " style="text-align: justify;">
                            <div class="news-module-ver">
                                <div class="news-module-ver-img">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-37097.jpg">
                                </div>

                                <div class="news-module-ver-content font-sourceSansPro">
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left">
                                            Telah dilihat 400 kali
                                        </div>
                                    </div>

                                    <div class="news-module-ver-title">
                                        Jejak Sejarah yang Hilang di Makassar
                                    </div>
                                </div>
                            </div>

                            <div class="news-module-ver">
                                <div class="news-module-ver-img">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-18952.jpg">
                                </div>

                                <div class="news-module-ver-content font-sourceSansPro">
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left">
                                            Telah dilihat 400 kali
                                        </div>
                                    </div>

                                    <div class="news-module-ver-title">
                                        Jejak Sejarah yang Hilang di Makassar
                                    </div>
                                </div>
                            </div>

                            <div class="news-module-ver">
                                <div class="news-module-ver-img">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-171934.jpg">
                                </div>

                                <div class="news-module-ver-content font-sourceSansPro">
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left">
                                            Telah dilihat 400 kali
                                        </div>
                                    </div>

                                    <div class="news-module-ver-title">
                                        Jejak Sejarah yang Hilang di Makassar
                                    </div>
                                </div>
                            </div>
                            <div class="news-module-ver">
                                <div class="news-module-ver-img">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-24622.jpg">
                                </div>

                                <div class="news-module-ver-content font-sourceSansPro">
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left">
                                            Telah dilihat 400 kali
                                        </div>
                                    </div>

                                    <div class="news-module-ver-title">
                                        Jejak Sejarah yang Hilang di Makassar
                                    </div>
                                </div>
                            </div>

                            <div class="news-module-ver">
                                <div class="news-module-ver-img">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-52206.jpg">
                                </div>

                                <div class="news-module-ver-content font-sourceSansPro">
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left">
                                            Telah dilihat 400 kali
                                        </div>
                                    </div>

                                    <div class="news-module-ver-title">
                                        Jejak Sejarah yang Hilang di Makassar
                                    </div>
                                </div>
                            </div>

                            <div class="news-module-ver">
                                <div class="news-module-ver-img">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-108489.jpg">
                                </div>

                                <div class="news-module-ver-content font-sourceSansPro">
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left">
                                            Telah dilihat 400 kali
                                        </div>
                                    </div>

                                    <div class="news-module-ver-title">
                                        Jejak Sejarah yang Hilang di Makassar
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item " style="text-align: justify;">
                            <div class="news-module-ver">
                                <div class="news-module-ver-img">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-18952.jpg">
                                </div>

                                <div class="news-module-ver-content font-sourceSansPro">
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left">
                                            Telah dilihat 400 kali
                                        </div>
                                    </div>

                                    <div class="news-module-ver-title">
                                        Jejak Sejarah yang Hilang di Makassar
                                    </div>
                                </div>
                            </div>

                            <div class="news-module-ver">
                                <div class="news-module-ver-img">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-171934.jpg">
                                </div>

                                <div class="news-module-ver-content font-sourceSansPro">
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left">
                                            Telah dilihat 400 kali
                                        </div>
                                    </div>

                                    <div class="news-module-ver-title">
                                        Jejak Sejarah yang Hilang di Makassar
                                    </div>
                                </div>
                            </div>
                            <div class="news-module-ver">
                                <div class="news-module-ver-img">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-37097.jpg">
                                </div>

                                <div class="news-module-ver-content font-sourceSansPro">
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left">
                                            Telah dilihat 400 kali
                                        </div>
                                    </div>

                                    <div class="news-module-ver-title">
                                        Jejak Sejarah yang Hilang di Makassar
                                    </div>
                                </div>
                            </div>

                            <div class="news-module-ver">
                                <div class="news-module-ver-img">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-52206.jpg">
                                </div>

                                <div class="news-module-ver-content font-sourceSansPro">
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left">
                                            Telah dilihat 400 kali
                                        </div>
                                    </div>

                                    <div class="news-module-ver-title">
                                        Jejak Sejarah yang Hilang di Makassar
                                    </div>
                                </div>
                            </div>

                            <div class="news-module-ver">
                                <div class="news-module-ver-img">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-108489.jpg">
                                </div>

                                <div class="news-module-ver-content font-sourceSansPro">
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left">
                                            Telah dilihat 400 kali
                                        </div>
                                    </div>

                                    <div class="news-module-ver-title">
                                        Jejak Sejarah yang Hilang di Makassar
                                    </div>
                                </div>
                            </div>
                            <div class="news-module-ver">
                                <div class="news-module-ver-img">
                                    <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-24622.jpg">
                                </div>

                                <div class="news-module-ver-content font-sourceSansPro">
                                    <div class="news-module-ver-etc">
                                        <div class="news-module-ver-time pull-left">
                                            Telah dilihat 400 kali
                                        </div>
                                    </div>

                                    <div class="news-module-ver-title">
                                        Jejak Sejarah yang Hilang di Makassar
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>





            </div>
        </div>
    </div>
</div>



