<!DOCTYPE html>
<html>
<head>
    <title>Detail News</title>
    <?php include('_css.php'); ?>

    <style type="text/css">
    .shareFixed {
        position: fixed; /*top: 130px;*/ width: 80px;  border-top-left-radius: 5px; border-bottom-left-radius; border: 1px solid #ed1c24; padding-bottom: 10px;
    }
    .shareAbsolute {
        position: absolute; /*top: 220px;*/ transform: translateY(0px); width: 80px;  border-top-left-radius: 5px; border-bottom-left-radius; border: 1px solid #ed1c24; padding-bottom: 10px;
    }
    </style>
</head>
<body class="body-offcanvas">
    <!--==================================================-->
    <!-- Header -->
    <!--==================================================-->
    <?php include('_header.php'); ?>

    <!--Content-->
    <!--==================================================-->
    <div class="share-container shareAbsolute" style="">
        <div class="s-share-counter text-center" style="background: #ed1c24; line-height: 30px;">
            <span style="color: #fff; line-height: 30px; font-size: 16px;">143</span>
            <span style="color: #fff; line-height: 30px; font-size: 10px;">Share</span>
        </div>
        <a href="#" style="background: url('../img/icon/side-fb.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
        <a href="#" style="background: url('../img/icon/side-twitter.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
        <a href="#" style="background: url('../img/icon/side-gp.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
        <a href="#" style="background: url('../img/icon/side-path.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
        <a href="#" style="background: url('../img/icon/side-instagram.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
    </div>
    <script type="text/javascript">

        function sharePositioning(){
            var marginLeft = ($(window).width() - 1100)/2;
            var leftFixed = (marginLeft - 80 - 15);
            $('.share-container').css('left', leftFixed);
        }

        sharePositioning();

        $(document).ready(function() {

            $(window).resize(function(){
                sharePositioning();
            });


            $(window).scroll(function () {
                if ( $(window).scrollTop() + $('.s-header').height() > $('.s-header').height() ) {
                    $('.share-container').addClass('shareFixed');
                    $('.share-container').removeClass('shareAbsolute');
                } else {
                    $('.share-container').removeClass('shareFixed');
                    $('.share-container').addClass('shareAbsolute');
                }
            });


        });
    </script>
    <section class="s-main-content">



        <!-- Adv Middle Top -->
        <!--==================================================-->
        <div class="adv adv-1100-90">
            ADVERTISE 1100 x 90
        </div>

        <!-- Middle Content -->
        <!--==================================================-->
        <div class="s-col-2-per-3 pull-left" style="/*background: #123;*/">
            <h1 style="font-size: 24px; font-weight: 700;">Malino Highlands, Wisata Termegah dan Landmark Dunia</h1>
            <span class="clearfix"></span>
            <div class="pull-left" style="background: #ed1b23; padding: 7px 15px; color: #fff;">Gadget</div>

            <div class="pull-left" style="padding-left: 15px; padding-right: 15px;">
                <div class="news-module-hor-icon pull-left">
                    <img src="../img/icon/profile.png">
                </div>
                <div class="news-module-hor-time pull-left">
                    Budi
                </div>
            </div>

            <div class="pull-left" style="padding-left: 15px; padding-right: 15px;">
                <div class="news-module-hor-icon pull-left">
                    <img src="../img/icon/clock.png">
                </div>
                <div class="news-module-hor-time pull-left">
                    9 menit yang lalu
                </div>
            </div>

            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-372442.jpg" width="100%" alt="" style="margin-top: 15px; margin-bottom: 15px;">

            <div>
                <div class="pull-right" style="width: 229px; height: auto; display: inline-block; background: #f5f5f5; padding: 20px 30px;">
                    <h3 style="margin-top: 0px;">Baca juga:</h3>
                    <ul class="s-baca-juga">
                        <li>
                            <a href="#">
                                XL Center Makassar Raih
                                Best Performers 2015
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Tujuh Smartphone Yang
                                Akan Lahir di 2016
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Transaksi Menggunakan TCASH
                                Meningkat 150%  Selama 2015
                            </a>
                        </li>
                    </ul>
                </div>
                <p>
                    MAKASSARTERKINI,- Wisata Malino adalah salah satu potensi alam Sulawesi Selatan yang begitu menakjubkan dan mempesona. Berbagai pemandangan menarik yang dapat dinikmati di bumi Malino. Mulai dari potensi alam nan hijau, kokohnya air terjun yang memberikan percikan kesejukan, buah-buahan yang segar dan masih banyak lagi.
                </p>
                <p>
                    Salah satu Kawasan wisata termegah dan terbesar di daerah yang dikenal sanagat dingin ini, yakni Malino Highlands. Berada di kawasan kebun teh di Patappang, Kecamatan Tinggi Moncong, Malino Kabupaten Gowa, Sulawesi Selatan  Malino Highlands hadir sebagai sebuah kawasan wisata termegah dan digadang menjadi Landmark Dunia. Kehadirannya memberikan kesan akan pesona alam hijau nan menawan bagi para pengunjungnya
                </p>
                <h3>Jarak Tempuh</h3>
                <p>
                    Untuk menuju lokasi wisata ini sangat mudah, karena lokasinya yang begitu strategis sehingga dapat dijangkau oleh sejumlah jenis kendaraan, baik itu roda dua maupun roda empat. Untuk tiba di lokasi ini Anda akan menempuhnya sekitar 2,5 jam perjalanan, dengan jarak tempuh sekitar 80 kilometer dari kota Makassar.
                </p>
                <h3>Biaya Masuk</h3>
                <p>
                    Untuk menikmati panorama serba hijau di Malino Highlands, pengunjung harus membayar Rp 50 ribu per orang sebagai tarif masuk kawasan kebun teh. Kebun teh Malino ini memang disulap menjadi kawasan tempat wisata untuk kalangan menengah ke atas. Lihat saja tarif kamar termurah yang ditawarkan Rp 1,75 juta per malam, kamar president suite Rp 7,5 juta per malam, dan vila eksklusif Rp 10 juta per malam. Seluruh fasilitas yang akan Anda gunakan bergaya arsitektur Jepang. Para pelayan yang akan melayani Anda juga semuanya berkostum ala Negeri Sakura.
                </p>
                <h3>Keistimewaan Malino Highlands</h3>
                <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-372747.jpg" width="100%" alt="" style="margin-top: 14px;">
                <p>
                    Berada di lokasi dataran tinggi Sulsel, Malino Highlands dapat memuaskan para pengunjungnya baik itu wisatawan lokal maupun manca negara. Segala fasilitas yang ada dirancang khusus secara terinci dan menawan, menambah kesan yang tak akan terlupakan. Di atas tanah seluas 900 hektar, sejauh mata memandang terbentang lahan hijau taman asri, yang merupakan bagian terindah dari kawasan Malino highlands.

                    Sementara di perbukitan yang luas, terhampar perkebunan teh hijau yang subur dan terawat. Diselingi gemercik suara air terjun semakin menambah kesan nyaman dengan udara yang sejuk dan segar.

                    Lihat juga : Indahnya Air Terjun Bissapu, Bantaeng

                    Lokasi wisata alam ini juga menawarkan beberapa permainan aktifitas di luar ruangan (outdoor activities) antara lain paralayang, menunggang kuda, bungee trampolines, cross country running, bersepeda, kegiatan festival layang-layang dan masih banyak lagi.

                    Selain itu, di area kebun teh tersebut juga rencanaya akan dibangun berbagai fasilitas yang dapat dinikmati keluarga seperti kebun binatang mini, dengan berbagai koleksi binatang diantaranya burung kasuari dan kuskus.
                </p>
            </div>

            <div style="background: #e6f5fa; padding: 20px 20px; margin-bottom: 10px;">
                Editor    : Andi Andi Andi, SE, M.Si
                <span class="clearfix"></span>
                Publisher : Budi Andi Budi SE, M.Si
            </div>
            <div class="adv adv-790-100">
                ADVERTISE 790 x 100
            </div>
                <iframe id="fe6ea43817dec8" name="f1f490dae234154" scrolling="no" title="Facebook Social Plugin" class="fb_ltr" src="https://www.facebook.com/plugins/comments.php?api_key=113869198637480&amp;channel_url=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D42%23cb%3Df154d65c633ed2c%26domain%3Ddevelopers.facebook.com%26origin%3Dhttps%253A%252F%252Fdevelopers.facebook.com%252Ff3a4d796e7cccfc%26relation%3Dparent.parent&amp;href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2Fcomments%23configurator&amp;locale=en_US&amp;numposts=5&amp;sdk=joey&amp;version=v2.6&amp;width=550" style="border: none; overflow: hidden; height: 1500px; width: 100%;"></iframe>

            <div style="background: #fad4bb; padding: 15px 30px; overflow: hidden;">
                <div class="pull-left">
                    <img src="../img/icon/news-ltr.png">
                </div>
                <div class="pull-left" style="margin-left: 30px;">
                    <h2 style="margin: 0px; color: #ed1c24; font-weight: 700;">Subscribe !!!</h2>
                    <p>
                        Jangan sampai ketinggalan berita terkini, <br/>
                        langganan newsletter kami sekarang!
                    </p>
                </div>
                <div style="margin-top: 22px;">
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Masukkan email anda" aria-describedby="basic-addon2">
                      <a href="#" class="input-group-addon" id="basic-addon2" style="background: #ff6600; color: #fff;">Kirim</a>
                  </div>
              </div>
            </div>

            <?php include('node_berita_terkait.php'); ?>
        </div>


        <!-- Middle Content -->
        <!--==================================================-->
        <div class="s-col-1-per-3 pull-right" style="">
            <div class="adv adv-300-250">
                ADVERTISE 300 x 250
            </div>
            <?php include('node_berita_pop.php'); ?>
            <div class="adv adv-300-250">
                ADVERTISE 300 x 250
            </div>
            <?php include('node_event_terkini.php'); ?>
            <?php for($i=0; $i < 10; $i++){ ?>
            <div class="adv adv-300-250">
                ADVERTISE 300 x 250
            </div>
            <?php } ?>
        </div>



    </section>
        <span class="clearfix"></span>
        <div class="adv adv-1100-90">
            ADVERTISE 1100 x 90
        </div>





    <!--Footer-->
    <!--==================================================-->
    <?php include('_footer.php'); ?>





</body>
</html>
