<!DOCTYPE html>
<html>
<head>
    <title>Detail Foto News</title>
    <?php include('_css.php'); ?>

    <style type="text/css">
    .shareFixed {
        position: fixed; /*top: 130px;*/ width: 80px;  border-top-left-radius: 5px; border-bottom-left-radius; border: 1px solid #ed1c24; padding-bottom: 10px;
    }
    .shareAbsolute {
        position: absolute; /*top: 220px;*/ transform: translateY(0px); width: 80px;  border-top-left-radius: 5px; border-bottom-left-radius; border: 1px solid #ed1c24; padding-bottom: 10px;
    }
    </style>
</head>
<body class="body-offcanvas">
    <!--==================================================-->
    <!-- Header -->
    <!--==================================================-->
    <?php include('_header.php'); ?>

    <!--Content-->
    <!--==================================================-->
    <div class="share-container shareAbsolute" style="">
        <div class="s-share-counter text-center" style="background: #ed1c24; line-height: 30px;">
            <span style="color: #fff; line-height: 30px; font-size: 16px;">143</span>
            <span style="color: #fff; line-height: 30px; font-size: 10px;">Share</span>
        </div>
        <a href="#" style="background: url('../img/icon/side-fb.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
        <a href="#" style="background: url('../img/icon/side-twitter.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
        <a href="#" style="background: url('../img/icon/side-gp.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
        <a href="#" style="background: url('../img/icon/side-path.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
        <a href="#" style="background: url('../img/icon/side-instagram.png') no-repeat; position: relative; width: 50px; height: 66px; display: block; margin-left: auto; margin-right: auto; margin-top: 15px;">
            <span class="text-center" style="width: 100%; position: absolute; bottom: 2px; left: 0px; color: #fff; font-size: 12px;">100</span>
        </a>
    </div>
    <script type="text/javascript">

        function sharePositioning(){
            var marginLeft = ($(window).width() - 1100)/2;
            var leftFixed = (marginLeft - 80 - 15);
            $('.share-container').css('left', leftFixed);
        }

        sharePositioning();

        $(document).ready(function() {

            $(window).resize(function(){
                sharePositioning();
            });


            $(window).scroll(function () {
                if ( $(window).scrollTop() + $('.s-header').height() > $('.s-header').height() ) {
                    $('.share-container').addClass('shareFixed');
                    $('.share-container').removeClass('shareAbsolute');
                } else {
                    $('.share-container').removeClass('shareFixed');
                    $('.share-container').addClass('shareAbsolute');
                }
            });


        });
    </script>
    <section class="s-main-content">



        <!-- Adv Middle Top -->
        <!--==================================================-->
        <div class="adv adv-1100-90">
            ADVERTISE 1100 x 90
        </div>

        <!-- Middle Content -->
        <!--==================================================-->
        <div class="s-col-2-per-3 pull-left" style="/*background: #123;*/">


            <h1 style="font-size: 24px; font-weight: 700;">Begini Kondisi Gedung di Taiwan Usai Digoyang Gempa 6,4 SR</h1>
            <span class="clearfix"></span>
            <div class="pull-left" style="background: #ed1b23; padding: 7px 15px; color: #fff;">Gadget</div>

            <div class="pull-left" style="padding-left: 15px; padding-right: 15px;">
                <div class="news-module-hor-icon pull-left">
                    <img src="../img/icon/profile.png">
                </div>
                <div class="news-module-hor-time pull-left">
                    Budi
                </div>
            </div>

            <div class="pull-left" style="padding-left: 15px; padding-right: 15px;">
                <div class="news-module-hor-icon pull-left">
                    <img src="../img/icon/clock.png">
                </div>
                <div class="news-module-hor-time pull-left">
                    9 menit yang lalu
                </div>
            </div>
            <span class="clearfix"></span>
            <div class="popup-gallery" style="margin-top: 15px; margin-bottom: 15px;">
                <div class="foto-news-hover-cont" style="">
                    <div class="f-hidden-foto-opt" style="top: 0px; right: 0px; position: absolute; padding: 10px;">
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-fb.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-twitter.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-gp.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-path.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-instagram.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-share.png">
                        </span>
                    </div>
                    <div class="foto-news-hover-img f-hidden-foto-opt" style="">
                        <img src="../img/icon/foto_news_hover.png" style="margin-top: 38px;">
                    </div>
                    <a href="http://farm9.staticflickr.com/8242/8558295633_f34a55c1c6_b.jpg" title="Warga memutarkan kaleng berisi kayu yang dibakar jelang perayaan Jeongwol Daeboreum di sebuah taman di Seoul, Korea Selatan, Minggu (21/2). Tarian api ini merupakan bagian dari perayaan Imlek ala Korea. (REUTERS/Kim Hong-Ji)">
                        <img src="http://farm9.staticflickr.com/8242/8558295633_f34a55c1c6_b.jpg" width="100%">
                    </a>
                </div>
                <p style="margin-bottom: 30px;">
                    Warga memutarkan kaleng berisi kayu yang dibakar jelang perayaan "Jeongwol Daeboreum"di sebuah taman di Seoul, Korea Selatan, Minggu (21/2). Tarian api ini merupakan bagian dari perayaan Imlek ala Korea. (REUTERS/Kim Hong-Ji)
                </p>



                <div class="foto-news-hover-cont" style="">
                    <div class="f-hidden-foto-opt" style="top: 0px; right: 0px; position: absolute; padding: 10px;">
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-fb.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-twitter.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-gp.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-path.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-instagram.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-share.png">
                        </span>
                    </div>
                    <div class="foto-news-hover-img f-hidden-foto-opt" style="">
                        <img src="../img/icon/foto_news_hover.png" style="margin-top: 38px;">
                    </div>
                    <a href="http://farm9.staticflickr.com/8382/8558295631_0f56c1284f_b.jpg" title="Penari beratraksi di depan bakaran api saat jelang perayaan Jeongwol Daeboreum di Seoul, Korea Selatan, Minggu (21/2). Tradisi perayaan Jeongwol Daeboreum ini merupakan bagian dari peringatan hari besar dalam penanggalan Imlek. (REUTERS/Kim Hong-Ji)">
                        <img src="http://farm9.staticflickr.com/8382/8558295631_0f56c1284f_b.jpg" width="100%">
                    </a>
                </div>
                <p style="margin-bottom: 30px;">
                    Penari beratraksi di depan bakaran api saat jelang perayaan "Jeongwol Daeboreum" di Seoul, Korea Selatan, Minggu (21/2). Tradisi perayaan "Jeongwol Daeboreum" ini merupakan bagian dari peringatan hari besar dalam penanggalan Imlek. (REUTERS/Kim Hong-Ji)
                </p>
                <div class="adv adv-790-100">
                    ADVERTISE 790 x 100
                </div>



                <div class="foto-news-hover-cont" style="">
                    <div class="f-hidden-foto-opt" style="top: 0px; right: 0px; position: absolute; padding: 10px;">
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-fb.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-twitter.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-gp.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-path.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-instagram.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-share.png">
                        </span>
                    </div>
                    <div class="foto-news-hover-img f-hidden-foto-opt" style="">
                        <img src="../img/icon/foto_news_hover.png" style="margin-top: 38px;">
                    </div>
                    <a href="http://farm9.staticflickr.com/8225/8558295635_b1c5ce2794_b.jpg" title="Sejumlah anak memutarkan kaleng berisi kayu yang dibakar jelang perayaan Jeongwol Daeboreum di sebuah taman di Seoul, Korea Selatan, Minggu (21/2). Tarian api ini merupakan bagian dari perayaan Imlek ala Korea. (REUTERS/Kim Hong-Ji)">
                        <img src="http://farm9.staticflickr.com/8225/8558295635_b1c5ce2794_b.jpg" width="100%">
                    </a>
                </div>
                <p style="margin-bottom: 30px;">
                    Sejumlah anak memutarkan kaleng berisi kayu yang dibakar jelang perayaan "Jeongwol Daeboreum"di sebuah taman di Seoul, Korea Selatan, Minggu (21/2). Tarian api ini merupakan bagian dari perayaan Imlek ala Korea. (REUTERS/Kim Hong-Ji)
                </p>


                <div class="foto-news-hover-cont" style="">
                    <div class="f-hidden-foto-opt" style="top: 0px; right: 0px; position: absolute; padding: 10px;">
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-fb.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-twitter.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-gp.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-path.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-instagram.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-share.png">
                        </span>
                    </div>
                    <div class="foto-news-hover-img f-hidden-foto-opt" style="">
                        <img src="../img/icon/foto_news_hover.png" style="margin-top: 38px;">
                    </div>
                    <a href="http://farm9.staticflickr.com/8383/8563475581_df05e9906d_b.jpg" title="Warga memutarkan kaleng berisi kayu yang dibakar jelang perayaan Jeongwol Daeboreum di sebuah taman di Seoul, Korea Selatan, Minggu (21/2). Tarian api ini merupakan bagian dari perayaan Imlek ala Korea. (REUTERS/Kim Hong-Ji)">
                        <img src="http://farm9.staticflickr.com/8383/8563475581_df05e9906d_b.jpg" width="100%">
                    </a>
                </div>
                <p style="margin-bottom: 30px;">
                    Warga memutarkan kaleng berisi kayu yang dibakar jelang perayaan "Jeongwol Daeboreum"di sebuah taman di Seoul, Korea Selatan, Minggu (21/2). Tarian api ini merupakan bagian dari perayaan Imlek ala Korea. (REUTERS/Kim Hong-Ji)
                </p>
                <div class="adv adv-790-100">
                    ADVERTISE 790 x 100
                </div>



                <div class="foto-news-hover-cont" style="">
                    <div class="f-hidden-foto-opt" style="top: 0px; right: 0px; position: absolute; padding: 10px;">
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-fb.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-twitter.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-gp.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-path.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-instagram.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-share.png">
                        </span>
                    </div>
                    <div class="foto-news-hover-img f-hidden-foto-opt" style="">
                        <img src="../img/icon/foto_news_hover.png" style="margin-top: 38px;">
                    </div>
                    <a href="http://farm9.staticflickr.com/8235/8559402846_8b7f82e05d_b.jpg" title="Warga memutarkan kaleng berisi kayu yang dibakar jelang perayaan Jeongwol Daeboreum di sebuah taman di Seoul, Korea Selatan, Minggu (21/2). Tarian api ini merupakan bagian dari perayaan Imlek ala Korea. (REUTERS/Kim Hong-Ji)">
                        <img src="http://farm9.staticflickr.com/8235/8559402846_8b7f82e05d_b.jpg" width="100%">
                    </a>
                </div>
                <p style="margin-bottom: 30px;">
                    Warga memutarkan kaleng berisi kayu yang dibakar jelang perayaan "Jeongwol Daeboreum"di sebuah taman di Seoul, Korea Selatan, Minggu (21/2). Tarian api ini merupakan bagian dari perayaan Imlek ala Korea. (REUTERS/Kim Hong-Ji)
                </p>


                <div class="foto-news-hover-cont" style="">
                    <div class="f-hidden-foto-opt" style="top: 0px; right: 0px; position: absolute; padding: 10px;">
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-fb.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-twitter.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-gp.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-path.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-instagram.png">
                        </span>
                        <span style="margin-left: 5px; margin-right: 5px;">
                            <img src="../img/icon/foto-share.png">
                        </span>
                    </div>
                    <div class="foto-news-hover-img f-hidden-foto-opt" style="">
                        <img src="../img/icon/foto_news_hover.png" style="margin-top: 38px;">
                    </div>
                    <a href="http://farm9.staticflickr.com/8235/8558295467_e89e95e05a_b.jpg" title="Warga memutarkan kaleng berisi kayu yang dibakar jelang perayaan Jeongwol Daeboreum di sebuah taman di Seoul, Korea Selatan, Minggu (21/2). Tarian api ini merupakan bagian dari perayaan Imlek ala Korea. (REUTERS/Kim Hong-Ji)">
                        <img src="http://farm9.staticflickr.com/8235/8558295467_e89e95e05a_b.jpg" width="100%">
                    </a>
                </div>
                <p style="margin-bottom: 30px;">
                    Warga memutarkan kaleng berisi kayu yang dibakar jelang perayaan "Jeongwol Daeboreum"di sebuah taman di Seoul, Korea Selatan, Minggu (21/2). Tarian api ini merupakan bagian dari perayaan Imlek ala Korea. (REUTERS/Kim Hong-Ji)
                </p>
            </div>


            <div class="adv adv-790-100">
                ADVERTISE 790 x 100
            </div>
            <iframe id="fe6ea43817dec8" name="f1f490dae234154" scrolling="no" title="Facebook Social Plugin" class="fb_ltr" src="https://www.facebook.com/plugins/comments.php?api_key=113869198637480&amp;channel_url=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter.php%3Fversion%3D42%23cb%3Df154d65c633ed2c%26domain%3Ddevelopers.facebook.com%26origin%3Dhttps%253A%252F%252Fdevelopers.facebook.com%252Ff3a4d796e7cccfc%26relation%3Dparent.parent&amp;href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2Fcomments%23configurator&amp;locale=en_US&amp;numposts=5&amp;sdk=joey&amp;version=v2.6&amp;width=550" style="border: none; overflow: hidden; height: 1500px; width: 100%;"></iframe>
            <div style="background: #fad4bb; padding: 15px 30px; overflow: hidden;">
                <div class="pull-left">
                    <img src="../img/icon/news-ltr.png">
                </div>
                <div class="pull-left" style="margin-left: 30px;">
                    <h2 style="margin: 0px; color: #ed1c24; font-weight: 700;">Subscribe !!!</h2>
                    <p>
                        Jangan sampai ketinggalan berita terkini, <br/>
                        langganan newsletter kami sekarang!
                    </p>
                </div>
                <div style="margin-top: 22px;">
                    <div class="input-group">
                      <input type="text" class="form-control" placeholder="Masukkan email anda" aria-describedby="basic-addon2">
                      <a href="#" class="input-group-addon" id="basic-addon2" style="background: #ff6600; color: #fff;">Kirim</a>
                  </div>
              </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('.popup-gallery').magnificPopup({
                        delegate: 'a',
                        type: 'image',
                        tLoading: 'Loading image #%curr%...',
                        mainClass: 'mfp-img-mobile',
                        gallery: {
                            enabled: true,
                            navigateByImgClick: true,
                            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                        },
                        image: {
                            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                            titleSrc: function(item) {
                                return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
                            }
                        }
                    });
                });
            </script>
            <?php include('node_berita_terkait.php'); ?>
        </div>


        <!-- Middle Content -->
        <!--==================================================-->
        <div class="s-col-1-per-3 pull-right" style="">
            <div class="adv adv-300-250">
                ADVERTISE 300 x 250
            </div>
            <?php include('node_berita_pop.php'); ?>
            <div class="adv adv-300-250">
                ADVERTISE 300 x 250
            </div>
            <?php include('node_event_terkini.php'); ?>
            <?php for($i=0; $i < 10; $i++){ ?>
            <div class="adv adv-300-250">
                ADVERTISE 300 x 250
            </div>
            <?php } ?>
        </div>



    </section>
        <span class="clearfix"></span>
        <div class="adv adv-1100-90">
            ADVERTISE 1100 x 90
        </div>





    <!--Footer-->
    <!--==================================================-->
    <?php include('_footer.php'); ?>





</body>
</html>
