
<header class="s-header" style="position: fixed; z-index: 9; width: 100%; top: 0px;">

        <div class="adv adv-top">
            <div class="adv-closer f-close-top-adv">
                <span>close/tutup</span>
                <span class="the-x">X</span>
            </div>
            ADVERTISE 1100 x 90
        </div>
    <!-- Begin Navbar -->
    <div class="s-header-c0"><!-- 35px -->
        <div class="s-container text-right">
            <a href="#">
                <span class="s-icon-fb-white"></span>
            </a>
            <a href="#">
                <span class="s-icon-twitter-white"></span>
            </a>
            <a href="#">
                <span class="s-icon-gplus-white"></span>
            </a>
            <a href="#">
                <span class="s-icon-instagram-white"></span>
            </a>
            <span class="s-icon-sprt-white"></span>
            <a href="#">
                <span class="s-icon-pp-white"></span>&nbsp;Login/Daftar
            </a>
        </div>
    </div>
    <div class="s-header-sprt--white"><!-- 3px -->
        <div class="s-container">
        <div class="s-beauty-line" style="">
            </div>
        </div>
    </div>
    <div class="s-header-c1"><!-- 81px -->
        <div class="s-container">
            <div class="s-tbl pull-left" style="width: 269px;">
                <div class="s-cell">
                    <img src="../img/icon/logo.png" width="231">
                </div>
            </div>
            <ul class="s-ul-header-nav f-header-nav">
                <li class="flex-item"><a class="active" href="#"><img src="../img/icon/home.png"></a></li>
                <li class="flex-item"><a data-id="news" href="#">news</a></li>
                <li class="flex-item"><a data-id="bisnis" href="#">bisnis</a></li>
                <li class="flex-item"><a data-id="tekno" href="#">tekno</a></li>
                <li class="flex-item"><a data-id="lifestyle" href="#">life style</a></li>
                <li class="flex-item"><a data-id="sport" href="#">sport</a></li>
                <li class="flex-item"><a data-id="gallery"  href="#">gallery</a></li>
                <li class="flex-item"><a id="indeks" class="ignore" href="#">indeks</a></li>
                <li class="flex-item"><a id="citizen-report" class="ignore" href="#">citizen report</a></li>
                <li class="flex-item">
                <a id="search-head" class="ignore" href="#"><img src="../img/icon/header-search.png" width="30px"></a>
                </li>
            </ul>

            <div id="search-cont" class="s-search-cont" style="display: none;">
                <input class="form-control pull-left" value="Cari berita..." style="">
                <button class="btn btn-success pull-left" style="">
                        Search
                </button>
            </div>
            <div id="news" class="s-mega-menu f-mega-menu" style="display: none;">
                <div class="s-menu">
                    <ul>
                        <li><a href="">PERISTIWA</a></li>
                        <li><a href="">POLITIK</a></li>
                        <li><a href="">INTERNASIONAL</a></li>
                        <li><a href="">TODAY IN HISTORY</a></li>
                        <li><a href="">LIPUTAN KHUSUS</a></li>
                        <li><a href="">INFOGRAFIS</a></li>
                        <li><a href="">WAWANCARA KHUSUS</a></li>
                    </ul>
                </div>
                <div class="s-news-cnt">
                    <div class="s-headline s-news-node">
                        <div class="img-container">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-373654.jpg" width="100%" alt="">
                        </div>
                        <span class="s-time">
                            <span class="s-clock-dark-stroke pull-left" style="margin-top: 5px; margin-right: 5px;"></span>
                            <span class="pull-left" style="margin-top: 2px;">13 Februari 2016 18:10</span>
                        </span>
                        <h3>
                            Sulitnya Mendidik Anak Difabel
                        </h3>
                    </div>
                    <?php for($i=0; $i < 4; $i++){ ?>
                    <div class="s-news-node">
                        <div class="img-container">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-373654.jpg" width="100%" alt="">
                        </div>
                        <h3>
                            Sulitnya Mendidik Anak Difabel
                        </h3>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div id="bisnis" class="s-mega-menu f-mega-menu" style="display: none;">
                <div class="s-menu">
                    <ul>
                        <li><a href="">EKONOMI</a></li>
                        <li><a href="">BANK</a></li>
                        <li><a href="">PROPERTY</a></li>
                        <li><a href="">CPMS</a></li>
                        <li><a href="">OTOMOTIF</a></li>
                        <li><a href="">HOTEL</a></li>
                    </ul>
                </div>
                <div class="s-news-cnt">
                    <div class="s-headline s-news-node">
                        <div class="img-container">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-373654.jpg" width="100%" alt="">
                        </div>
                        <span class="s-time">
                            <span class="s-clock-dark-stroke pull-left" style="margin-top: 5px; margin-right: 5px;"></span>
                            <span class="pull-left" style="margin-top: 2px;">13 Februari 2016 18:10</span>
                        </span>
                        <h3>
                            Sulitnya Mendidik Anak Difabel
                        </h3>
                    </div>
                    <?php for($i=0; $i < 4; $i++){ ?>
                    <div class="s-news-node">
                        <div class="img-container">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-373654.jpg" width="100%" alt="">
                        </div>
                        <h3>
                            Sulitnya Mendidik Anak Difabel
                        </h3>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div id="tekno" class="s-mega-menu f-mega-menu" style="display: none;">
                <div class="s-menu">
                    <ul>
                        <li><a href="">GADGET</a></li>
                        <li><a href="">TIPS</a></li>
                        <li><a href="">GAME</a></li>
                        <li><a href="">INTERNET</a></li>
                        <li><a href="">TECH NEWS</a></li>
                        <li><a href="">TEKNO</a></li>
                        <li><a href="">ANDROID</a></li>
                        <li><a href="">START UP</a></li>
                    </ul>
                </div>
                <div class="s-news-cnt">
                    <div class="s-headline s-news-node">
                        <div class="img-container">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-373654.jpg" width="100%" alt="">
                        </div>
                        <span class="s-time">
                            <span class="s-clock-dark-stroke pull-left" style="margin-top: 5px; margin-right: 5px;"></span>
                            <span class="pull-left" style="margin-top: 2px;">13 Februari 2016 18:10</span>
                        </span>
                        <h3>
                            Sulitnya Mendidik Anak Difabel
                        </h3>
                    </div>
                    <?php for($i=0; $i < 4; $i++){ ?>
                    <div class="s-news-node">
                        <div class="img-container">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-373654.jpg" width="100%" alt="">
                        </div>
                        <h3>
                            Sulitnya Mendidik Anak Difabel
                        </h3>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div id="lifestyle" class="s-mega-menu f-mega-menu" style="display: none;">
                <div class="s-menu">
                    <ul>
                        <li><a href="">FASHION & BEAUTY</a></li>
                        <li><a href="">KARIR</a></li>
                        <li><a href="">LIFE STYLE NEWS</a></li>
                        <li><a href="">SHOPPING</a></li>
                        <li><a href="">KULINER</a></li>
                        <li><a href="">TRVEL</a></li>
                        <li><a href="">HOME &amp; DECORATION</a></li>
                        <li><a href="">BUDAYA</a></li>
                    </ul>
                </div>
                <div class="s-news-cnt">
                    <div class="s-headline s-news-node">
                        <div class="img-container">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-373654.jpg" width="100%" alt="">
                        </div>
                        <span class="s-time">
                            <span class="s-clock-dark-stroke pull-left" style="margin-top: 5px; margin-right: 5px;"></span>
                            <span class="pull-left" style="margin-top: 2px;">13 Februari 2016 18:10</span>
                        </span>
                        <h3>
                            Sulitnya Mendidik Anak Difabel
                        </h3>
                    </div>
                    <?php for($i=0; $i < 4; $i++){ ?>
                    <div class="s-news-node">
                        <div class="img-container">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-373654.jpg" width="100%" alt="">
                        </div>
                        <h3>
                            Sulitnya Mendidik Anak Difabel
                        </h3>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div id="sport" class="s-mega-menu f-mega-menu" style="display: none;">
                <div class="s-menu">
                    <ul>
                        <li><a href="">PSM</a></li>
                        <li><a href="">LIGA INDONESIA</a></li>
                        <li><a href="">LIGA INTERNASIONAL</a></li>
                        <li><a href="">SPORT</a></li>
                    </ul>
                </div>
                <div class="s-news-cnt">
                    <div class="s-headline s-news-node">
                        <div class="img-container">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-373654.jpg" width="100%" alt="">
                        </div>
                        <span class="s-time">
                            <span class="s-clock-dark-stroke pull-left" style="margin-top: 5px; margin-right: 5px;"></span>
                            <span class="pull-left" style="margin-top: 2px;">13 Februari 2016 18:10</span>
                        </span>
                        <h3>
                            Sulitnya Mendidik Anak Difabel
                        </h3>
                    </div>
                    <?php for($i=0; $i < 4; $i++){ ?>
                    <div class="s-news-node">
                        <div class="img-container">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-373654.jpg" width="100%" alt="">
                        </div>
                        <h3>
                            Sulitnya Mendidik Anak Difabel
                        </h3>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div id="gallery" class="s-mega-menu f-mega-menu" style="display: none;">
                <div class="s-menu">
                    <ul>
                        <li><a href="">PHOTO</a></li>
                        <li><a href="">VIDEO</a></li>
                    </ul>
                </div>
                <div class="s-news-cnt">
                    <div class="s-headline s-news-node">
                        <div class="img-container">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-373654.jpg" width="100%" alt="">
                        </div>
                        <span class="s-time">
                            <span class="s-clock-dark-stroke pull-left" style="margin-top: 5px; margin-right: 5px;"></span>
                            <span class="pull-left" style="margin-top: 2px;">13 Februari 2016 18:10</span>
                        </span>
                        <h3>
                            Sulitnya Mendidik Anak Difabel
                        </h3>
                    </div>
                    <?php for($i=0; $i < 4; $i++){ ?>
                    <div class="s-news-node">
                        <div class="img-container">
                            <img src="https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-373654.jpg" width="100%" alt="">
                        </div>
                        <h3>
                            Sulitnya Mendidik Anak Difabel
                        </h3>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</header>
