<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <link rel="stylesheet" href="../css/bootstrap.css">
        <link rel="stylesheet" href="../css/bootstrap.offcanvas.css">
        <script src="../js/amcharts/amcharts.js"></script>
        <script src="../js/amcharts/pie.js"></script>
        <script src="../js/amcharts/themes/black.js"></script>

        <?php include('_css.php'); ?>
        
    </head>
    <body class="body-offcanvas">
        <!-- Header -->
        <!--==================================================-->
        <header>
            <!-- Begin Navbar -->
            <div class="row purpleHeader">
                
            </div>
            <div class="row backgroundHeader">
                
            </div>

        </header>
        <!--Content-->
        <!--==================================================-->
        <section id="content">
            <div class="row backgroundContent">
                <div class="container">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">
                                <div class="tk-module">
                                    <div class="tk-module-header">

                                        <div class="tk-module-header-stripes">
                                        </div>
                                        <div class="tk-module-header-title">
                                            <p>Tentang Kami</p>

                                            <div class="tk-module-header-title-tooltip">
                                                <img src="../img/icon/red-tri.png">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tk-module-content">
                                        <p>
                                            Diterbitkan pada tahun 2003, Makassar Terkini merupakan majalah bulanan komunitas dalam bahasa Indonesia untuk wilayah Makassar. Tujuan penerbitan majalah ini adalah sebagai media komunikasi dan informasi antar warga di Makassar.

                                            </br></br>Majalah Makassar Terkini adalah satu-satunya majalah komunitas yang menjangkau komunitas kaum terpelajar dan memiliki tingkat kesejahteraan hidup yang baik.

                                            </br></br>Dengan berfokus pada kualitas, cakupan berita Makassar Terkini meliputi topik-topik seputar info metro, info bisnis & dagang, info wisata & hiburan, info keluarga dan info teknologi.

                                            </br></br>Makassar Terkini telah menjadi peluang penting bagi para pengiklan yang ingin menjangkau komunitas Makasar    
                                        </p>
                                    </div>
                                </div>

                                <div class="tk-module">
                                    <div class="tk-module-header">

                                        <div class="tk-module-header-stripes">
                                        </div>
                                        <div class="tk-module-header-title">
                                            <p>Positioning</p>

                                            <div class="tk-module-header-title-tooltip">
                                                <img src="../img/icon/red-tri.png">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tk-module-content">
                                        <p>
                                            Majalah komunitas Kawasan Makassar
                                        </p>
                                    </div>
                                </div>

                                <div class="tk-module">
                                    <div class="tk-module-header">

                                        <div class="tk-module-header-stripes">
                                        </div>
                                        <div class="tk-module-header-title">
                                            <p>Diferensiasi</p>

                                            <div class="tk-module-header-title-tooltip">
                                                <img src="../img/icon/red-tri.png">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tk-module-content">
                                        <p>
                                            1. Pelopor dan market leader majalah komunitas di kawasan Makassar
                                            </br></br>2. Dikelola secara profesional dengan fokus pada kualitas
                                            </br></br>3. Majalah dengan jumlah oplah terbesar di kawasan Makassar
                                        </p>
                                    </div>
                                </div>

                                <div class="tk-module">
                                    <div class="tk-module-header">

                                        <div class="tk-module-header-stripes">
                                        </div>
                                        <div class="tk-module-header-title">
                                            <p>Tagline</p>

                                            <div class="tk-module-header-title-tooltip">
                                                <img src="../img/icon/red-tri.png">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tk-module-content">
                                        <p>
                                            Semakin informatif, efektif dan terpercaya!
                                        </p>
                                    </div>
                                </div>

                                <div class="tk-module">
                                    <div class="tk-module-header">

                                        <div class="tk-module-header-stripes">
                                        </div>
                                        <div class="tk-module-header-title">
                                            <p>Survey / Pembaca</p>

                                            <div class="tk-module-header-title-tooltip">
                                                <img src="../img/icon/red-tri.png">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tk-module-content">
                                        <div id="chartdiv"></div>   
                                    </div>
                                </div>

                                <div class="tk-module">
                                    <div class="tk-module-header">

                                        <div class="tk-module-header-stripes">
                                        </div>
                                        <div class="tk-module-header-title">
                                            <p>Distribusi</p>

                                            <div class="tk-module-header-title-tooltip">
                                                <img src="../img/icon/red-tri.png">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tk-module-content">
                                        <p>
                                            Transportasi, Asuransi,  Elektronik & Komputer, Fashion, Finance, Food & Drink, Hotel, Jasa, Kesehatan & Kecantikan, Komunikasi, Media, Otomotif, Pendidikan, Perlengkapan Rumah Tangga, Property, Kantor Pemerintahan
                                        </p>
                                    </div>
                                </div>

                                <div class="tk-demografi">
                                    <span class="tk-demografi-head">
                                        Jenis Kelamin
                                    </span>
                                    <p class="tk-demografi-content">
                                       44% Wanita, 56% Pria
                                    </p>

                                    <span class="tk-demografi-head">
                                        Kelompok Usia
                                    </span>
                                    <p class="tk-demografi-content">
                                        15% < 25 Tahun,
                                        <br>35% 25-29 Tahun
                                        <br>33% < 30-34 Tahun,
                                        <br>17% > 40 Tahun

                                    </p>

                                    <span class="tk-demografi-head">
                                        Tingkat Ekonomi
                                    </span>
                                    <p class="tk-demografi-content">
                                        33% Kelompok A,
                                        <br>60% Kelompok B,
                                        <br>7% Kelompok C
                                    </p>

                                    <span class="tk-demografi-head">
                                        Area Distribusi
                                    </span>
                                    <p class="tk-demografi-content">
                                        97% Makassar, 1% Surabaya,
                                        </br>1% Bali, 1% Jakarta
                                    </p>

                                    <span class="tk-demografi-head">
                                        Pekerjaan
                                    </span>
                                    <p class="tk-demografi-content">
                                        34% Karyawan/Profesional
                                        </br>31% Pengusaha
                                        </br>24% Ibu Rumah Tangga
                                        </br>7% Mahasiswa/Pelajar
                                        </br>4% Lainnya
                                    </p>

                                    <span class="tk-demografi-head">
                                        Pendidikan
                                    </span>
                                    <p class="tk-demografi-content">
                                        42% Diploma, 46% Sarjana S1,
                                        </br>0,4% Master (S2), 11,6% dan lain lain
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </body>    
</html>

<script>

    $(document).ready(function () {

        var chart = AmCharts.makeChart( "chartdiv", {
          "type": "pie",
          "theme": "none",
          "dataProvider": [ {
            "country": "Lithuania",
            "litres": 501.9
          }, {
            "country": "Czech Republic",
            "litres": 301.9
          }, {
            "country": "Ireland",
            "litres": 201.1
          }, {
            "country": "Germany",
            "litres": 165.8
          }, {
            "country": "Australia",
            "litres": 139.9
          }, {
            "country": "Austria",
            "litres": 128.3
          }, {
            "country": "UK",
            "litres": 99
          }, {
            "country": "Belgium",
            "litres": 60
          }, {
            "country": "The Netherlands",
            "litres": 50
          } ],
          "valueField": "litres",
          "titleField": "country",
           "balloon":{
           "fixedPosition":true
          },
          "export": {
            "enabled": true
          }
        } );

    });

</script>