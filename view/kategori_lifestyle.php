<!DOCTYPE html>
<html>
    <head>
        <title>Kategori Lifestyle</title>
        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Advertisement Top -->
        <!--==================================================-->
        <!-- Header -->
        <!--==================================================-->
        <?php include('_header.php'); ?>


        <!--Content-->
        <!--==================================================-->
        <section class="s-main-content">

            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

            <!-- Adv Left -->
            <!--==================================================-->
            <div class="adv adv-160-600 adv-top-left">

            </div>

            <!-- Adv Right -->
            <!--==================================================-->
            <div class="adv adv-160-600 adv-top-right">

            </div>

            <!-- Middel Content -->
            <!--==================================================-->
            <div class="s-col-2-per-3 pull-left f-content-area" style="/*background: #123;*/">

                <div class="s-fancy-header-red">
                <span class="s-title">LIFESTYLE</span>
                    <ul class="list-unstyled s-sub-menu">
                        <li>
                            <a class="active" href="#">
                                Fashion &amp; Beauty
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Karir
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Lifestyle News
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Shopping
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Kuliner
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Travel
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Home Decoration
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Budaya
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="adv adv-790-100">
                    ADVERTISE 790 x 100
                </div>
                <?php include('node_berita_lifestyle.php'); ?>
                <div class="adv adv-790-100">
                    ADVERTISE 790 x 100
                </div>
                <?php include('node_berita_terkini.php'); ?>
            </div>

            <!-- Middel Content -->
            <!--==================================================-->
            <div class="s-col-1-per-3 pull-right f-extra-area" style="">
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php include('node_berita_pop.php'); ?>
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php include('node_event_terkini.php'); ?>
                <?php for($i=0; $i < 1; $i++){ ?>
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php } ?>
             </div>
             <span class="clearfix"></span>
           <!--  <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div> -->



        </section>




        <!--Footer-->
        <!--==================================================-->
        <?php include('_footer.php'); ?>





    </body>
</html>
