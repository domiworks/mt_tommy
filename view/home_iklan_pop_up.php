<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Advertisement Top -->
        <!--==================================================-->
        <!-- Header -->
        <!--==================================================-->
        <?php include('_header.php'); ?>

        <!--Content-->
        <!--==================================================-->
        <div class="adv adv-160-600 adv-top-left" style="top: 131px;">

        </div>

        <!-- Adv Right -->
        <!--==================================================-->
        <div class="adv adv-160-600 adv-top-right" style="top: 131px;">

        </div>
        <script type="text/javascript">

            function advFixer(){
                var marginLeft = ($(window).width() - 1100)/2;
                var fixer = (marginLeft - 160 - 25);
                $('.adv-top-left').css('left', fixer);
                $('.adv-top-right').css('right', fixer);
            }

            advFixer();

            $(document).ready(function() {

                $(window).resize(function(){
                    advFixer();
                });

            });
        </script>
        <section class="s-main-content" style="overflow: hidden;">

            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

            <!-- Adv Left -->
            <!--==================================================-->

            <!-- Big Carousel -->
            <!--==================================================-->
            <div class="s-big-carousel">
                <div id="main-carousel" class="carousel slide" data-ride="carousel" style="height: inherit; position: relative;">

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox" style="height: inherit;">
                        <div class="item active" style="">
                            <?php for($i=0; $i < 3; $i++){ ?>
                            <a href="#" class="s-news-node" style="background: url('https://wallpapers.wallhaven.cc/wallpapers/full/wallhaven-373654.jpg') no-repeat center center; background-size: cover; ">
                                <div class="" style="width: 100%; height: 100%; overflow: hidden; float: left; position: relative;">
                                    <span class="tag-top bisnis">Internasional</span>
                                 </div>
                                <div class="s-news-info-cnt" style="">
                                <h3>
                                    Technopark Makassar Kolaborasikan Universitas, Pemerintah, dan Swasta
                                </h3>
                                <span class="s-time">
                                    <span class="s-clock-white-filled pull-left"></span>
                                    <span class="pull-left">13 Februari 2016 18:10</span>
                                </span>
                                </div>
                            </a>
                            <?php } ?>
                        </div>
                        <div class="item" style="color: #fff; height: inherit;">
                            <?php for($i=0; $i < 3; $i++){ ?>
                            <a href="#" class="s-news-node" style="background: url('https://s-media-cache-ak0.pinimg.com/736x/16/0b/c7/160bc7a33fff836a3a85950c79a614e9.jpg') no-repeat center center; background-size: cover; ">
                                <!-- <img src=""> -->
                                <div class="" style="width: 100%; height: 100%; overflow: hidden; float: left; position: relative;">
                                    <span class="tag-top tekno">Peristiwa</span>
                                </div>
                                <div class="s-news-info-cnt" style="">
                                <h3>
                                    Technopark Makassar Kolaborasikan Universitas, Pemerintah, dan Swasta
                                </h3>
                                <span class="s-time">
                                    <span class="s-clock-white-filled pull-left"></span>
                                    <span class="pull-left">13 Februari 2016 18:10</span>
                                </span>
                                </div>
                            </a>
                            <?php } ?>
                        </div>
                        <div class="item" style="color: #fff; height: inherit;">
                            <?php for($i=0; $i < 3; $i++){ ?>
                            <a href="#" class="s-news-node" style="background: url('https://lh4.ggpht.com/t0VAk0yMLwPg2jXCZ5HPMJAKH9Z-1fDcFGciAbQSqbTn3Xd_0OIeXbE7pUV02khWDw=h900') no-repeat center center; background-size: cover; ">
                                <!-- <img src=""> -->
                                <div class="" style="width: 100%; height: 100%; overflow: hidden; float: left; position: relative;">
                                    <span class="tag-top lifestyle">Peristiwa</span>
                                </div>
                                <div class="s-news-info-cnt" style="">
                                <h3>
                                    Technopark Makassar Kolaborasikan Universitas, Pemerintah, dan Swasta
                                </h3>
                                <span class="s-time">
                                    <span class="s-clock-white-filled pull-left"></span>
                                    <span class="pull-left">13 Februari 2016 18:10</span>
                                </span>
                                </div>
                            </a>
                            <?php } ?>
                        </div>
                    </div>

                    <!-- Controls -->
                    <a class="s-left-btn" href="#main-carousel" role="button" data-slide="prev" style="">
                        <img src="../img/icon/big-car-left.png">
                    </a>
                    <a class="s-right-btn" href="#main-carousel" role="button" data-slide="next">
                        <img src="../img/icon/big-car-right.png">
                    </a>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#main-carousel').carousel('pause');
                });
            </script>

            <!-- News Ticker -->
            <!--==================================================-->
            <div class="s-news-ticker">
                <span class="s-label">
                    Berita Terkini:
                </span>
                <div class="s-ticker">
                    <div id="ticker-carousel" class="carousel slide" data-ride="carousel" style="height: inherit; position: relative;">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <a href="#" class="item s-ellipsis active ">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                            </a href="#">
                            <a href="#" class="item s-ellipsis" style="color: #fff; height: inherit; line-height: 38px; text-indent: 16px;">
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.
                            </a href="#">
                            <a href="#" class="item s-ellipsis" style="color: #fff; height: inherit; line-height: 38px; text-indent: 16px;">
                                But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness.
                            </a href="#">
                        </div>

                        <!-- Controls -->
                        <a class="s-tick" href="#ticker-carousel" role="button" data-slide="prev" style="top: 7px; right: 36px;">
                            <img src="../img/icon/news-tick-left.png">
                        </a>
                        <a class="s-tick" href="#ticker-carousel" role="button" data-slide="next" style="top: 7px; right: 16px;">
                            <img src="../img/icon/news-tick-right.png">
                        </a>
                    </div>
                </div>
            </div>

            <!-- Middel Content -->
            <!--==================================================-->
            <div class="s-col-2-per-3 pull-left f-content-area" style="/*background: #123;*/">
                <div class="adv adv-790-100">
                    ADVERTISE 790 x 100
                </div>
                <?php include('node_berita_terkini.php'); ?>
                <?php include('node_berita_rekomendasi.php'); ?>
                <div class="adv adv-790-100">
                    ADVERTISE 790 x 100
                </div>
                <?php include('node_berita_kategori.php'); ?>
                <?php include('node_citizen_report.php'); ?>
                <?php include('node_video_populer.php'); ?>
                <?php include('node_foto_populer.php'); ?>
            </div>

            <!-- Middel Content -->
            <!--==================================================-->
            <div class="s-col-1-per-3 pull-right f-extra-area" style="">
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php include('node_berita_pop.php'); ?>
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>

                <div class="row backgroundContent">
                    <div class="container-fluid">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 index-contentBack">

                                    <div class="news-wrapper-terkini">
                                        <div class="news-wrapper-terkini-header">
                                            <div class="news-wrapper-terkini-left">
                                                EVENT TERKINI
                                            </div>

                                            <div class="news-wrapper-terkini-right">
                                                  LIHAT SEMUA <a href="#"><img src="../img/icon/all-window.png"></a>
                                            </div>
                                        </div>

                                        <!-- Row START -->
                                        <div class="news-module-ver-terkini">
                                            <div class="news-module-ver-img-terkini">
                                                <img src="../img/produk/kupluk.png">
                                            </div>

                                            <div class="news-module-ver-content-terkini font-sourceSansPro">
                                                <div class="news-module-ver-title-terkini">
                                                    Anji Siap Bernostalgia di Makassar
                                                </div>

                                                <div class="news-module-ver-etc-terkini">
                                                    <div class="news-module-ver-icon-terkini pull-left">
                                                        <img src="../img/icon/clock-white.png">
                                                    </div>

                                                    <div class="news-module-ver-time-terkini pull-left">
                                                        13 Februari 2016 13:15
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Row END -->
                                        <!-- Row START -->
                                        <div class="news-module-ver-terkini">
                                            <div class="news-module-ver-img-terkini">
                                                <img src="../img/produk/kupluk.png">
                                            </div>

                                            <div class="news-module-ver-content-terkini font-sourceSansPro">
                                                <div class="news-module-ver-title-terkini">
                                                    Anji Siap Bernostalgia di Makassar
                                                </div>

                                                <div class="news-module-ver-etc-terkini">
                                                    <div class="news-module-ver-icon-terkini pull-left">
                                                        <img src="../img/icon/clock-white.png">
                                                    </div>

                                                    <div class="news-module-ver-time-terkini pull-left">
                                                        13 Februari 2016 13:15
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Row END -->
                                        <!-- Row START -->
                                        <div class="news-module-ver-terkini">
                                            <div class="news-module-ver-img-terkini">
                                                <img src="../img/produk/kupluk.png">
                                            </div>

                                            <div class="news-module-ver-content-terkini font-sourceSansPro">
                                                <div class="news-module-ver-title-terkini">
                                                    Anji Siap Bernostalgia di Makassar
                                                </div>

                                                <div class="news-module-ver-etc-terkini">
                                                    <div class="news-module-ver-icon-terkini pull-left">
                                                        <img src="../img/icon/clock-white.png">
                                                    </div>

                                                    <div class="news-module-ver-time-terkini pull-left">
                                                        13 Februari 2016 13:15
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Row END -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <?php for($i=0; $i < 1; $i++){ ?>
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php } ?>
             </div>



        </section>




        <!--Footer-->
        <!--==================================================-->
        <?php include('_footer.php'); ?>

  <script type="text/javascript">
        $('#iklan_modal').modal('show');
        </script>



    </body>
</html>



