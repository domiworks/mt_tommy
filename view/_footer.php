<span class="clearfix"></span>
<footer style="position: relative; /*margin-top: 220px;*/">
    <a id="to_top" href="#" style="position: absolute; right: 10px; top: -60px;">
        <img src="../img/icon/anchor.png" width="40">
    </a>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 backgroundFooter">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="container">
                    <div class="col-md-12 col-sm-12 col-xs-12 font-openSans">
                        <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="col-md-3 col-sm-6 col-xs-12 footer-jarakText">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">News</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Peristiwa</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Politik</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Nasional</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Liputan Khusus</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Today in History</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 footer-jarakText">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Bisnis</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Ekonomi</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Perbankan</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Property</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Otomotif</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Hotel</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 footer-jarakText">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Lifestyle</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Shopping</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Kuliner</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Travel</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Budaya</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Event</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12 footer-jarakText">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">News</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Peristiwa</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Politik</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Nasional</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Liputan Khusus</a>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 footer-text">
                                            <a href="">Today in History</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="row">
                                <div class="col-md-12 footer-terhubung">
                                    <div class="row">
                                        <div class="col-md-12 footer-logo">
                                            <img src="../img/icon/logo.png">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 footer-titleBerita">
                                            Dapatkan Berita Terbaru kami secara Gratis!
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 footer-button">
                                            <div class="input-group">
                                                <input type="text" class="form-control field-newsletter font-openSans" placeholder="masukan email anda ...">
                                                <div class="input-group-btn">
                                                    <button class="btn btn-default btn-subscribed"  type="submit">KIRIM</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 footer-subBerita">
                                            example@mail.com
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row redHeader">
        <div class="container">
            <div class="col-md-12 col-sm-12 footer-copyright font-sourceSansPro">
                <div class="footer-left-section">
                    © 2016 - MAKASSAR TERKINI. All Right Reserved
                    </br>
                    Developed & Maintenance By : PT. iLugroup Multimedia Indonesia
                </div>

                <div class="footer-right-section">
                    <ul>
                        <li><a>Contact</a></li>
                        <li><a>Redaksi</a></li>
                        <li><a>Career</a></li>
                        <li><a>Sitemap</a></li>
                        <li><a>Disclaimer</a></li>
                        <li><a>Mobile Version</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="modal fade" tabindex="-1" role="dialog" id="iklan_modal" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-body adv" style="background: #ed1b23; width: 600px; height: 400px; padding: 0px;">

          <div class="adv-closer" data-dismiss="modal">
            <span>close/tutup</span>
            <span class="the-x">X</span>
        </div>
        <p style="color: #fff;">ADV 600px x 400px</p>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    /* header matter */
    var headerHeight = $('.s-header').height() + 10;

    // $(document).ready(function(){
        $('.s-main-content').css('margin-top', headerHeight);
    // });



    function dinamika_iklan(){
        console.log('dinamika_iklan');
        if($('.f-extra-area').height() < $('.f-content-area').height()){
            $(window).scroll(function () {

                var scroll_detector = $(window).scrollTop() + $(window).height();


                if($('html').find('.s-big-carousel').length != 0){
                    var batas_iklan = $('.f-extra-area').height() + 829;
                }else{

                    var batas_iklan = $('.f-extra-area').height() + 220;
                }

                if ( scroll_detector > batas_iklan ) {
                    $('.f-extra-area').addClass('main-freeze');
                }else{
                    $('.f-extra-area').removeClass('main-freeze');
                }

                console.log( $(window).scrollTop() + $(window).height() + "___"+  $('.f-extra-area').height());
                var pentokan = $(document).height() - $('footer').height();

                if ( scroll_detector > pentokan ){
                    console.log('sadsdadsa');
                    $('.f-extra-area').addClass('main-stick');
                }else{
                    $('.f-extra-area').removeClass('main-stick');
                }
            });
        };
    };

    $(document).on('ready', function(){

        /* left and right advertisement */
        $('.adv-top-left, .adv-top-right').css('top', $('.s-header').height()+10);

        $(document).on('mouseover', '.f-header-nav a:not(.ignore)', function(){
             console.log($(this).data('id'));

            $('.f-mega-menu').fadeOut();
            $('.f-mega-menu#'+$(this).data('id')).fadeIn();
        });

        $(document).on('mouseover', '.f-header-nav a.ignore', function(){
            $('.f-mega-menu').fadeOut();
        });

        $(document).on('mouseleave', '.f-mega-menu', function(){

            $('.f-mega-menu').fadeOut();
        });


        $(document).on('click', '.f-close-top-adv', function(){
            $('.adv-top').remove();

            headerHeight = $('.s-header').height() + 10;

            $('.adv-top-left, .adv-top-right').css('top', $('.s-header').height()+10);


            $('.s-main-content').css('margin-top', headerHeight);

        });

        $(document).on('click', '#search-head', function(){
           $('#search-cont').slideToggle();
        });

        var timer;
        function hide(){
        //Hide your menu bar
        }
        function show(){
        //Show your menu bar
        }

        $("#buttonID").hover(
            function() {
                clearTimeout(timer);
                show();
            }, function() {
                timer = setTimeout(function(){hide()},1000);
            }
        );




        function scrollTo(hash) {
            // location.hash = "#" + hash;
            location.hash = hash;
        }

        $('.f-flyright-nav a').on('click', function(){
            scrollTo($(this).attr('href'));
        });

        $('#to_top').on('click', function(){
            var body = $("html, body");
            body.stop().animate({scrollTop:0}, '500', 'swing', function() {
            });
        });


    });



    function sharePositioning(){
        var marginright = ($(window).width() - 1100)/2;
        var rightFixed = (marginright);
        $('.f-flyright-nav').css('right', rightFixed);
    }

    sharePositioning();

    $(document).ready(function() {

        $(window).resize(function(){
            sharePositioning();
        });


        $(window).scroll(function () {
            if ( $(window).scrollTop() + $('.s-header').height() > $('.s-header').height() + $('.adv-1100-90').height() ) {
                $('.f-flyright-nav').addClass('flyRightFixed');
                $('.f-flyright-nav').removeClass('flyRightAbsolute');
            } else {
                $('.f-flyright-nav').removeClass('flyRightFixed');
                $('.f-flyright-nav').addClass('flyRightAbsolute');
            }
        });

        $(window).scroll(function () {
            if ( $(window).scrollTop() + $('.s-header').height() > $('.s-header').height() + $('.adv-1100-90').height() ) {
                $('.f-flyLeft-nav').addClass('flyLeftFixed');
                $('.f-flyLeft-nav').removeClass('flyLeftAbsolute');
            } else {
                $('.f-flyLeft-nav').removeClass('flyLeftFixed');
                $('.f-flyLeft-nav').addClass('flyLeftAbsolute');
            }
        });

        dinamika_iklan();

    });
</script>
