<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <?php include('_css.php'); ?>
    </head>
    <body class="body-offcanvas">
        <!-- Advertisement Top -->
        <!--==================================================-->
        <!-- Header -->
        <!--==================================================-->
        <?php include('_header.php'); ?>


        <!--Content-->
        <!--==================================================-->
        <section class="s-main-content">

            <!-- Adv Middle Top -->
            <!--==================================================-->
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>

            <!-- Adv Left -->
            <!--==================================================-->
            <div class="adv adv-160-600 adv-top-left">

            </div>

            <!-- Adv Right -->
            <!--==================================================-->
            <div class="adv adv-160-600 adv-top-right">

            </div>

            <!-- Middel Content -->
            <!--==================================================-->
            <div class="s-col-2-per-3 pull-left" style="/*background: #123;*/">

                <div class="s-fancy-header-red">
                <span class="s-title" style="text-transform: uppercase;">Citizen Report</span>
                    <ul class="list-unstyled s-sub-menu">
                        <li>
                            <a class="active" href="#">
                                Keseluruhan
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Berita
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Infografis
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Berita Video
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Berita Foto
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="adv adv-790-100">
                    ADVERTISE 790 x 100
                </div>
                <?php include('node_citizen_report.php'); ?>
                <div class="adv adv-790-100">
                    ADVERTISE 790 x 100
                </div>
                <?php include('node_berita_terkini.php'); ?>
            </div>

            <!-- Middel Content -->
            <!--==================================================-->
            <div class="s-col-1-per-3 pull-right" style="">
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php include('node_berita_pop.php'); ?>
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php include('node_event_terkini.php'); ?>
                <?php for($i=0; $i < 3; $i++){ ?>
                <div class="adv adv-300-250">
                    ADVERTISE 300 x 250
                </div>
                <?php } ?>
             </div>
             <span class="clearfix"></span>
            <div class="adv adv-1100-90">
                ADVERTISE 1100 x 90
            </div>



        </section>




        <!--Footer-->
        <!--==================================================-->
        <?php include('_footer.php'); ?>





    </body>
</html>
